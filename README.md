# Document Viewer

This project contains two Maven based Java projects enabling the visualisation of document resulting of a processing in a WebLab based application.
* document-viewer-portlet, which is the portlet presenting the documents
* structured-document-renderer is a component used a a dependency


# Build status
## Develop
[![develop build status](https://gitlab.ow2.org/weblab/portlets/document-viewer/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/portlets/document-viewer/commits/develop)

## Master
[![master build status](https://gitlab.ow2.org/weblab/portlets/document-viewer/badges/master/build.svg)](https://gitlab.ow2.org/weblab/portlets/document-viewer/commits/master)