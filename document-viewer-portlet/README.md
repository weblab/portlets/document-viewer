
## OW2 Document Viewer  portlet


The document viewer portlet allow to specify following points:

 - Customisable url generation, to plug docview on different knowledge base behind tooltip (DocViewPortlet-portlet.xml)


To use the url generation you have to add url maker object to DocumentViewerController .

    <bean class="com.airbus.ds.weblab.portlets.controller.DocumentViewerController">
    		<constructor-arg index="0" type="java.lang.String"
    			value="#{systemProperties['kdb.portlet.down']}" />
    		<constructor-arg index="1"
    			type="org.ow2.weblab.portlet.utils.IUrlMaker" ref="defaultUrlMaker" /> 
    	</bean>
        
    	<bean id="defaultUrlMaker" class="org.ow2.weblab.portlet.utils.DefaultUrlMaker">
    		<constructor-arg index="0" type="java.lang.String"
    			value="https://fr.wikipedia.org/wiki/">
    	</bean>


It can be use via overlay mechanism of the current portlet.
