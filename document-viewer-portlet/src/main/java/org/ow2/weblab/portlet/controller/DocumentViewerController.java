/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.helper.impl.SemanticResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.portlet.Constants;
import org.ow2.weblab.portlet.bean.HighlightedPagesBean;
import org.ow2.weblab.portlet.bean.LegendField;
import org.ow2.weblab.portlet.bean.LegendFieldsBean;
import org.ow2.weblab.portlet.bean.MediaServiceBean;
import org.ow2.weblab.portlet.bean.NamedEntity;
import org.ow2.weblab.portlet.service.DocumentViewerService;
import org.ow2.weblab.portlet.service.MediaViewerService;
import org.ow2.weblab.portlet.service.UserService;
import org.ow2.weblab.portlet.utils.IUrlMaker;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * MVC Spring controller of the document viewer portlet.
 *
 * @author emilienbondu and esther
 *
 */

@RequestMapping(value = "VIEW")
@SessionAttributes({ Constants.ORIGINAL_RESOURCE, Constants.ORIGINAL_SEMANTIC_RESOURCE, Constants.RESOURCE, Constants.TRANSLATION_RESOURCE, Constants.NEED_RELOAD, Constants.RESOURCE_URI,
		Constants.SEMANTIC_RESOURCE, Constants.RESOURCE_TITLE, Constants.METABEAN, Constants.HIGHLIGHTED_PAGES, Constants.CURRENT_PAGE_NUMBER, Constants.TOTAL_PAGE_NUMBER, Constants.LEGEND_FIELDS,
		Constants.IS_EMPTY, Constants.SPAN_ENTITIES, Constants.SPAN_HREFS, Constants.LEGEND_URI_CLASS_MAP, Constants.IS_TRANSLATION, Constants.IS_SOURCE, Constants.MEDIA_VIEWER_INFO,
		Constants.IS_STRUCTURED, Constants.DISPLAY_STRUCTURED, Constants.KDB_DOWN, Constants.URL_MAKER_ACTIVE })
public class DocumentViewerController {


	private static final String UTF_8 = "UTF-8";


	private static final String TEXT_HTML = "text/html";


	/**
	 * The logger used inside
	 */
	protected final Log logger = LogFactory.getLog(this.getClass());


	// The first part of the URI of the created instances.
	private static final String INSTANCE_URI_PREFIX = "http://weblab.ow2.org/wookie/instances/";


	@Autowired
	protected DocumentViewerService documentViewerService;


	@Autowired
	protected MediaViewerService mediaViewerService;


	@Autowired
	@Qualifier("legendBean")
	protected LegendFieldsBean defaultLegendFields;


	@Autowired
	@Qualifier("pagesBean")
	HighlightedPagesBean defaultPagesBean;


	@Autowired
	UserService userService;


	IUrlMaker urlMaker = null;


	/**
	 * Show links to workspace and KDB if set in configuration.xml
	 */
	private boolean kdbDown = false;


	@javax.annotation.Resource(name = "rightToLeftLang")
	private final List<String> rightToLeftLang = new ArrayList<>();


	// #############################################################################
	// render mapping methods #
	// #############################################################################

	/**
	 * Constructor for DocView
	 */
	public DocumentViewerController() {

		this(null, null);
	}


	public DocumentViewerController(final IUrlMaker urlMaker) {

		this((String) null, urlMaker);
	}


	public DocumentViewerController(final String kdbDown) {

		this(kdbDown, null);
	}


	public DocumentViewerController(final String kdbDown, final IUrlMaker urlMaker) {

		if (kdbDown != null) {
			this.kdbDown = Boolean.parseBoolean(kdbDown);
		}
		this.urlMaker = urlMaker;
		this.logger.debug("workspace and kdb down:" + this.kdbDown);
	}


	/**
	 * The main view rendering method
	 *
	 * @param model
	 *            The model
	 * @param request
	 *            The request used to retrive user role
	 * @return The model and view
	 */
	@RenderMapping
	public ModelAndView showMainView(final ModelMap model, final RenderRequest request) {

		this.setHasAdminRole(model, request);

		final ModelAndView mav = new ModelAndView("view");
		if (!model.containsAttribute(Constants.IS_EMPTY)) {
			mav.addObject(Constants.IS_EMPTY, Boolean.TRUE);
			mav.addObject(Constants.IS_SOURCE, Boolean.TRUE);
		}
		mav.addObject(Constants.KDB_DOWN, Boolean.valueOf(this.kdbDown));
		if (this.urlMaker != null) {
			mav.addObject(Constants.URL_MAKER_ACTIVE, this.urlMaker.isActive());
		} else {
			mav.addObject(Constants.URL_MAKER_ACTIVE, Boolean.FALSE);

		}
		mav.addAllObjects(model);
		return mav;
	}


	// #############################################################################
	// action mapping methods #
	// #############################################################################

	/**
	 * The user select a validated entity. An event to share this action is sent to others Portlets.
	 *
	 * @param response
	 *            The action resource
	 * @param instanceURI
	 *            The uri of the selected instance
	 * @param classURI
	 *            The class of the selected instance
	 * @param model
	 *            The model
	 */
	@ActionMapping(value = Constants.ACTION_MAPPING_SELECT_VALIDATED_ENTITY)
	public void selectValidatedInstance(final ActionResponse response, @RequestParam("instance_uri") final String instanceURI, @RequestParam("class_uri") final String classURI, final ModelMap model) {

		this.logger.debug("receiving action select validated entity");

		if ((instanceURI != null) && (classURI != null)) {

			final PieceOfKnowledge pok = new PieceOfKnowledge();
			pok.setUri("http://pok_" + UUID.randomUUID().toString());

			final WProcessingAnnotator wpa = new WProcessingAnnotator(URI.create(instanceURI), pok);
			wpa.writeCandidate(Boolean.FALSE);
			wpa.writeType(URI.create(classURI));

			if (model.containsAttribute(Constants.RESOURCE) && (model.get(Constants.RESOURCE) instanceof Document)) {

				final Document currentDoc = (Document) model.get(Constants.RESOURCE);
				final SemanticResource semRes = new SemanticResource(currentDoc);

				final String label = this.getDocumentViewerService().findLabel(semRes, instanceURI);
				if (StringUtils.isNotEmpty(label)) {
					wpa.writeLabel(label);
				}
			}

			this.logger.debug("Sending event for the action 'instanceSelection'");
			response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/docview/action}instanceSelection"), pok);
		} else {
			this.logger.warn("Sending event for the action 'instanceSelection'");
		}
	}


	/**
	 * The user select a candidate entity. An event to share this action is sent to others Portlets.
	 *
	 * @param response
	 *            The action response
	 * @param instanceURI
	 *            The uri of the selected instance
	 * @param classURI
	 *            The class of the selected instance
	 * @param label
	 *            The label of the selected instance
	 * @param model
	 *            The model
	 */
	@ActionMapping(value = Constants.ACTION_MAPPING_SELECT_CANDIDATE_ENTITY)
	public void selectCandidateInstance(final ActionResponse response, @RequestParam("candidate_instance_uri") final String instanceURI, @RequestParam("class_uri") final String classURI,
			@RequestParam("label") final String label, final ModelMap model) {

		this.logger.debug("receiving action select candidate entity " + instanceURI + "(" + classURI + ")");

		if ((instanceURI != null) && (classURI != null)) {
			// PieceOfKnowledge to send
			final PieceOfKnowledge pok = new PieceOfKnowledge();
			pok.setUri("http://pok_" + UUID.randomUUID().toString());

			final WProcessingAnnotator wpa = new WProcessingAnnotator(URI.create(instanceURI), pok);
			wpa.writeCandidate(Boolean.TRUE);
			wpa.writeType(URI.create(classURI));
			wpa.writeLabel(label);

			this.logger.debug("Sending event for the action 'candidateInstanceSelection'");
			response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/docview/action}candidateInstanceSelection"), pok);
		}
	}


	/**
	 * The user add the current document to the basket. An event to share this action is sent to others Portlets.
	 *
	 * @param response
	 *            The action response
	 * @param model
	 *            The model
	 */
	@ActionMapping(params = "action=addToBasket")
	public void addToBasket(final ActionResponse response, final ModelMap model) {

		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/docview/action}addToBasket"), (Document) model.get(Constants.ORIGINAL_RESOURCE));
	}


	/**
	 * The user pin the current document to the basket. An event to share this action is sent to others Portlets.
	 *
	 * @param response
	 *            The action response
	 * @param model
	 *            The model
	 */
	@ActionMapping(params = "action=pinToBasket")
	public void pinToBasket(final ActionResponse response, final ModelMap model) {

		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/docview/action}pinToBasket"), (Document) model.get(Constants.ORIGINAL_RESOURCE));
	}


	/**
	 * The user add a new entity.
	 *
	 * @param response
	 *            The action response
	 * @param namedEntity
	 *            The name entity
	 * @param model
	 *            The model
	 */
	@ActionMapping("saveAddition")
	public void saveAddition(final ActionResponse response, final NamedEntity namedEntity, final ModelMap model) {

		this.logger.debug("receiving action insert segment " + namedEntity);

		if (namedEntity != null) {

			// Construct the base uri from the entity type and the instance url prefix
			final String entType = namedEntity.getEntityClassURI().split("#")[1];
			final String baseUri = DocumentViewerController.INSTANCE_URI_PREFIX + entType + "#";

			// Generate the entity uri randomly and append it to the base uri
			final UUID entityURI = UUID.randomUUID();
			final String uri = URI.create(baseUri + entityURI).toString();

			namedEntity.setEntityURI(uri);

			this.logger.debug("Inserting Segment");
			this.getDocumentViewerService().insertNamedEntity((String) model.get(Constants.RESOURCE_URI), (Integer) model.get(Constants.CURRENT_PAGE_NUMBER), namedEntity);

			final HighlightedPagesBean pages = (HighlightedPagesBean) model.get(Constants.HIGHLIGHTED_PAGES);
			pages.getPages().remove(model.get(Constants.CURRENT_PAGE_NUMBER));
		}
	}


	/**
	 * The user modify an entity
	 *
	 * @param response
	 *            The action response
	 * @param namedEntity
	 *            The named entity to be saved
	 * @param model
	 *            The model
	 */
	@ActionMapping("saveEdit")
	public void saveEdit(final ActionResponse response, final NamedEntity namedEntity, final ModelMap model) {

		this.logger.warn("Nothing has been coded here !!!");
		// TODO
	}


	/**
	 * The user delete an entity
	 *
	 * @param response
	 *            The action response
	 * @param namedEntity
	 *            The named entity to be deleted
	 * @param model
	 *            The model
	 */
	@ActionMapping("saveDelete")
	public void saveDelete(final ActionResponse response, final NamedEntity namedEntity, final ModelMap model) {

		if (namedEntity != null) {
			this.logger.debug("Deleting Segment");
			this.getDocumentViewerService().deleteNamedEntity((String) model.get(Constants.RESOURCE_URI), (SemanticResource) model.get(Constants.SEMANTIC_RESOURCE),
					(Integer) model.get(Constants.CURRENT_PAGE_NUMBER), namedEntity);

			final HighlightedPagesBean pages = (HighlightedPagesBean) model.get(Constants.HIGHLIGHTED_PAGES);
			pages.getPages().remove(model.get(Constants.CURRENT_PAGE_NUMBER));
		}
	}


	// #############################################################################
	// event mapping methods #
	// #############################################################################

	/**
	 * Another Portlet sent a document to be rendered
	 *
	 * @param request
	 *            The event request
	 * @param model
	 *            The model
	 * @return The model map
	 */
	@EventMapping(value = "{http://weblab.ow2.org/portlet/docview/reaction}displayDocument")
	public ModelMap processLoadedDocumentEvent(final EventRequest request, final ModelMap model) {

		// receive a event containing a document
		this.logger.debug("receiving the event : {http://weblab.ow2.org/portlet/docview/reaction}displayDocument");

		DocumentViewerController.initDefaultModel(model, true);

		model.addAttribute(Constants.RESOURCE, request.getEvent().getValue());
		return model;
	}


	/**
	 * Another Portlet sent an "unloaded" document to be rendered
	 *
	 * @param request
	 *            The event request
	 * @param model
	 *            The model map
	 */
	@EventMapping(value = "{http://weblab.ow2.org/portlet/docview/reaction}loadAndDisplayDocument")
	public void processNotLoadedDocumentEvent(final EventRequest request, final ModelMap model) {

		// receive a event containing an unloaded document
		this.logger.debug("receiving the event : {http://weblab.ow2.org/portlet/docview/reaction}loadAndDisplayDocument");

		if (this.logger.isTraceEnabled()) {
			this.logger.trace("incoming model :" + model);
		}

		DocumentViewerController.initDefaultModel(model, true);

		model.addAttribute(Constants.RESOURCE, request.getEvent().getValue());
		model.addAttribute(Constants.CURRENT_PAGE_NUMBER, Integer.valueOf(1));
	}


	/**
	 * Another Portlet sent an unloaded document to be rendered with a set of relevant keywords
	 *
	 * @param request
	 *            The event request
	 * @param model
	 *            The model map
	 */
	@EventMapping(value = "{http://weblab.ow2.org/portlet/docview/reaction}loadAndDisplayDocumentWithRelevantTerms")
	public void processNotLoadedDocumentWithTermsEvent(final EventRequest request, final ModelMap model) {

		// receive a event containing an unloaded document
		this.logger.debug("receiving the event : {http://weblab.ow2.org/portlet/docview/reaction}loadAndDisplayDocumentWithRelevantTerms");

		if (this.logger.isTraceEnabled()) {
			this.logger.trace("incoming model event:" + model);
		}

		DocumentViewerController.initDefaultModel(model, true);

		model.addAttribute(Constants.RESOURCE, request.getEvent().getValue());
		model.addAttribute(Constants.CURRENT_PAGE_NUMBER, Integer.valueOf(1));
	}


	/**
	 * Another Portlet sent event to display translated document
	 *
	 * @param response
	 *            The even response
	 * @param request
	 *            The event request
	 * @param model
	 *            The model map
	 */
	@EventMapping(value = "{http://weblab.ow2.org/portlet/docview/reaction}selectTranslation")
	public void selectTranslation(final EventRequest request, final EventResponse response, final ModelMap model) {

		// receive a event containing a pok with translation information
		this.logger.debug("receiving the event : {http://weblab.ow2.org/portlet/docview/reaction}selectTranslation");

		final Resource resource = (Resource) request.getEvent().getValue();

		if (resource instanceof PieceOfKnowledge) {

			DocumentViewerController.initDefaultModel(model, false);

			model.addAttribute(Constants.TRANSLATION_RESOURCE, resource);
			model.addAttribute(Constants.IS_TRANSLATION, Boolean.TRUE);
			model.addAttribute(Constants.IS_SOURCE, Boolean.FALSE);
			model.addAttribute(Constants.NEED_RELOAD, Boolean.TRUE);
		}
	}


	/**
	 * Another Portlet sent event to original source document
	 *
	 * @param response
	 *            The even response
	 * @param request
	 *            The event request
	 * @param model
	 *            The model map
	 */
	@EventMapping(value = "{http://weblab.ow2.org/portlet/docview/reaction}selectOriginal")
	public void selectOriginal(final EventRequest request, final EventResponse response, final ModelMap model) {

		// receive a event containing a pok
		this.logger.debug("receiving the event : {http://weblab.ow2.org/portlet/docview/reaction}selectOriginal");

		DocumentViewerController.initDefaultModel(model, false);

		model.addAttribute(Constants.IS_SOURCE, Boolean.TRUE);
		model.addAttribute(Constants.IS_TRANSLATION, Boolean.FALSE);
		model.addAttribute(Constants.NEED_RELOAD, Boolean.TRUE);
	}


	// #############################################################################
	// resource mapping methods #
	// #############################################################################

	/**
	 * Update session to keep displayStrcutred state
	 *
	 * @param request
	 *            The resource request
	 * @param displayStructured
	 *            The displayStructured value
	 * @param model
	 *            The model map
	 */
	@ResourceMapping("serveUpdateDisplayStructured")
	public void serveUpdateDisplayStructured(final ResourceRequest request, final boolean displayStructured, final ModelMap model) {

		model.addAttribute(Constants.DISPLAY_STRUCTURED, Boolean.valueOf(displayStructured));
	}


	/**
	 * Access to the legend fragment
	 *
	 * @param response
	 *            The resource response
	 * @param request
	 *            The resource request
	 * @param model
	 *            The model map
	 * @return The model and view
	 */
	@ResourceMapping("legend")
	public ModelAndView serveLegend(final ResourceRequest request, final ResourceResponse response, final ModelMap model) {

		this.logger.debug("serveLegend -- start");

		response.setContentType(DocumentViewerController.TEXT_HTML);
		response.setCharacterEncoding(DocumentViewerController.UTF_8);

		final ModelAndView mav = new ModelAndView();
		mav.setView("legend");

		if (!model.containsAttribute(Constants.LEGEND_FIELDS)) {
			model.addAttribute(Constants.LEGEND_FIELDS, this.defaultLegendFields);
		}

		DocumentViewerController.initLegendUriClassMap(model, ((LegendFieldsBean) model.get(Constants.LEGEND_FIELDS)).getFields());

		if (!model.containsAttribute(Constants.CURRENT_PAGE_NUMBER)) {
			model.addAttribute(Constants.CURRENT_PAGE_NUMBER, Integer.valueOf(1));
		}

		mav.getModelMap().addAllAttributes(model);

		this.logger.debug("serveLegend -- stop");

		return mav;
	}


	/**
	 * Acces to a given page of the document.
	 *
	 * @param response
	 *            The resource response
	 * @param request
	 *            The resource request
	 * @param model
	 *            The model map
	 * @return The model and view
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	@ResourceMapping("page")
	public ModelAndView servePage(final ResourceRequest request, final ResourceResponse response, final ModelMap model) throws Exception {

		this.logger.debug("servePage -- start");

		ModelAndView mav = new ModelAndView("HLDocument");
		response.setContentType(DocumentViewerController.TEXT_HTML);
		response.setCharacterEncoding(DocumentViewerController.UTF_8);

		if (!model.containsKey(Constants.RESOURCE)) {
			mav = new ModelAndView("error");
			mav.addObject("page_unavailable", Boolean.TRUE);
			return mav;
		}

		final int pageNumber = Integer.valueOf(request.getParameter("pageNumber")).intValue();

		this.createPageModel(model, pageNumber, response);

		if (model.get(Constants.RESOURCE) instanceof Document) {
			final Document doc = (Document) model.get(Constants.RESOURCE);
			final boolean isStructured = this.getDocumentViewerService().checkHasStructure(doc);
			model.addAttribute(Constants.IS_STRUCTURED, Boolean.valueOf(isStructured));
		}

		mav.getModelMap().addAllAttributes(model);

		this.logger.debug("servePage -- stop");

		return mav;
	}


	/**
	 * To render a tooltip for a given HTML span
	 *
	 * @param response
	 *            The resource response
	 * @param spanID
	 *            The id of the span to be displayed
	 * @param model
	 *            The model map
	 * @return The model and view
	 * @throws UnsupportedEncodingException
	 *             If UTF-8 is not handled (should never occurs)
	 */
	@ResourceMapping("serveTooltip")
	public ModelAndView serveToolTip(final ResourceRequest request, final ResourceResponse response, final String spanID, final ModelMap model) throws UnsupportedEncodingException {

		this.logger.debug("serveToolTip -- start");

		response.setContentType(DocumentViewerController.TEXT_HTML);
		response.setCharacterEncoding(DocumentViewerController.UTF_8);

		final ModelAndView mav = new ModelAndView("tooltip");
		this.setHasAdminRole(model, request);
		// inject model attributes
		mav.addObject("spanID", spanID);

		@SuppressWarnings("unchecked")
		final HashMap<String, LinkedList<String>> spanEnts = (HashMap<String, LinkedList<String>>) model.get(Constants.SPAN_ENTITIES);

		final LinkedList<String> listEntsWithPossibleDuplicates = spanEnts.get(spanID);

		final HashSet<String> listEnts = new HashSet<>();
		if (listEntsWithPossibleDuplicates != null) {
			listEnts.addAll(listEntsWithPossibleDuplicates);
		}

		mav.addObject("listEnts", listEnts);

		if (this.urlMaker != null) {
			mav.addObject(Constants.URL_MAKER_ACTIVE, this.urlMaker.isActive());
		} else {
			mav.addObject(Constants.URL_MAKER_ACTIVE, Boolean.FALSE);

		}
		@SuppressWarnings("unchecked")
		final HashMap<String, String> spanHref = (HashMap<String, String>) model.get(Constants.SPAN_HREFS);
		final String href = spanHref.get(spanID);

		if (href != null) {
			mav.addObject("href", href);
			mav.addObject("hrefDecode", URLDecoder.decode(href, DocumentViewerController.UTF_8));
		}

		DocumentViewerController.initLegendUriClassMap(model, this.defaultLegendFields.getFields());

		if (!model.containsKey("tooltipBean")) {
			// add new attribute
			model.addAttribute("tooltipBean", this.getDocumentViewerService().getTooltipDetails(listEnts, href, (HashMap<String, String>) model.get(Constants.LEGEND_URI_CLASS_MAP),
					(SemanticResource) model.get(Constants.ORIGINAL_SEMANTIC_RESOURCE), this.urlMaker));
			model.addAttribute("sourceId", ((Resource) model.get(Constants.RESOURCE)).getUri());
		}

		this.logger.debug("serveTooltip -- stop");

		return mav;
	}


	/**
	 * @param response
	 *            The resource response
	 * @param spanID
	 *            The id of the popup to be populated
	 * @param model
	 *            The model map
	 * @return The model and view
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	@ResourceMapping("servePopupTranslate")
	public ModelAndView servePopupTranslate(final ResourceResponse response, final String spanID, final ModelMap model) throws Exception {

		this.logger.debug("servePopupTranslate -- start");

		response.setContentType(DocumentViewerController.TEXT_HTML);
		response.setCharacterEncoding(DocumentViewerController.UTF_8);

		final ModelAndView mav = new ModelAndView("popupTranslate");

		DocumentViewerController.initLegendUriClassMap(model, this.defaultLegendFields.getFields());

		final Document doc = (Document) model.get(Constants.ORIGINAL_RESOURCE);

		// add segment mapping
		final SemanticResource semRes = new SemanticResource(doc);

		final Map<String, String> availableLangs = this.getDocumentViewerService().findTranslationsAvailable(semRes);
		model.addAttribute("availableLangs", availableLangs);

		// no translation info found
		if (availableLangs == null) {
			return null;
		}

		if (!model.containsAttribute(Constants.SPAN_ENTITIES)) {
			final HashMap<String, LinkedList<String>> spanEntities = new HashMap<>();
			final HashMap<String, String> spanHrefs = new HashMap<>();

			model.addAttribute(Constants.SPAN_ENTITIES, spanEntities);
			model.addAttribute(Constants.SPAN_HREFS, spanHrefs);
		}

		// build translation documents
		final Map<String, Object> mapHiddenPages = new HashMap<>();
		for (final Entry<String, String> entry : availableLangs.entrySet()) {

			final ModelMap modelMap = new ModelMap();

			// build sub doc for this translation
			final Document subDoc = this.getDocumentViewerService().buildSubDocument(doc, semRes, spanID, entry.getKey());
			modelMap.put(Constants.RESOURCE, subDoc);
			modelMap.put(Constants.SEMANTIC_RESOURCE, new SemanticResource(subDoc));

			// init variables
			final HighlightedPagesBean hiddenPages = new HighlightedPagesBean();
			hiddenPages.setPageSize(this.defaultPagesBean.getPageSize());
			hiddenPages.setShowInstanceLink(this.defaultPagesBean.isShowInstanceLink());
			hiddenPages.setShowTooltips(this.defaultPagesBean.isShowTooltips());
			hiddenPages.setShowAddBasketAction(this.defaultPagesBean.isShowAddBasketAction());
			hiddenPages.setShowPinBasketAction(this.defaultPagesBean.isShowPinBasketAction());
			hiddenPages.setPages(new HashMap<Integer, String>());

			// build
			final Writer writer = new StringWriter();
			this.getDocumentViewerService().highlightPage(1, modelMap, writer, this.defaultLegendFields, true, hiddenPages);

			mapHiddenPages.put(entry.getKey(), hiddenPages);
		}

		mav.addObject("mapHiddenHighlightedPages", mapHiddenPages);

		this.logger.debug("servePopupTranslate -- stop");

		return mav;
	}


	/**
	 * To render the edit panel about a given entity.
	 *
	 * @param highlightedText
	 *            The text that was selected
	 * @param namedEntityToAdd
	 *            The named entity to add
	 * @param spanID
	 *            The id of th span on which to open an edit panel
	 * @param response
	 *            The resource response
	 * @param model
	 *            The model map
	 * @return The model and view
	 */
	@ResourceMapping("serveEditPanel")
	public ModelAndView serveEditPanel(final ResourceResponse response, final String highlightedText, final NamedEntity namedEntityToAdd, final String spanID, final ModelMap model) {

		this.logger.debug("serveEditPanel -- start");

		response.setContentType(DocumentViewerController.TEXT_HTML);
		response.setCharacterEncoding(DocumentViewerController.UTF_8);

		final ModelAndView mav = new ModelAndView("editPanel");

		// inject model attributes
		if (highlightedText != "") {
			mav.addObject("highlightedText", highlightedText);
		}
		mav.addObject("namedEntityToAdd", namedEntityToAdd);

		mav.addObject("spanID", spanID);

		model.addAttribute("spanID", spanID);

		// Get the list of entity urls corresponding to this span
		final HashMap<String, LinkedList<String>> spanEnts = (HashMap<String, LinkedList<String>>) model.get("spanEntities");

		final LinkedList<String> listEntsWithPossibleDuplicates = spanEnts.get(spanID);

		final HashSet<String> listEnts = new HashSet<>();
		if (listEntsWithPossibleDuplicates != null) {
			listEnts.addAll(listEntsWithPossibleDuplicates);
		}

		DocumentViewerController.initLegendUriClassMap(model, this.defaultLegendFields.getFields());

		// Get the details of the named entities associated with the urls
		model.addAttribute("listNamedEntities",
				this.getDocumentViewerService().getListNamedEntities((String) model.get(Constants.RESOURCE_URI), (SemanticResource) model.get(Constants.SEMANTIC_RESOURCE),
						(Integer) model.get(Constants.CURRENT_PAGE_NUMBER), listEnts, (HashMap<String, String>) model.get(Constants.LEGEND_URI_CLASS_MAP)));

		// Add the list of entity urls to the model (can loop through them in the editPanel.jsp)
		mav.addObject("listEnts", listEnts);

		this.logger.debug("serveEditPanel -- stop");

		return mav;
	}


	/**
	 * To access to the target of a given link of the document (page, paragraph, etc.)
	 *
	 * @param spanID
	 *            The id of the span
	 * @param response
	 *            The resource response
	 * @param model
	 *            The model map
	 * @return The model and view
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	@ResourceMapping("hrefRequest")
	public ModelAndView hrefRequest(final ResourceResponse response, final String spanID, final ModelMap model) throws Exception {

		this.logger.debug("hrefRequest -- start");

		ModelAndView mav = new ModelAndView("HLDocument");
		response.setContentType(DocumentViewerController.TEXT_HTML);
		response.setCharacterEncoding(DocumentViewerController.UTF_8);

		if (!model.containsKey(Constants.RESOURCE)) {
			mav = new ModelAndView("error");
			mav.addObject("page_unavailable", Boolean.TRUE);
			return mav;
		}

		final HashMap<String, String> spanHref = (HashMap<String, String>) model.get(Constants.SPAN_HREFS);

		final String href = spanHref.get(spanID);

		// If the href is non-null and internal, then load the appropriate page, otherwise do nothing
		// TODO in the future - check to see if the target page is in the triplestore and go there.
		if (href != null) {
			mav.addObject("href", href);
			mav.addObject("hrefDecode", URLDecoder.decode(href, DocumentViewerController.UTF_8));
			if (href.startsWith("http://Internal_Href")) {
				final int pageNumber = Integer.parseInt(href.split(".html")[0].split("#")[1]);
				this.createPageModel(model, pageNumber, response);
				mav.addAllObjects(model);
			}
		}

		this.logger.debug("hrefRequest -- start");

		return mav;
	}


	@ResourceMapping("mediaPlayer")
	public ModelAndView addMediaPlayer(final ResourceRequest request, final ResourceResponse response, final ModelMap model) {

		this.logger.debug("mediaPlayer -- start");

		this.logger.debug("Media Player Called");

		final ModelAndView mav = new ModelAndView("mediaPlayer");
		response.setContentType(DocumentViewerController.TEXT_HTML);
		response.setCharacterEncoding(DocumentViewerController.UTF_8);
		final SemanticResource semResource = (SemanticResource) model.get(Constants.ORIGINAL_SEMANTIC_RESOURCE);
		final Resource resource = (Resource) model.get(Constants.RESOURCE);

		final MediaServiceBean mediaServiceInfo = new MediaServiceBean();
		mediaServiceInfo.setResource(semResource);
		this.getMediaViewerService().findContentFormat(mediaServiceInfo, resource.getUri());
		model.addAttribute(Constants.MEDIA_VIEWER_INFO, mediaServiceInfo);

		this.logger.debug("mediaPlayer -- stop");

		return mav;
	}


	// #############################################################################
	// exceptions handlers #
	// #############################################################################

	/**
	 * Handle Weblab and IO exceptions
	 *
	 * @param ex
	 *            The exception
	 * @return The model and view
	 */
	@ExceptionHandler({ WebLabUncheckedException.class, WebLabCheckedException.class, IOException.class })
	public ModelAndView catchError(final Exception ex) {

		final ModelAndView mav = new ModelAndView("error");
		mav.addObject("message_error", ex.getLocalizedMessage());
		mav.addObject("resource_processing_error", Boolean.TRUE);
		return mav;
	}


	/**
	 * Handle other errors
	 *
	 * @param ex
	 *            The exception
	 * @return The model and view
	 */
	@ExceptionHandler({ Exception.class })
	public ModelAndView getException(final Exception ex) {

		final ModelAndView mav = new ModelAndView("error");
		mav.addObject("message_error", ex.getLocalizedMessage());
		return mav;
	}


	// #############################################################################
	// utilities #
	// #############################################################################

	/**
	 * To update the model for a given page
	 *
	 * @param model
	 * @param pageNumber
	 * @param response
	 * @throws Exception
	 */
	private void createPageModel(final ModelMap model, final int pageNumber, final ResourceResponse response) throws Exception {

		model.addAttribute(Constants.CURRENT_PAGE_NUMBER, Integer.valueOf(pageNumber));

		this.logger.debug("reload ? :" + model.get(Constants.NEED_RELOAD));

		boolean isLangChecked = false;

		if (model.containsKey(Constants.NEED_RELOAD) && ((Boolean) model.get(Constants.NEED_RELOAD)).booleanValue()) {

			// cleaning old model attributes
			model.addAttribute(Constants.NEED_RELOAD, Boolean.FALSE);
			model.remove(Constants.HIGHLIGHTED_PAGES);
			model.remove(Constants.RESOURCE_TITLE);
			model.remove(Constants.SEMANTIC_RESOURCE);
			model.remove(Constants.TOTAL_PAGE_NUMBER);
			model.remove(Constants.METABEAN);

			// add info to display new document from uri in a pok
			if (!model.containsAttribute(Constants.RESOURCE_URI) && (model.get(Constants.RESOURCE) instanceof PieceOfKnowledge)) {
				this.logger.debug("Loading document from POK...");
				model.addAttribute(Constants.RESOURCE_URI, this.getDocumentViewerService().getURIFromPoK((PieceOfKnowledge) model.get(Constants.RESOURCE)));
			}
			// add info to display translation of a document
			else if (model.containsKey(Constants.IS_TRANSLATION) && ((Boolean) model.get(Constants.IS_TRANSLATION)).booleanValue()
					&& (model.get(Constants.TRANSLATION_RESOURCE) instanceof PieceOfKnowledge)) {

				this.logger.debug("Loading document from translation POK...");

				final Document doc = (Document) model.get(Constants.ORIGINAL_RESOURCE);
				final String translation = new WProcessingAnnotator(doc).readAvailableTranslation().firstTypedValue(); // translation is never null if we are here
				model.addAttribute("textStartRight", Boolean.valueOf(this.rightToLeftLang.contains(translation)));
				isLangChecked = true;
				// build translated document
				final PieceOfKnowledge pok = (PieceOfKnowledge) model.get(Constants.TRANSLATION_RESOURCE);
				final Document translatedDocument = this.getDocumentViewerService().buildSubDocumentTranslated(doc, pok);
				model.addAttribute(Constants.RESOURCE, translatedDocument);
				model.addAttribute(Constants.SEMANTIC_RESOURCE, new SemanticResource(translatedDocument));
			}
			// go back to display source document
			else if (model.containsKey(Constants.IS_SOURCE) && ((Boolean) model.get(Constants.IS_SOURCE)).booleanValue()) {

				this.logger.debug("Back to display source document!");

				final Document doc = (Document) model.get(Constants.ORIGINAL_RESOURCE);

				// build source document
				final Document sourceDocument = this.getDocumentViewerService().buildSubDocumentSource(doc);
				model.addAttribute(Constants.RESOURCE, sourceDocument);
				model.addAttribute(Constants.SEMANTIC_RESOURCE, new SemanticResource(sourceDocument));
			}
		}


		if (!model.containsAttribute(Constants.HIGHLIGHTED_PAGES)) {
			final HighlightedPagesBean pages = new HighlightedPagesBean();
			pages.setPageSize(this.defaultPagesBean.getPageSize());
			pages.setShowInstanceLink(this.defaultPagesBean.isShowInstanceLink());
			pages.setShowTooltips(this.defaultPagesBean.isShowTooltips());
			pages.setShowAddBasketAction(this.defaultPagesBean.isShowAddBasketAction());
			pages.setShowPinBasketAction(this.defaultPagesBean.isShowPinBasketAction());
			pages.setPages(new HashMap<Integer, String>());
			// add new attribute
			model.addAttribute(Constants.HIGHLIGHTED_PAGES, pages);
		}

		final HighlightedPagesBean pagesBean = (HighlightedPagesBean) model.get(Constants.HIGHLIGHTED_PAGES);

		if (!model.containsAttribute(Constants.SPAN_ENTITIES)) {
			final HashMap<String, LinkedList<String>> spanEntities = new HashMap<>();
			final HashMap<String, String> spanHrefs = new HashMap<>();

			model.addAttribute(Constants.SPAN_ENTITIES, spanEntities);
			model.addAttribute(Constants.SPAN_HREFS, spanHrefs);
		}

		DocumentViewerController.initLegendUriClassMap(model, this.defaultLegendFields.getFields());

		if (!pagesBean.getPages().containsKey(Integer.valueOf(pageNumber))) {
			this.logger.debug("Calling highlighter for the page " + pageNumber);

			this.getDocumentViewerService().highlightPage(pageNumber, model, response.getWriter(), this.defaultLegendFields, true, null);

			// update original resource
			if ((!model.containsKey(Constants.IS_TRANSLATION) || !((Boolean) model.get(Constants.IS_TRANSLATION)).booleanValue())
					&& (!model.containsKey(Constants.IS_SOURCE) || !((Boolean) model.get(Constants.IS_SOURCE)).booleanValue())) {
				//update ORIGINAL_RESOURCE only on load of the first page of document
				if(pageNumber==1) {
					model.addAttribute(Constants.ORIGINAL_RESOURCE, model.get(Constants.RESOURCE));
				}

				model.addAttribute(Constants.ORIGINAL_SEMANTIC_RESOURCE, new SemanticResource((Document) model.get(Constants.ORIGINAL_RESOURCE)));

				final Document doc = (Document) model.get(Constants.ORIGINAL_RESOURCE);

				// build source document
				final Document sourceDocument = this.getDocumentViewerService().buildSubDocumentSource(doc);
				model.addAttribute(Constants.RESOURCE, sourceDocument);
				model.addAttribute(Constants.SEMANTIC_RESOURCE, new SemanticResource(sourceDocument));

				// FIXME externalize the load the POK into a document in the model the first time an event is received in the highlightPage function.
				// Need to call it a second time to display only the source information the first call was only to load the POK into a document the first time an event is received...
				this.getDocumentViewerService().highlightPage(pageNumber, model, response.getWriter(), this.defaultLegendFields, true, null);
			}
		}

		final String originalLang = new DublinCoreAnnotator((Resource) model.get("resource")).readLanguage().firstTypedValue();
		if (!isLangChecked && (originalLang != null)) {
			model.addAttribute("textStartRight", Boolean.valueOf(this.rightToLeftLang.contains(originalLang)));
		}

		// meta
		model.addAttribute(Constants.METABEAN,
				this.getDocumentViewerService().getMetadata(((Resource) model.get(Constants.RESOURCE)).getUri(), (SemanticResource) model.get(Constants.SEMANTIC_RESOURCE)));

		this.logger.debug("number of pages for " + ((Resource) model.get(Constants.RESOURCE)).getUri()
				+ this.getDocumentViewerService().getMetadata(((Resource) model.get(Constants.RESOURCE)).getUri(), (SemanticResource) model.get(Constants.SEMANTIC_RESOURCE)).getTotalPages());
	}


	private static void initLegendUriClassMap(final ModelMap model, final List<LegendField> legendFields) {

		if (!model.containsAttribute(Constants.LEGEND_URI_CLASS_MAP)) {
			// Construct the map of entity uris and their associated class
			// names, including the equivalent types
			final HashMap<String, String> legendUriClassMap = new HashMap<>();
			for (final LegendField legend : legendFields) {
				legendUriClassMap.put(legend.getEntityType(), legend.getStyleClassName());
				for (final String sublegend : legend.getEqClasses()) {
					legendUriClassMap.put(sublegend, legend.getStyleClassName());
				}
			}
			model.addAttribute(Constants.LEGEND_URI_CLASS_MAP, legendUriClassMap);
		}
	}


	private static void initDefaultModel(final ModelMap model, final boolean clean) {

		if (clean) {
			model.clear();
		}

		if (!model.containsAttribute(Constants.DISPLAY_STRUCTURED)) {
			model.addAttribute(Constants.DISPLAY_STRUCTURED, Boolean.TRUE);
		}

		if (!model.containsAttribute(Constants.IS_EMPTY)) {
			model.addAttribute(Constants.IS_EMPTY, Boolean.FALSE);
		}

		if (!model.containsAttribute(Constants.NEED_RELOAD)) {
			model.addAttribute(Constants.NEED_RELOAD, Boolean.TRUE);
		}

		if (!model.containsAttribute(Constants.IS_TRANSLATION)) {
			model.addAttribute(Constants.IS_TRANSLATION, Boolean.FALSE);
		}

		if (!model.containsAttribute(Constants.IS_SOURCE)) {
			model.addAttribute(Constants.IS_SOURCE, Boolean.FALSE);
		}

		if (!model.containsAttribute(Constants.IS_STRUCTURED)) {
			model.addAttribute(Constants.IS_STRUCTURED, Boolean.TRUE);
		}
	}


	/**
	 * @return The service used inside
	 */
	protected DocumentViewerService getDocumentViewerService() {

		return this.documentViewerService;
	}


	/**
	 * @return The service used inside
	 */
	protected MediaViewerService getMediaViewerService() {

		return this.mediaViewerService;
	}


	/**
	 * Check if this profile as portlet admin role
	 *
	 * @param map
	 *            The model to be enriched
	 * @param request
	 *            The requets used to retrieve user role
	 */
	protected void setHasAdminRole(final ModelMap map, final PortletRequest request) {

		try {
			map.addAttribute("hasAdminRole", Boolean.valueOf(this.userService.hasAdminRole(request)));
		} catch (final SystemException | PortalException e) {
			this.logger.warn(e);
		}
	}

}
