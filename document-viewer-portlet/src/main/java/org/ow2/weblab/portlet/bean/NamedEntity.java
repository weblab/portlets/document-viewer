/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.util.List;

/**
 * Bean class representing a Named Entity A named entity has properties like
 * start and end offset in a text, Value like "Person", URItype like
 * "http://weblab.eads.com/property/gate/annotation" Style class name like
 * "_annotation_gate_person"
 *
 * @author emilien
 */
public class NamedEntity {


	private int startOffset;


	private int endOffset;


	private List<String> entityLabels;


	private String entityURI;


	private String entityClassURI;


	private String styleClassName;


	private boolean candidate;


	private String date;


	private String location;


	private String externalUrl;


	private String entityMapped;


	/**
	 * Default empty constructor
	 */
	public NamedEntity() {

		// Empty constructor
	}


	/**
	 * @param startOffset
	 *            The start offset of the named entity in the text
	 * @param endOffset
	 *            The end offset of the named entity in the text
	 * @param entityLabels
	 *            The list of labels of the instance
	 * @param entityURI
	 *            The uri of the instance
	 * @param entityClassURI
	 *            The type of the instance
	 * @param isCandidate
	 *            Whether or not is the instance a candidate
	 * @param styleClassName
	 *            The name of the style class for the entity
	 */
	public NamedEntity(final int startOffset, final int endOffset, final List<String> entityLabels, final String entityURI, final String entityClassURI, final boolean isCandidate,
			final String styleClassName) {

		super();
		this.startOffset = startOffset;
		this.endOffset = endOffset;
		this.entityLabels = entityLabels;
		this.entityURI = entityURI;
		this.entityClassURI = entityClassURI;
		this.candidate = isCandidate;
		this.styleClassName = styleClassName;

	}


	/**
	 * @return the startOffset
	 */
	public int getStartOffset() {

		return this.startOffset;
	}


	/**
	 * @param startOffset
	 *            the startOffset to set
	 */
	public void setStartOffset(final int startOffset) {

		this.startOffset = startOffset;
	}


	/**
	 * @return the isCandidate
	 */
	public boolean isCandidate() {

		return this.candidate;
	}


	/**
	 * @param isCandidate
	 *            the isCandidate to set
	 */
	public void setCandidate(final boolean isCandidate) {

		this.candidate = isCandidate;
	}


	/**
	 * @return the styleClassName
	 */
	public String getStyleClassName() {

		return this.styleClassName;
	}


	/**
	 * @param styleClassName
	 *            the styleClassName
	 */
	public void setStyleClassName(final String styleClassName) {

		this.styleClassName = styleClassName;
	}


	/**
	 * @return the endOffset
	 */
	public int getEndOffset() {

		return this.endOffset;
	}


	/**
	 * @param endOffset
	 *            the endOffset to set
	 */
	public void setEndOffset(final int endOffset) {

		this.endOffset = endOffset;
	}


	/**
	 * @return the entityLabel
	 */
	public List<String> getEntityLabels() {

		return this.entityLabels;
	}


	/**
	 * @param entityLabels
	 *            the entityLabels to set
	 */
	public void setEntityLabels(final List<String> entityLabels) {

		this.entityLabels = this.sanitanizeLabels(entityLabels);
	}


	/**
	 * @param entityLabels2
	 *            An input list of entity label
	 * @return The same list after sanitisation. Default implementation does nothing
	 */
	protected List<String> sanitanizeLabels(final List<String> entityLabels2) {

		// If we want to sanitize the labels, it has to be done here
		return entityLabels2;
	}


	/**
	 * @return the entityURI
	 */
	public String getEntityURI() {

		return this.entityURI;
	}


	/**
	 * @param entityURI
	 *            the entityURI to set
	 */
	public void setEntityURI(final String entityURI) {

		this.entityURI = entityURI;
	}


	/**
	 * @return the entityClassURI
	 */
	public String getEntityClassURI() {

		return this.entityClassURI;
	}


	/**
	 * @param entityClassURI
	 *            the entityClassURI to set
	 */
	public void setEntityClassURI(final String entityClassURI) {

		this.entityClassURI = entityClassURI;
	}


	/**
	 * @return The date
	 */
	public String getDate() {

		return this.date;
	}


	/**
	 * @param date
	 *            The date
	 */
	public void setDate(final String date) {

		this.date = date;
	}


	/**
	 * @return The location
	 */
	public String getLocation() {

		return this.location;
	}


	/**
	 * @param location
	 *            The location
	 */
	public void setLocation(final String location) {

		this.location = location;
	}


	/**
	 * @return the external url
	 */
	public String getExternalUrl() {

		return this.externalUrl;
	}


	/**
	 * @param externalUrl
	 *            the external url to set
	 */
	public void setExternalUrl(final String externalUrl) {

		this.externalUrl = externalUrl;
	}


	/**
	 * @return entityMapped
	 */
	public String getEntityMapped() {

		return this.entityMapped;
	}


	/**
	 * @param entityMapped
	 *            the new type of the entity after mapping
	 */
	public void setEntityMapped(final String entityMapped) {

		this.entityMapped = entityMapped;
	}

}
