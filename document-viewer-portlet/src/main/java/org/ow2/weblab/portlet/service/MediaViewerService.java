/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.portlet.bean.MediaServiceBean;
import org.ow2.weblab.portlet.bean.conf.MediaServiceConfBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.QuerySolutionMap;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.sparql.resultset.ResultSetMem;

@Service
public class MediaViewerService {


	protected final Log logger = LogFactory.getLog(this.getClass());


	protected final String REQUEST_PATH = "requests";


	protected final String SELECT_ALL_CONTENT = "selectAllContent";


	@Autowired
	@Qualifier("mediaServiceConfBean")
	private MediaServiceConfBean configuration;


	public void findContentFormat(MediaServiceBean mediaServiceBean, final String resourceUri) {

		// initialize variable
		mediaServiceBean.setVideo(false);
		mediaServiceBean.setAudio(false);
		mediaServiceBean.setImage(false);

		if (mediaServiceBean.getResource() != null) {

			final QuerySolutionMap qsmap = new QuerySolutionMap();
			qsmap.add("resourceURI", ResourceFactory.createResource(resourceUri));
			String query = "";
			try {
				query = this.loadSPARQL(this.SELECT_ALL_CONTENT);
			} catch (final IOException e) {
				this.logger.error("Unable to open or find the file", e);
			}

			final ParameterizedSparqlString pq = new ParameterizedSparqlString(query, qsmap);
			final ResultSetMem resultSet = mediaServiceBean.getResource().selectAsJenaResultSet(pq.toString());
			final Map<String, String> formatAndNative = new HashMap<>();
			final Map<String, String> formatAndNormalised = new HashMap<>();

			QuerySolution resultSetIndex;
			while (resultSet.hasNext()) {
				resultSetIndex = resultSet.next();
				this.logger.debug("result :  " + resultSetIndex.toString());

				RDFNode typeNode = resultSetIndex.get("type");
				if (typeNode != null) {
					String type = typeNode.asLiteral().toString().toLowerCase();
					mediaServiceBean.setType(type);
				}

				if (resultSetIndex.get("p").toString().toLowerCase().contains("nativecontent")) {
					formatAndNative.put(resultSetIndex.get("format").toString(), resultSetIndex.get("exposedAs").toString());
				} else {
					formatAndNormalised.put(resultSetIndex.get("format").toString(), resultSetIndex.get("exposedAs").toString());
				}
			}

			final Iterator<String> videoIterator = this.configuration.getVideoFormats().iterator();
			String videoFormat = "";

			while (videoIterator.hasNext() && !mediaServiceBean.isVideo()) {
				videoFormat = videoIterator.next();
				if (formatAndNative.containsKey(videoFormat)) {
					this.setVideoType(mediaServiceBean, videoFormat);
					mediaServiceBean.setNativeContentPath(formatAndNative.get(videoFormat));
					mediaServiceBean.setVideo(true);
				} else if (formatAndNormalised.containsKey(videoFormat)) {
					this.setVideoType(mediaServiceBean, videoFormat);
					mediaServiceBean.setNativeContentPath(formatAndNormalised.get(videoFormat));
					mediaServiceBean.setVideo(true);
				}
			}

			if (!mediaServiceBean.isVideo()) {

				final Iterator<String> audioIterator = this.configuration.getAudioFormats().iterator();
				String audioFormat = "";

				while (audioIterator.hasNext() && !mediaServiceBean.isAudio()) {

					audioFormat = audioIterator.next();

					if (formatAndNative.containsKey(audioFormat)) {
						this.setAudioType(mediaServiceBean, audioFormat);
						mediaServiceBean.setNativeContentPath(formatAndNative.get(audioFormat));
						mediaServiceBean.setAudio(true);
					} else if (formatAndNormalised.containsKey(audioFormat)) {
						this.setAudioType(mediaServiceBean, audioFormat);
						mediaServiceBean.setNativeContentPath(formatAndNormalised.get(audioFormat));
						mediaServiceBean.setAudio(true);
					}
				}

				final Iterator<String> imageIterator = this.configuration.getImageFormats().iterator();
				String imageFormat = "";

				while (imageIterator.hasNext() && !mediaServiceBean.isImage()) {

					imageFormat = imageIterator.next();

					if (formatAndNative.containsKey(imageFormat)) {
						this.setImageType(mediaServiceBean, imageFormat);
						mediaServiceBean.setNativeContentPath(formatAndNative.get(imageFormat));
						mediaServiceBean.setImage(true);
					} else if (formatAndNormalised.containsKey(imageFormat)) {
						this.setImageType(mediaServiceBean, imageFormat);
						mediaServiceBean.setNativeContentPath(formatAndNormalised.get(imageFormat));
						mediaServiceBean.setImage(true);
					}
				}
			}
		} else {
			this.logger.warn("semantic Resource is null, content won't be retrieved");
		}
	}


	// internal use : load a SPARQL select QUery from a .rq file on disk
	protected String loadSPARQL(final String queryName) throws IOException {

		final String path = this.REQUEST_PATH + File.separator + queryName + ".rq";
		try (final InputStream in = this.getClass().getClassLoader().getResourceAsStream(path)) {
			return IOUtils.toString(in, "UTF-8");
		}
	}


	protected void setVideoType(MediaServiceBean mediaServiceBean, final String format) {

		if (format.contains("mp4") || format.contains("youtube")) {
			mediaServiceBean.setFormat("video/mp4");
		} else if (format.contains("webm")) {
			mediaServiceBean.setFormat("video/webm");
		} else if (format.contains("ogg")) {
			mediaServiceBean.setFormat("video/ogg");
		} else {
			mediaServiceBean.setFormat(format);
		}
	}


	protected void setAudioType(MediaServiceBean mediaServiceBean, final String format) {

		if (format.contains("wav")) {
			mediaServiceBean.setFormat("audio/wav");
		} else if (format.contains("webm")) {
			mediaServiceBean.setFormat("audio/webm");
		} else if (format.contains("ogg")) {
			mediaServiceBean.setFormat("audio/ogg");
		} else if (format.contains("mp3") || format.contains("mpeg")) {
			mediaServiceBean.setFormat("audio/mpeg");
		} else {
			mediaServiceBean.setFormat(format);
		}
	}


	protected void setImageType(MediaServiceBean mediaServiceBean, final String format) {

		if (format.contains("png")) {
			mediaServiceBean.setFormat("image/png");
		} else if (format.contains("jpg") || format.contains("jpeg")) {
			mediaServiceBean.setFormat("image/jpeg");
		} else {
			mediaServiceBean.setFormat(format);
		}
	}
}
