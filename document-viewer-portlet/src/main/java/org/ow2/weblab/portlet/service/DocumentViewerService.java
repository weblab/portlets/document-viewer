/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.DocumentRendererBean;
import org.ow2.weblab.DocumentRendererProcessor;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.WebLabModel;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.impl.SemanticResource;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.model.structure.WStructureAnnotator;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.portlet.Constants;
import org.ow2.weblab.portlet.bean.DocumentMetadataBean;
import org.ow2.weblab.portlet.bean.HighlightedPagesBean;
import org.ow2.weblab.portlet.bean.LegendField;
import org.ow2.weblab.portlet.bean.LegendFieldsBean;
import org.ow2.weblab.portlet.bean.NamedEntity;
import org.ow2.weblab.portlet.bean.TooltipBean;
import org.ow2.weblab.portlet.bean.conf.DocumentServiceConfBean;
import org.ow2.weblab.portlet.utils.IUrlMaker;
import org.ow2.weblab.rdf.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.QuerySolutionMap;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import com.hp.hpl.jena.sparql.resultset.ResultSetMem;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.vocabulary.DC_11;

/**
 * Business document viewer services. Contains all business methods needed to build model for the document viewer
 * (rendering, retrieve entity information, etc.)
 *
 * @author emilienbondu and esther
 */
@Service
public class DocumentViewerService {


	private static final String REQUESTS_PATH = "requests";


	private static final String ADD_LABELS_TO_SEGMENT_REQUEST_FILENAME = "addLabelsToSegment";


	private static final String ADD_LABELS_TO_SEGMENT_NON_STRUCT_REQUEST_FILENAME = "addLabelsToSegmentNonStruct";


	private static final String DELETE_SEGMENT_REQUEST_FILENAME = "deleteSegment";


	private static final String DELETE_SEGMENT_NON_STRUCT_REQUEST_FILENAME = "deleteSegmentNonStruct";


	private static final String INSERT_SEGMENT_REQUEST_FILENAME = "insertSegment";


	private static final String INSERT_SEGMENT_NON_STRUCT_REQUEST_FILENAME = "insertSegmentNonStruct";


	private static final String SELECT_NAMED_ENTITY_DATA_REQUEST_FILENAME = "selectNamedEntityData";


	private static final String SELECT_NAMED_ENTITY_DATA_NON_STRUCT_REQUEST_FILENAME = "selectNamedEntityDataNonStruct";


	private static final String SELECT_TOOLTIP_ENTITY_DATA_REQUEST_FILENAME = "selectEntityTooltipData";


	private static final String ASK_IS_STRUCTURED_REQUEST_FILENAME = "askIsStructured";


	private static final String SELECT_SUB_DOCUMENT = "selectSubDocumentFromSegment";


	private static final String SELECT_MEDIA_UNIT_FROM_LANG_AND_TRANSLATION_SERVICE = "selectMediaUnitFromLangAndTranslationService";


	private static final String SELECT_TRANSLATIONS_AVAILABLE = "selectTranslationsAvailableForSegment";


	private static final String SELECT_ENTITY_FROM_SEGMENT = "selectEntityFromSegment";


	private static final String SELECT_LOWER_BOUND = "selectLowSegBound";


	private static final String SELECT_UPPER_BOUND = "selectUpSegBound";


	private static final String SELECT_MIN = "selectMinFromSegment";


	private static final String SELECT_METADATA_REQUEST_FILENAME = "selectDocMetadata";


	private static final String SELECT_LABEL_FOR_ENTITY = "selectLabelForEntity";


	/**
	 * The logger used inside
	 */
	protected final Log logger = LogFactory.getLog(this.getClass());


	@Autowired
	@Qualifier("documentServiceConfBean")
	protected DocumentServiceConfBean configuration;


	@Autowired
	protected ResourceContainer resourceContainer;


	/**
	 * To render a given page of the document by highlighting entities. The rendered page is also cached in memory.
	 *
	 * @param pageNumber
	 *            The page to render
	 * @param model
	 *            The model
	 * @param writer
	 *            The writer
	 * @param legendFields
	 *            the legend
	 * @param withStructure
	 *            boolean to tell if returned document needs to be structured or not
	 * @param pages
	 *            The previous pages (can be null, if so, the highlighted pages bean in model will be used)
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	public void highlightPage(final int pageNumber, final ModelMap model, final Writer writer, final LegendFieldsBean legendFields, final boolean withStructure, final HighlightedPagesBean pages)
			throws Exception {

		final HighlightedPagesBean p;
		if (pages == null) {
			p = (HighlightedPagesBean) model.get(Constants.HIGHLIGHTED_PAGES);
		} else {
			p = pages;
		}

		@SuppressWarnings("unchecked")
		final HashMap<String, LinkedList<String>> spanEntities = (HashMap<String, LinkedList<String>>) model.get(Constants.SPAN_ENTITIES);
		@SuppressWarnings("unchecked")
		final HashMap<String, String> spanHrefs = (HashMap<String, String>) model.get(Constants.SPAN_HREFS);
		@SuppressWarnings("unchecked")
		final HashMap<String, String> legendUriClassMap = (HashMap<String, String>) model.get(Constants.LEGEND_URI_CLASS_MAP);

		// Create the writer which will be used to construct the html page
		final StringWriter writerStr = new StringWriter();
		final XMLOutputFactory xof = XMLOutputFactory.newInstance();
		final XMLStreamWriter xtw = xof.createXMLStreamWriter(writerStr);

		// Construct the map of legend class names and their associated colours
		// (used to construct the css)
		final HashMap<String, String> legendCssMap = new HashMap<>();
		for (final LegendField legend : legendFields.getFields()) {
			legendCssMap.put(legend.getStyleClassName(), legend.getStyleColor());
		}

		final DocumentRendererBean renderBean = new DocumentRendererBean();
		renderBean.setPagenum(pageNumber);
		renderBean.setRelative(false);
		renderBean.setShowInstanceLink(true);
		renderBean.setShowToolTips(true);
		renderBean.setXtw(xtw);
		renderBean.setSpanEntities(spanEntities);
		renderBean.setSpanHrefs(spanHrefs);
		renderBean.setFullHTML(false);
		renderBean.setLegendCssMap(legendCssMap);
		renderBean.setLegendUriClassMap(legendUriClassMap);
		renderBean.setForceNonNormalised(!withStructure);

		if (this.configuration.isRemoteEndpoint()) {

			final String docURI = (String) model.get(Constants.RESOURCE_URI);
			this.logger.debug("Calling DocumentRenderer with docURI: " + docURI + ", sparqlEndpoint: " + this.configuration.getSparqlEndpointURL() + ", pageNumber: " + pageNumber);

			renderBean.setDocURI(docURI);
			renderBean.setSPARQLEndpoint(this.configuration.getSparqlEndpointURL());

		} else {

			Resource resource = (Resource) model.get("resource");
			if (resource instanceof PieceOfKnowledge) {
				resource = this.getWLResource(getURIFromPoK((PieceOfKnowledge) resource), "");
				// updating model
				this.logger.debug("Current Doc Uri: " + resource.getUri());
				model.addAttribute(Constants.RESOURCE, resource);
			}
			final Document document = (Document) resource;

			this.logger.debug("Calling DocumentRenderer with : " + document.getUri());
			if (!model.containsAttribute(Constants.SEMANTIC_RESOURCE) || (model.get(Constants.SEMANTIC_RESOURCE) == null)) {
				model.addAttribute(Constants.SEMANTIC_RESOURCE, new SemanticResource(document));
			}

			renderBean.setDocURI(document.getUri());
			renderBean.setSemRes((SemanticResource) model.get(Constants.SEMANTIC_RESOURCE));
		}

		DocumentRendererProcessor.getRenderedHtmlRepresentation(renderBean);

		p.getPages().put(Integer.valueOf(pageNumber), replaceJumpLines(writerStr.toString()));
	}


	/**
	 * To get the tooltip bean from a list of entities.
	 *
	 * @param listEnts
	 *            The list of entity URI
	 * @param href
	 *            The target link of the tooltip
	 * @param legendUriClassMap
	 *            The map of legend URI
	 * @param semanticResource
	 *            The semantic resource opened on the document
	 * @param urlMaker
	 *            The url maker
	 * @return The created tooltip
	 */
	public TooltipBean getTooltipDetails(final HashSet<String> listEnts, final String href, final HashMap<String, String> legendUriClassMap, final SemanticResource semanticResource,
			IUrlMaker urlMaker) {

		// Get the information from the triplestore / semantic resource
		// If the user has hovered on an internal href containing a named entity, we want to return both the page number and the list of entities.

		final TooltipBean ttb = new TooltipBean();
		if (href != null) {

			if (href.startsWith("http://Internal_Href_")) {
				// Internal href - No sparql query needed, just substring of href
				final int pageNum = Integer.parseInt(href.split(".html")[0].split("#")[1]);
				ttb.setPageNum(pageNum);
			} else {
				// This is an external href.
				// TODO in the future when sources are available
				// Check to see if the external href has already been treated and set the page accordingly
			}
		}

		final List<NamedEntity> namedEntities = new LinkedList<>();

		if (listEnts != null) {

			final QuerySolutionMap qsmap = new QuerySolutionMap();

			for (final String entity : listEnts) {
				qsmap.add("RESOURCE_1", ResourceFactory.createResource(entity));

				final NamedEntity namedEntity = new NamedEntity();
				final List<String> labels = new LinkedList<>();

				namedEntity.setEntityURI(entity);

				final ParameterizedSparqlString q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_TOOLTIP_ENTITY_DATA_REQUEST_FILENAME, qsmap);
				ResultSet rs;
				if (this.configuration.isRemoteEndpoint()) {
					rs = executeSelectQuery(q, this.configuration.getSparqlEndpointURL());
				} else {
					rs = semanticResource.selectAsJenaResultSet(q.asQuery().toString());
				}
				while (rs.hasNext()) {
					final QuerySolution result = rs.next();

					namedEntity.setEntityClassURI(result.get("type").asResource().toString());
					namedEntity.setStyleClassName(legendUriClassMap.get(result.get("type").asResource().toString()));

					if (result.contains("isCandidate")) {
						namedEntity.setCandidate(result.get("isCandidate").asLiteral().getBoolean());
					}
					if (!labels.contains(result.get("label").asLiteral().getString())) {
						labels.add(result.get("label").asLiteral().getString());
					}
					if (result.contains("takesPlaceAt")) {
						namedEntity.setLocation(result.get("takesPlaceAt").asResource().toString());
					}
					if (result.contains("date")) {
						namedEntity.setDate(result.get("date").asLiteral().getString());
					}
				}

				namedEntity.setEntityLabels(labels);


				if (urlMaker != null && urlMaker.isActive().booleanValue()) {
					namedEntity.setExternalUrl(urlMaker.make(namedEntity.isCandidate(), namedEntity.getEntityClassURI(), namedEntity.getEntityURI(),namedEntity.getEntityLabels().get(0)));
					namedEntity.setEntityMapped(urlMaker.getEntityMapped(namedEntity.getEntityClassURI()));
				}
				namedEntities.add(namedEntity);
			}
		}

		if (namedEntities.isEmpty()) {
			final NamedEntity namedEntity = new NamedEntity();
			namedEntity.setEntityClassURI("No entities found");
			namedEntity.setEntityURI("No entities found");
			final List<String> labels = new LinkedList<>();
			labels.add("No labels found");
			namedEntity.setEntityLabels(labels);

			namedEntities.add(namedEntity);
		}
		ttb.setNamedEntities(namedEntities);
		return ttb;
	}


	/**
	 * To insert a new entity in the document. Insert the corresponding triples in the SPARQL endpoint in the case of a
	 * remote configuration, do nothing otherwise (FOR NOW).
	 *
	 * @param documentURI
	 *            The uri of the document
	 * @param pageNumber
	 *            The page number
	 * @param namedEntity
	 *            The name entity
	 */
	public void insertNamedEntity(final String documentURI, final Integer pageNumber, final NamedEntity namedEntity) {

		if ((namedEntity != null) && (documentURI != null)) {

			// Insert the NamedEntity
			if (this.configuration.isRemoteEndpoint()) {
				final boolean isStructured = this.askIsStructured(documentURI, this.configuration.getSparqlEndpointURL(), null);

				final QuerySolutionMap qsmap = new QuerySolutionMap();
				final String segURI = "http://Seg_" + UUID.randomUUID();

				qsmap.add("RESOURCE_1", ResourceFactory.createResource(documentURI));
				qsmap.add("RESOURCE_2", ResourceFactory.createResource(segURI));
				qsmap.add("RESOURCE_3", ResourceFactory.createResource(namedEntity.getEntityURI()));
				qsmap.add("RESOURCE_4", ResourceFactory.createResource(namedEntity.getEntityClassURI()));
				qsmap.add("LITERAL_1", ResourceFactory.createTypedLiteral(pageNumber));
				qsmap.add("LITERAL_2", ResourceFactory.createTypedLiteral(Integer.valueOf(namedEntity.getStartOffset())));
				qsmap.add("LITERAL_3", ResourceFactory.createTypedLiteral(Integer.valueOf(namedEntity.getEndOffset())));

				// The labels will be added separately if they exist

				ParameterizedSparqlString q;
				if (isStructured) {
					q = this.prepareParameterizedQuery(DocumentViewerService.INSERT_SEGMENT_REQUEST_FILENAME, qsmap);
				} else {
					q = this.prepareParameterizedQuery(DocumentViewerService.INSERT_SEGMENT_NON_STRUCT_REQUEST_FILENAME, qsmap);
				}

				UpdateExecutionFactory.createRemote(q.asUpdate(), this.configuration.getSparqlEndpointURLUpdate()).execute();

				// Need to check if there are several labels associated with this named entity
				// They are only added if they are not duplicates of ones already there.

				final List<String> labels = namedEntity.getEntityLabels();

				for (int i = 0; i < labels.size(); i++) {
					if (labels.get(i) != "") {

						qsmap.add("LITERAL_4", ResourceFactory.createTypedLiteral(labels.get(i)));
						if (isStructured) {
							q = this.prepareParameterizedQuery(DocumentViewerService.ADD_LABELS_TO_SEGMENT_REQUEST_FILENAME, qsmap);
						} else {
							q = this.prepareParameterizedQuery(DocumentViewerService.ADD_LABELS_TO_SEGMENT_NON_STRUCT_REQUEST_FILENAME, qsmap);
						}
						UpdateExecutionFactory.createRemote(q.asUpdate(), this.configuration.getSparqlEndpointURLUpdate()).execute();
					}
				}
				this.logger.debug("Entity inserted");
			} else {
				this.logger.warn("Entity edition is not implemented for non remote endpoints.");
				// TODO we can add a segment on the resource and then ?
			}
		}
	}


	/**
	 * To delete an entity in the document. Remove the corresponding triples in the SPARQL endpoint in the case of a
	 * remote configuration, do nothing otherwise (FOR NOW).
	 *
	 * @param documentURI
	 *            The uri of the document
	 * @param semResource
	 *            The semantic resource opened on the document
	 * @param pageNumber
	 *            The page number
	 * @param namedEntity
	 *            The named entity
	 */
	public void deleteNamedEntity(final String documentURI, final SemanticResource semResource, final Integer pageNumber, final NamedEntity namedEntity) {

		if ((namedEntity != null) && (documentURI != null)) {

			if (this.configuration.isRemoteEndpoint()) {

				// Delete the NamedEntity

				final boolean isStructured = this.askIsStructured(documentURI, this.configuration.getSparqlEndpointURL(), semResource);
				final QuerySolutionMap qsmap = new QuerySolutionMap();

				qsmap.add("RESOURCE_1", ResourceFactory.createResource(documentURI));
				// qsmap.add("RESOURCE_2", ResourceFactory.createResource("http://Seg_" + UUID.randomUUID()));
				qsmap.add("RESOURCE_3", ResourceFactory.createResource(namedEntity.getEntityURI()));
				qsmap.add("RESOURCE_4", ResourceFactory.createResource(namedEntity.getEntityClassURI()));
				qsmap.add("LITERAL_1", ResourceFactory.createTypedLiteral(pageNumber));
				qsmap.add("LITERAL_2", ResourceFactory.createTypedLiteral(Integer.valueOf(namedEntity.getStartOffset())));
				qsmap.add("LITERAL_3", ResourceFactory.createTypedLiteral(Integer.valueOf(namedEntity.getEndOffset())));
				// Note, if the user chooses to delete the resource, we delete all labels

				ParameterizedSparqlString q;
				if (isStructured) {
					q = this.prepareParameterizedQuery(DocumentViewerService.DELETE_SEGMENT_REQUEST_FILENAME, qsmap);
				} else {
					q = this.prepareParameterizedQuery(DocumentViewerService.DELETE_SEGMENT_NON_STRUCT_REQUEST_FILENAME, qsmap);
				}
				UpdateExecutionFactory.createRemote(q.asUpdate(), this.configuration.getSparqlEndpointURLUpdate()).execute();
			} else {
				this.logger.warn("Delete is not implemented for non remote endpoints.");
				// TODO delete segment and then ?
			}
		}
	}


	/**
	 * To get informations (bean) about a list of entities URIs
	 *
	 * @param documentURI
	 *            The uri of the document
	 * @param semResource
	 *            The semantic resource opened on the document
	 * @param pageNumber
	 *            The page number
	 * @param listEnts
	 *            The list of entities URI
	 * @param legendUriClassMap
	 *            The legend map
	 * @return The set of entities
	 */
	@SuppressWarnings("unchecked")
	public Set<NamedEntity> getListNamedEntities(final String documentURI, final SemanticResource semResource, final Integer pageNumber, final HashSet<String> listEnts,
			final HashMap<String, String> legendUriClassMap) {

		if ((listEnts != null) && !listEnts.isEmpty()) {

			final QuerySolutionMap qsmap = new QuerySolutionMap();
			final boolean isStructured = this.askIsStructured(documentURI, this.configuration.getSparqlEndpointURL(), semResource);

			final LinkedList<NamedEntity> llne = new LinkedList<>();

			for (final String entity : listEnts) {

				final NamedEntity namedEntity = new NamedEntity();
				final LinkedList<String> labels = new LinkedList<>();
				String oldStart = "";
				String newStart = "";

				qsmap.add("RESOURCE_1", ResourceFactory.createResource(documentURI));
				qsmap.add("LITERAL_1", ResourceFactory.createTypedLiteral(pageNumber));
				qsmap.add("RESOURCE_2", ResourceFactory.createResource(entity));

				ResultSet rs;
				ParameterizedSparqlString q;
				if (isStructured) {
					q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_NAMED_ENTITY_DATA_REQUEST_FILENAME, qsmap);
					if (this.configuration.isRemoteEndpoint()) {
						rs = executeSelectQuery(q, this.configuration.getSparqlEndpointURL());
					} else {
						rs = semResource.selectAsJenaResultSet(q.asQuery().toString());
					}
				} else {
					q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_NAMED_ENTITY_DATA_NON_STRUCT_REQUEST_FILENAME, qsmap);

					if (this.configuration.isRemoteEndpoint()) {
						rs = executeSelectQuery(q, this.configuration.getSparqlEndpointURL());
					} else {
						rs = semResource.selectAsJenaResultSet(q.asQuery().toString());
					}
				}

				while (rs.hasNext()) {
					final QuerySolution result = rs.next();
					newStart = result.get("start").asLiteral().toString();

					// Each named entity may have several labels
					if (!newStart.equals(oldStart)) {

						// If we've changed segment, there are no more labels for this named entity
						if (!oldStart.equals("")) {

							// Finish writing the old named entity
							namedEntity.setEntityLabels((List<String>) labels.clone());
							final NamedEntity namedEntityCopy = new NamedEntity(namedEntity.getStartOffset(), namedEntity.getEndOffset(), namedEntity.getEntityLabels(), namedEntity.getEntityURI(),
									namedEntity.getEntityClassURI(), namedEntity.isCandidate(), namedEntity.getStyleClassName());
							llne.add(namedEntityCopy);

							// Clear the list of labels
							labels.clear();
						}
						// Start writing the new named entity

						namedEntity.setStartOffset(((Integer) result.get("start").asLiteral().getValue()).intValue());
						namedEntity.setEndOffset(((Integer) result.get("end").asLiteral().getValue()).intValue());
						namedEntity.setEntityURI(entity);
						namedEntity.setEntityClassURI(result.get("type").asResource().toString());
						namedEntity.setStyleClassName(legendUriClassMap.get(result.get("type").asResource().toString()));
						labels.add(result.get("label").asLiteral().getString());
						namedEntity.setCandidate(result.get("isCandidate").asLiteral().getBoolean());

						oldStart = newStart;
					} else {
						// We haven't changed segment, so we're still adding labels to the same named entity
						labels.add(result.get("label").asLiteral().getString());
					}
				}
				// Finish writing the old named entity
				namedEntity.setEntityLabels(labels);

				final NamedEntity namedEntityCopy = new NamedEntity(namedEntity.getStartOffset(), namedEntity.getEndOffset(), namedEntity.getEntityLabels(), namedEntity.getEntityURI(),
						namedEntity.getEntityClassURI(), namedEntity.isCandidate(), namedEntity.getStyleClassName());
				llne.add(namedEntityCopy);
			}
			// Only return unique named entities
			final Set<NamedEntity> hsne = new HashSet<>();
			hsne.addAll(llne);
			return hsne;
		}
		return null;
	}


	/**
	 * Method to get URI from a receive PieceOfKnowledge using POK_PROPERTY_DOC_URI
	 *
	 * @param pok
	 *            The pok to extract URI of the doc from
	 * @return URI if exist null otherwise
	 */
	public String getURIFromPoK(final PieceOfKnowledge pok) {

		if (pok != null) {
			final SemanticResource sem = new SemanticResource(pok);

			final ResultSetMem rs = sem.selectAsJenaResultSet("PREFIX wlr:<" + WebLabRetrieval.NAMESPACE + "> SELECT ?doc WHERE {?hit wlr:isLinkedTo ?doc}");
			if (rs.hasNext()) {
				return rs.next().getResource("doc").getURI();
			}
		}
		return null;
	}


	/**
	 * Method to get translated document from a receive PieceOfKnowledge
	 *
	 * @param pok
	 *            The pok to extract URI of the doc from
	 * @return Document if exist null otherwise
	 */
	public String getTranslatedDocumentFromPoK(final PieceOfKnowledge pok) {

		if (pok != null) {
			final SemanticResource sem = new SemanticResource(pok);

			final ResultSetMem rs = sem.selectAsJenaResultSet("PREFIX wlr:<" + WebLabRetrieval.NAMESPACE + "> SELECT ?doc WHERE {?hit wlr:isLinkedTo ?doc}");

			if (rs.hasNext()) {
				return rs.next().getResource("doc").getURI();
			}
		}
		return null;
	}


	/**
	 * To load a WebLab resource on a repository service.
	 *
	 * @param docURI
	 * @param remoteUser
	 * @return
	 * @throws WebLabCheckedException
	 */
	private Resource getWLResource(final String docURI, final String remoteUser) throws WebLabCheckedException {

		// getting resource from repository
		final LoadResourceArgs args = new LoadResourceArgs();
		args.setResourceId(docURI);

		try {
			return convertToUniqueMuResource(this.resourceContainer.loadResource(args).getResource());
		} catch (final Exception e) {
			throw new WebLabCheckedException("Unable to load document " + docURI + " (" + e.getMessage() + ")", e);
		}
	}


	/**
	 * This method is a temporary dirty patch to handle the multi media units without structure that were not taken into account before
	 * 
	 * @param resource
	 *            The resource to be converted
	 * @return A new resource or the input one if no change is required
	 */
	protected Resource convertToUniqueMuResource(final Resource resource) {

		if (!(resource instanceof Document)) {
			return resource;
		}

		// If there is translation we don't merge the media unit
		final Value<String> availableTranslation = new WProcessingAnnotator(resource).readAvailableTranslation();
		if (availableTranslation.hasValue()) {
			return resource;
		}

		// Do not merge documents with structure annotations
		final Document doc = (Document) resource;
		for (final MediaUnit mu : doc.getMediaUnit()) {
			if (new WStructureAnnotator(mu).readPageNumber().hasValue()) {
				return resource;
			}
		}

		// TODO complete clone of resource + optimize duplicate code

		final List<Text> textList = ResourceUtil.getSelectedSubResources(resource, Text.class);

		if (textList.size() <= 1) {
			return resource;
		}
		if (!(resource instanceof Document)) {
			return resource;
		}
		return mergeMediaUnit(resource, textList);
	}


	/**
	 * This method merge all media unit text from a resource,
	 *
	 * @param resource
	 *            the resource containing all the media unit
	 * @param textList
	 *            represent a list of media unit text
	 * @return the resource with only one media unit text
	 */
	protected Resource mergeMediaUnit(final Resource resource, final List<Text> textList) {

		final Document document = WebLabResourceFactory.createResource("", resource.getUri(), Document.class);
		document.setUri(resource.getUri());

		for (final Annotation annotation : resource.getAnnotation()) {
			final Annotation newAnnotation = WebLabResourceFactory.createAndLinkAnnotation(document);
			newAnnotation.setUri(annotation.getUri());
			newAnnotation.setData(annotation.getData());
		}

		for (final Video mediaUnit : ResourceUtil.getSelectedSubResources(resource, Video.class)) {
			final Video mu = WebLabResourceFactory.createAndLinkMediaUnit(document, Video.class);
			mu.setContent(mediaUnit.getContent());
			mu.setUri(mediaUnit.getUri());

			// TODO Add segments to Video MU
			// List<SpatialSegment> segments = ResourceUtil.getSelectedSegments(mediaUnit, SpatialSegment.class);
			// for (final SpatialSegment segment : segments)
			// {
			// SpatialSegment newSegment = SegmentFactory.createAndLinkSpatioTemporalSegment(document, timestamp, shape);
			// newSegment.setUri(segment.getUri());
			// }

			final List<Annotation> annotations = mediaUnit.getAnnotation();
			for (final Annotation annotation : annotations) {
				final Annotation newAnnotation = WebLabResourceFactory.createAndLinkAnnotation(mu);
				newAnnotation.setUri(annotation.getUri());
				newAnnotation.setData(annotation.getData());
			}
		}

		for (final Audio mediaUnit : ResourceUtil.getSelectedSubResources(resource, Audio.class)) {
			final Audio mu = WebLabResourceFactory.createAndLinkMediaUnit(document, Audio.class);
			mu.setContent(mediaUnit.getContent());
			mu.setUri(mediaUnit.getUri());
			final List<TemporalSegment> segments = ResourceUtil.getSelectedSegments(mediaUnit, TemporalSegment.class);
			for (final TemporalSegment segment : segments) {
				final TemporalSegment newSegment = SegmentFactory.createAndLinkTemporalSegment(mu, segment.getStart(), segment.getEnd());
				newSegment.setUri(segment.getUri());
			}
			final List<Annotation> annotations = mediaUnit.getAnnotation();
			for (final Annotation annotation : annotations) {
				final Annotation newAnnotation = WebLabResourceFactory.createAndLinkAnnotation(mu);
				newAnnotation.setUri(annotation.getUri());
				newAnnotation.setData(annotation.getData());
			}
		}

		final Text composedText = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);
		composedText.setContent("");
		for (final Text mediaUnit : textList) {
			final String content = composedText.getContent().isEmpty() ? "" : composedText.getContent() + "\n";
			final int intervalFromStart = content.length();

			composedText.setContent(content + mediaUnit.getContent() + "\n");

			final List<LinearSegment> segments = ResourceUtil.getSelectedSegments(mediaUnit, LinearSegment.class);
			for (final LinearSegment segment : segments) {
				final LinearSegment newSegment = SegmentFactory.createAndLinkLinearSegment(composedText, segment.getStart() + intervalFromStart, segment.getEnd() + intervalFromStart);
				newSegment.setUri(segment.getUri());
			}

			final List<Annotation> annotations = mediaUnit.getAnnotation();
			for (final Annotation annotation : annotations) {
				final Annotation newAnnotation = WebLabResourceFactory.createAndLinkAnnotation(composedText);
				newAnnotation.setUri(annotation.getUri());
				newAnnotation.setData(annotation.getData());
			}
		}

		for (final Image mediaUnit : ResourceUtil.getSelectedSubResources(resource, Image.class)) {
			final Image mu = WebLabResourceFactory.createAndLinkMediaUnit(document, Image.class);
			mu.setContent(mediaUnit.getContent());
			mu.setUri(mediaUnit.getUri());

			// TODO Add segments to Image MU
			// List<SpatialSegment> segments = ResourceUtil.getSelectedSegments(mediaUnit, SpatialSegment.class);
			// for (final SpatialSegment segment : segments)
			// {
			// SpatialSegment newSegment = SegmentFactory.createAndLinkTSpatialSegment(mu, segment.getCoordinate());
			// newSegment.setUri(segment.getUri());
			// }

			final List<Annotation> annotations = mediaUnit.getAnnotation();
			for (final Annotation annotation : annotations) {
				final Annotation newAnnotation = WebLabResourceFactory.createAndLinkAnnotation(mu);
				newAnnotation.setUri(annotation.getUri());
				newAnnotation.setData(annotation.getData());
			}
		}
		return document;
	}


	/**
	 * To prepare SPARQL query
	 *
	 * @param queryName
	 *            The name of the query file to be loaded
	 * @param qsmap
	 *            The solution map to be used to enrich the loaded template
	 * @return The enriched template query
	 */
	protected ParameterizedSparqlString prepareParameterizedQuery(final String queryName, final QuerySolutionMap qsmap) {

		final ParameterizedSparqlString q = new ParameterizedSparqlString(this.getQueryString(queryName), qsmap, PrefixMapping.Factory.create().setNsPrefixes(this.configuration.getPrefixMap()));

		if (this.logger.isTraceEnabled()) {
			this.logger.trace(q);
		}
		return q;
	}


	private boolean askIsStructured(final String docURI, final String SPARQLEndpoint, final SemanticResource semResource) {

		/*
		 * Ask if the given document uri has structure Page (i.e. if it is normalised)
		 */
		final QuerySolutionMap qsmap = new QuerySolutionMap();
		qsmap.add("RESOURCE_1", ResourceFactory.createResource(docURI));

		final ParameterizedSparqlString q = this.prepareParameterizedQuery(DocumentViewerService.ASK_IS_STRUCTURED_REQUEST_FILENAME, qsmap);

		if (this.configuration.isRemoteEndpoint()) {
			try (final QueryEngineHTTP qe = QueryExecutionFactory.createServiceRequest(this.configuration.getSparqlEndpointURL(), q.asQuery())) {
				return qe.execAsk();
			}
		}
		return semResource.ask(q.asQuery().toString());
	}


	/**
	 * @param docURI
	 *            The uri of the document
	 * @param semResource
	 *            The semantic resource containing the document (might be null in case of remote endpoint)
	 * @return A riche document metadata bean (iin fact containing only the number of pages)
	 */
	public DocumentMetadataBean getMetadata(final String docURI, final SemanticResource semResource) {

		final QuerySolutionMap qsmap = new QuerySolutionMap();
		qsmap.add("RESOURCE_1", ResourceFactory.createResource(docURI));

		final ParameterizedSparqlString q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_METADATA_REQUEST_FILENAME, qsmap);

		final DocumentMetadataBean meta = new DocumentMetadataBean();
		if (this.configuration.isRemoteEndpoint()) {
			final ResultSet rs;
			try (final QueryEngineHTTP qe = QueryExecutionFactory.createServiceRequest(this.configuration.getSparqlEndpointURL(), q.asQuery())) {
				rs = qe.execSelect();
				if (rs.hasNext()) {
					final QuerySolution s = rs.next();
					meta.setTotalPages(s.getLiteral("nbpages").getInt());
				}
			}
		} else {
			final ResultSet rs = semResource.selectAsJenaResultSet(q.asQuery().toString());
			if (rs.hasNext()) {
				final QuerySolution s = rs.next();
				meta.setTotalPages(s.getLiteral("nbpages").getInt());
			}
		}

		return meta;
	}


	/**
	 * To execute a SELECT query on a remote SPARQL endpoint.
	 *
	 * @param q
	 *            The templated query (with values) to be used
	 * @param sparqlEndpoint
	 *            The endpoint to address the request
	 * @return The in memory result set resulting from the request
	 */
	protected ResultSet executeSelectQuery(final ParameterizedSparqlString q, final String sparqlEndpoint) {

		try (final QueryEngineHTTP qe = QueryExecutionFactory.createServiceRequest(sparqlEndpoint, q.asQuery())) {
			return new ResultSetMem(qe.execSelect());
		}
	}


	/**
	 * To load a SPARQL query from files.
	 * 
	 * @param methodName
	 *            The name of the query file to be loaded
	 * @return The string loaded from the query file.
	 */
	protected String getQueryString(final String methodName) {

		// For a given methodName, the contents of the file gives the query string
		try {
			final String resourceName = DocumentViewerService.REQUESTS_PATH + File.separator + methodName + ".rq";
			this.logger.debug("Searching for: " + resourceName);
			if (this.logger.isTraceEnabled()) {
				this.logger.trace("prepared request: " + IOUtils.toString(this.getClass().getClassLoader().getResourceAsStream(resourceName), "UTF-8"));
			}
			return IOUtils.toString(this.getClass().getClassLoader().getResourceAsStream(resourceName), "UTF-8");

		} catch (final IOException e) {
			this.logger.error("Unable to find request file for this method " + e.getMessage());
		}
		return null;
	}


	/**
	 * @param semDoc
	 *            The semantic resource containing the statements about the document
	 * @return The map of service names and languages available for the document inside the semantic resource
	 */
	public Map<String, String> findTranslationsAvailable(final SemanticResource semDoc) {

		final Map<String, String> mapServiceLang = new HashMap<>();

		final String query = this.getQueryString(DocumentViewerService.SELECT_TRANSLATIONS_AVAILABLE);
		final ResultSetMem rs = semDoc.selectAsJenaResultSet(query.toString());

		// no translation available
		if (!rs.hasNext()) {
			return null;
		}

		while (rs.hasNext()) {
			final QuerySolution qs = rs.nextSolution();
			mapServiceLang.put(qs.getResource("serviceName").getURI(), qs.getLiteral("lang").getString());
		}

		return mapServiceLang;
	}


	/**
	 * @param doc
	 *            The original document
	 * @param semDoc
	 *            The semantic resource on the original document
	 * @param segmentUri
	 *            The uri of translation segment?
	 * @param serviceName
	 *            The name of the translation provider
	 * @return The document with only the mediaunit translated by <code>serviceName</code> from the provided segment
	 */
	public Document buildSubDocument(final Document doc, final SemanticResource semDoc, final String segmentUri, final String serviceName) {

		if (doc == null) {
			return null;
		}

		final int lowerBound = this.findLowerBound(semDoc, segmentUri, serviceName);
		final int upperBound = this.findUpperBound(semDoc, segmentUri, serviceName);

		// find min
		QuerySolutionMap qsmap = new QuerySolutionMap();
		qsmap.add("SEGMENT_1", ResourceFactory.createResource(segmentUri));
		qsmap.add("SERVICE_1", ResourceFactory.createResource(serviceName));
		qsmap.add("LOWER_1", ResourceFactory.createTypedLiteral(Integer.valueOf(lowerBound)));
		qsmap.add("UPPER_1", ResourceFactory.createTypedLiteral(Integer.valueOf(upperBound)));
		ParameterizedSparqlString q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_MIN, qsmap);

		ResultSetMem rs = semDoc.selectAsJenaResultSet(q.toString());

		if (!rs.hasNext()) {
			return null;
		}

		final int min = rs.nextSolution().getLiteral("min").getInt();

		// create sub document
		final Document subDoc = WebLabResourceFactory.createResource("0", "1", Document.class);
		final Text text = WebLabResourceFactory.createAndLinkMediaUnit(subDoc, Text.class);

		qsmap = new QuerySolutionMap();
		qsmap.add("SEGMENT_1", ResourceFactory.createResource(segmentUri));
		qsmap.add("SERVICE_1", ResourceFactory.createResource(serviceName));
		qsmap.add("LOWER_1", ResourceFactory.createTypedLiteral(Integer.valueOf(lowerBound)));
		qsmap.add("UPPER_1", ResourceFactory.createTypedLiteral(Integer.valueOf(upperBound)));
		q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_SUB_DOCUMENT, qsmap);

		rs = semDoc.selectAsJenaResultSet(q.toString());

		// no translation information
		if (!rs.hasNext()) {
			return null;
		}

		while (rs.hasNext()) {
			final QuerySolution qs = rs.nextSolution();

			// build content
			String content = text.getContent();
			if (content == null) {
				content = "";
			}
			text.setContent(content + qs.get("?subText").asLiteral().toString());

			// get segment info
			final LinearSegment seg = new LinearSegment();
			seg.setUri(qs.getResource("translatedSeg").getURI());
			seg.setStart(qs.getLiteral("translatedSegStart").getInt() - min);
			seg.setEnd(qs.getLiteral("translatedSegEnd").getInt() - min);
			text.getSegment().add(seg);

			// find entity
			qsmap = new QuerySolutionMap();
			qsmap.add("SEGMENT_1", ResourceFactory.createResource(seg.getUri()));
			qsmap.add("LOWER_1", ResourceFactory.createTypedLiteral(Integer.valueOf(qs.getLiteral("translatedSegStart").getInt())));
			qsmap.add("UPPER_1", ResourceFactory.createTypedLiteral(Integer.valueOf(qs.getLiteral("translatedSegEnd").getInt())));
			q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_ENTITY_FROM_SEGMENT, qsmap);

			final ResultSetMem rs2 = semDoc.selectAsJenaResultSet(q.toString());
			// semDoc.constructAsRDF("construct {?a ?b ?c} where {?a ?b ?c}", System.out);

			while (rs2.hasNext()) {
				final QuerySolution qs2 = rs2.nextSolution();
				if (qs2.contains("segRef")) {
					// get segment info
					final LinearSegment entitySeg = new LinearSegment();
					entitySeg.setUri(qs2.getResource("seg").getURI());
					entitySeg.setStart(qs2.getLiteral("segStart").getInt() - min);
					entitySeg.setEnd(qs2.getLiteral("segEnd").getInt() - min);

					text.getSegment().add(entitySeg);

					// get entity info
					final URI entityUri = URI.create(qs2.getResource("segRef").getURI());

					final WProcessingAnnotator wpa = new WProcessingAnnotator(text);
					wpa.startInnerAnnotatorOn(URI.create(entitySeg.getUri()));
					wpa.writeRefersTo(entityUri);
					wpa.endInnerAnnotator();
					wpa.startInnerAnnotatorOn(entityUri);

					if (qs2.contains("entType")) {
						wpa.writeType(URI.create(qs2.getResource("entType").getURI()));
					}
					if (qs2.contains("entLabel")) {
						wpa.writeLabel(qs2.getLiteral("entLabel").getString());
					}
					if (qs2.contains("isCandidate")) {
						wpa.writeCandidate(Boolean.valueOf(qs2.getLiteral("isCandidate").getBoolean()));
					}
				}
			}
		}
		return subDoc;
	}


	/**
	 * @param doc
	 *            The original document
	 * @return The document containing only the media unit in original language
	 * @throws WebLabCheckedException
	 *             If something wrong occurs cloning the resource
	 */
	public Document buildSubDocumentSource(final Document doc) throws WebLabCheckedException {

		final Document docSource = (Document) cloneResource(doc);

		// select all translated mu to remove them
		final String query = "PREFIX wlp:<" + WebLabProcessing.NAMESPACE + "> PREFIX dc:<" + DC_11.NAMESPACE + "> PREFIX model:<" + WebLabModel.NAMESPACE
				+ "> SELECT ?mu WHERE {?mu wlp:isTranslationOf ?mu2. ?doc model:isComposedByMediaUnit ?mu2}";

		final List<String> muURIList = new ArrayList<>();

		final ResultSetMem rs = new SemanticResource(docSource).selectAsJenaResultSet(query);
		while (rs.hasNext()) {
			final QuerySolution qs = rs.nextSolution();
			muURIList.add(qs.get("?mu").asResource().getURI());
		}

		// delete translated mu in source document
		final WStructureAnnotator ws = new WStructureAnnotator(docSource);
		final Iterator<MediaUnit> it = docSource.getMediaUnit().iterator();
		boolean isStructured = false;
		while (it.hasNext()) {
			final MediaUnit mu = it.next();
			if (muURIList.contains(mu.getUri())) {
				it.remove();
				docSource.getMediaUnit().remove(mu);
			}
			ws.startInnerAnnotatorOn(URI.create(mu.getUri()));
			if (ws.readPageNumber().hasValue()) {
				isStructured = true;
			}
		}

		final List<Text> textList = ResourceUtil.getSelectedSubResources(docSource, Text.class);
		if ((textList.size() > 1) && !isStructured) {
			return (Document) mergeMediaUnit(docSource, textList);
		}

		return docSource;
	}


	/**
	 * @param doc
	 *            The original document
	 * @param pok
	 *            The pok containing the target language to be selected
	 * @return The document containing only the media unit translated into the provided target language by the provided serviceName
	 * @throws WebLabCheckedException
	 *             If something wrong occurs when cloning the document
	 */
	public Document buildSubDocumentTranslated(final Document doc, final PieceOfKnowledge pok) throws WebLabCheckedException {

		if (pok == null) {
			return null;
		}
		// extract lang and service name from received pok
		String lang = null;
		String serviceName = null;

		final SemanticResource semPok = new SemanticResource(pok);
		// semPok.constructAsRDF("construct {?a ?b ?c} where {?a ?b ?c}", System.out);

		final String query = "PREFIX wlp:<" + WebLabProcessing.NAMESPACE + "> PREFIX dc:<" + DC_11.NAMESPACE
				+ "> SELECT ?lang ?serviceName WHERE {?uri dc:language ?lang. ?uri wlp:isProducedBy ?serviceName}";
		final ResultSetMem rs = semPok.selectAsJenaResultSet(query);
		if (rs.hasNext()) {
			final QuerySolution qs = rs.next();
			serviceName = qs.getResource("serviceName").getURI();
			lang = qs.getLiteral("lang").getString();
		}

		if (StringUtils.isBlank(lang) || StringUtils.isBlank(serviceName)) {
			return null;
		}

		// clean document to keep only translated media unit
		final QuerySolutionMap qsmap = new QuerySolutionMap();
		qsmap.add("LANG_1", ResourceFactory.createPlainLiteral(lang));
		qsmap.add("SERVICE_1", ResourceFactory.createResource(serviceName));
		final ParameterizedSparqlString q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_MEDIA_UNIT_FROM_LANG_AND_TRANSLATION_SERVICE, qsmap);

		final ResultSetMem rs2 = new SemanticResource(doc).selectAsJenaResultSet(q.toString());

		// no translation information
		if (!rs2.hasNext()) {
			this.logger.warn("No translation Information for Doc Uri " + doc.getUri());
			return null;
		}

		// list all mu uri to keep
		final Set<String> muSet = new HashSet<>();
		while (rs2.hasNext()) {
			final QuerySolution qs = rs2.nextSolution();
			muSet.add(qs.get("?mu").asResource().getURI());
		}

		// delete mu in translated document
		final Document documentTranslated = (Document) cloneResource(doc);
		final Iterator<MediaUnit> it = documentTranslated.getMediaUnit().iterator();
		while (it.hasNext()) {
			final MediaUnit mu = it.next();
			if (!muSet.contains(mu.getUri())) {
				it.remove();
				documentTranslated.getMediaUnit().remove(mu);
			}
		}

		final List<Text> textList = ResourceUtil.getSelectedSubResources(documentTranslated, Text.class);
		if (textList.size() > 1) {
			return (Document) mergeMediaUnit(documentTranslated, textList);
		}

		return documentTranslated;
	}


	// FIXME another way to do this with crossed streams to avoid copying all the resource in memory
	private Resource cloneResource(final Resource resource) throws WebLabCheckedException {

		final WebLabMarshaller wlm = new WebLabMarshaller();
		byte[] data = null;
		// output clean document
		try (final ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			wlm.marshalResource(resource, out);
			// rewrite clean document
			data = out.toByteArray();
		} catch (@SuppressWarnings("unused") final IOException ioe) {
			// Exception on close. Don't care.
		}

		Resource res = null;
		try (final ByteArrayInputStream istream = new ByteArrayInputStream(data)) {
			res = wlm.unmarshal(istream, resource.getClass());
		} catch (@SuppressWarnings("unused") final IOException ioe) {
			// Exception on close. Don't care.
		}
		return res;
	}


	private int findLowerBound(final SemanticResource semDoc, final String segmentUri, final String serviceName) {

		final QuerySolutionMap qsmap = new QuerySolutionMap();
		qsmap.add("SEGMENT_1", ResourceFactory.createResource(segmentUri));
		qsmap.add("SERVICE_1", ResourceFactory.createResource(serviceName));

		final ParameterizedSparqlString q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_LOWER_BOUND, qsmap);

		int lowerBound = -1;
		final ResultSetMem rs = semDoc.selectAsJenaResultSet(q.toString());
		while (rs.hasNext()) {
			final QuerySolution qs = rs.nextSolution();
			lowerBound = qs.getLiteral("startSeg").getInt();
		}

		return lowerBound;
	}


	private int findUpperBound(final SemanticResource semDoc, final String segmentUri, final String serviceName) {

		final QuerySolutionMap qsmap = new QuerySolutionMap();
		qsmap.add("SEGMENT_1", ResourceFactory.createResource(segmentUri));
		qsmap.add("SERVICE_1", ResourceFactory.createResource(serviceName));

		final ParameterizedSparqlString q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_UPPER_BOUND, qsmap);

		int upperBound = -1;
		final ResultSetMem rs = semDoc.selectAsJenaResultSet(q.toString());
		while (rs.hasNext()) {
			final QuerySolution qs = rs.nextSolution();
			upperBound = qs.getLiteral("endSeg").getInt();
		}

		return upperBound;
	}


	/**
	 * @param html
	 *            The input html file
	 * @return Replace newline characters by break line html code
	 */
	protected String replaceJumpLines(final String html) {

		final StringBuilder sb = new StringBuilder(html);

		final int start = html.indexOf("</style>");
		final String escapedContent = html.substring(start, html.length()).replaceAll("\n", "<br />");

		sb.replace(start, html.length(), escapedContent);

		return sb.toString();
	}


	/**
	 * To know if a resource has information about the structure (pages) or not from document directly.
	 *
	 * @param document
	 *            The document to be checked
	 * @return Whether or not the document is structured
	 */
	public boolean checkHasStructure(final Document document) {

		for (final MediaUnit mu : document.getMediaUnit()) {
			if (new WStructureAnnotator(mu).readPageNumber().hasValue()) {
				return true;
			}
		}
		return false;
	}


	public String findLabel(final SemanticResource semDoc, final String entityUri) {

		QuerySolutionMap qsmap = new QuerySolutionMap();
		qsmap.add("RESOURCE_1", ResourceFactory.createResource(entityUri));

		ParameterizedSparqlString q = this.prepareParameterizedQuery(DocumentViewerService.SELECT_LABEL_FOR_ENTITY, qsmap);
		ResultSet rs = semDoc.selectAsJenaResultSet(q.asQuery().toString());

		// no label available
		if (!rs.hasNext()) {
			return null;
		}

		String label = null;

		while (rs.hasNext()) {
			final QuerySolution qs = rs.nextSolution();
			label = qs.getLiteral("label").getString();
		}
		return label;
	}
}
