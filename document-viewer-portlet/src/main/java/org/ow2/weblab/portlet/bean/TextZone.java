/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a text zone. A text zone reference a entities list.
 *
 * @author emilien
 */
public class TextZone {


	private int textStartOffset;


	private int textEndOffset;


	private List<NamedEntity> entitiesList;


	/**
	 * public constructor with parameters.
	 *
	 * @param textStartOffset
	 *            The starting offset of the text zone has in {@link String#substring(int, int)}
	 * @param textEndOffset
	 *            The end offset of text zone has in {@link String#substring(int, int)}
	 */
	public TextZone(final int textStartOffset, final int textEndOffset) {
		this.textStartOffset = textStartOffset;
		this.textEndOffset = textEndOffset;
		this.entitiesList = new ArrayList<>();
	}


	/**
	 * test if a text zone equal this text zone by offset.
	 *
	 * @param tz
	 *            The text zone
	 * @return true if tz offset equal this offset.
	 */
	public boolean equals(final TextZone tz) {
		return ((tz.textStartOffset == this.textStartOffset) && (tz.textEndOffset == this.textEndOffset));
	}


	/**
	 * check a named entity is referenced in this text zone.
	 *
	 * @param ne
	 *            the NamedEntity.
	 * @return true if the zone reference ne.
	 */
	public boolean reference(final NamedEntity ne) {
		final int neStart = ne.getStartOffset();
		final int neEnd = ne.getEndOffset();
		return (((neStart < this.textStartOffset) && (neEnd > this.textStartOffset)) || ((neStart >= this.textStartOffset) && (neStart < this.textEndOffset)));
	}


	/**
	 * check if the text zone is an overlap zone (at least two entities referenced).
	 *
	 * @return true if the text zone reference at least two entities.
	 */
	public boolean isOverLap() {
		if (this.entitiesList.size() > 1) {
			return true;
		}
		return false;
	}


	/**
	 * check if the text zone reference at least one entity.
	 *
	 * @return true if the text zone reference at least one entity.
	 */
	public boolean referenceEntities() {
		return this.entitiesList.size() != 0;
	}


	/**
	 * get the first entity referenced by the text zone.
	 *
	 * @return the first entity referenced by the text zone.
	 */
	public NamedEntity getEntityReference() {
		return this.entitiesList.get(0);
	}


	/**
	 * @return the textStartOffset
	 */
	public int getTextStartOffset() {
		return this.textStartOffset;
	}


	/**
	 * @param textStartOffset
	 *            the textStartOffset to set
	 */
	public void setTextStartOffset(final int textStartOffset) {
		this.textStartOffset = textStartOffset;
	}


	/**
	 * @return the textEndOffset
	 */
	public int getTextEndOffset() {
		return this.textEndOffset;
	}


	/**
	 * @param textEndOffset
	 *            the textEndOffset to set
	 */
	public void setTextEndOffset(final int textEndOffset) {
		this.textEndOffset = textEndOffset;
	}


	/**
	 * @return the entitiesList
	 */
	public List<NamedEntity> getEntitiesList() {
		return this.entitiesList;
	}


	/**
	 * @param entitiesList
	 *            the entitiesList to set
	 */
	public void setEntitiesList(final List<NamedEntity> entitiesList) {
		this.entitiesList = entitiesList;
	}

}