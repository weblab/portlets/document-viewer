/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.util.List;


/**
 * Bean representing data necessary to display the tooltips
 *
 * @author esther
 *
 */
public class TooltipBean {


	private List<NamedEntity> namedEntities;


	private int pageNum;


	 

	/**
	 * @return the namedEntities
	 */
	public List<NamedEntity> getNamedEntities() {
		return this.namedEntities;
	}


	/**
	 * @param namedEntities
	 *            the namedEntities to set
	 */
	public void setNamedEntities(final List<NamedEntity> namedEntities) {
		this.namedEntities = namedEntities;
	}


	/**
	 * @return the pageNum
	 */
	public int getPageNum() {
		return this.pageNum;
	}


	/**
	 * @param pageNum
	 *            the pageNum to set
	 */
	public void setPageNum(final int pageNum) {
		this.pageNum = pageNum;
	}

}
