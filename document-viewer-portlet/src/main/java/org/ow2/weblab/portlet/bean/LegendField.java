/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.util.List;

/**
 * Bean representing an field in a DocumentLegend. It contains information about
 * the entity that this field represent. display name, type or color.
 *
 * @author emilien and esther
 *
 */
public class LegendField {


	private String entityType;


	private String matchedValue;


	private String displayName;


	private String styleColor;


	private String styleClassName;


	private List<String> eqClasses;


	private boolean show;


	/**
	 * Public constructor
	 *
	 * @param entityType
	 *            Entity type (For example "http://weblab.eads.com/property/gate/annotation")
	 * @param matchedValue
	 *            value of the entity. (For example "Person")
	 * @param displayName
	 *            value in the legend. (For example "Persons")
	 * @param styleColor
	 *            Color of the entity in the legend.
	 * @param styleClassName
	 *            style class name used for style elements.
	 * @param show
	 *            show entity or not in document using this legend.
	 */
	public LegendField(final String entityType, final String matchedValue, final String displayName, final String styleColor, final String styleClassName, final boolean show) {
		super();
		this.entityType = entityType;
		this.matchedValue = matchedValue;
		this.displayName = displayName;
		this.styleColor = styleColor;
		this.styleClassName = styleClassName;
		this.show = show;
	}


	/**
	 * Public constructor without parameters.
	 */
	public LegendField() {
		//
	}


	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return this.entityType;
	}


	/**
	 * @param entityType
	 *            the entityType to set
	 */
	public void setEntityType(final String entityType) {
		this.entityType = entityType;
	}


	/**
	 * @return the matchedValue
	 */
	public String getMatchedValue() {
		return this.matchedValue;
	}


	/**
	 * @param matchedValue
	 *            the matchedValue to set
	 */
	public void setMatchedValue(final String matchedValue) {
		this.matchedValue = matchedValue;
	}


	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return this.displayName;
	}


	/**
	 * @param displayName
	 *            the displayName to set
	 */
	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}


	/**
	 * @return the styleColor
	 */
	public String getStyleColor() {
		return this.styleColor;
	}


	/**
	 * @param styleColor
	 *            the styleColor to set
	 */
	public void setStyleColor(final String styleColor) {
		this.styleColor = styleColor;
	}


	/**
	 * @return the styleClassName
	 */
	public String getStyleClassName() {
		return this.styleClassName;
	}


	/**
	 * @param styleClassName
	 *            the styleClassName to set
	 */
	public void setStyleClassName(final String styleClassName) {
		this.styleClassName = styleClassName;
	}


	/**
	 * @return the eqClasses
	 */
	public List<String> getEqClasses() {
		return this.eqClasses;
	}


	/**
	 * @param eqClasses
	 *            the eqClasses to set
	 */
	public void setEqClasses(final List<String> eqClasses) {
		this.eqClasses = eqClasses;
	}


	/**
	 * @return the show
	 */
	public boolean isShow() {
		return this.show;
	}


	/**
	 * @param show
	 *            the show to set
	 */
	public void setShow(final boolean show) {
		this.show = show;
	}

}