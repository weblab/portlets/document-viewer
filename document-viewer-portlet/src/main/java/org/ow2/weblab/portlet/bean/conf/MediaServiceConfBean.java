/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean.conf;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Bean for service configuration
 *
 * @author rgauthier
 *
 */
@Component
public class MediaServiceConfBean {


	/**
	 * List of mime type to be opened as audio
	 */
	protected List<String> audioFormats;


	/**
	 * List of mime type to be opened as video
	 */
	protected List<String> videoFormats;


	/**
	 * List of mime type to be opened as image
	 */
	protected List<String> imageFormats;


	/**
	 * @return the audioFormats
	 */
	public List<String> getAudioFormats() {
		return this.audioFormats;
	}


	/**
	 * @param audioFormats
	 *            the audioFormats to set
	 */
	public void setAudioFormats(final List<String> audioFormats) {
		this.audioFormats = audioFormats;
	}


	/**
	 * @return the videoFormats
	 */
	public List<String> getVideoFormats() {
		return this.videoFormats;
	}


	/**
	 * @param videoFormats
	 *            the videoFormats to set
	 */
	public void setVideoFormats(final List<String> videoFormats) {
		this.videoFormats = videoFormats;
	}


	/**
	 * @return the imageFormats
	 */
	public List<String> getImageFormats() {
		return this.imageFormats;
	}


	/**
	 * @param imageFormats
	 *            the imageFormats to set
	 */
	public void setImageFormats(final List<String> imageFormats) {
		this.imageFormats = imageFormats;
	}

}
