/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * Bean representing highlighted pages and pages information
 *
 * @author emilienbondu
 *
 */
public class HighlightedPagesBean implements Serializable {


	private static final long serialVersionUID = 1L;


	private Map<Integer, String> pages;


	private boolean showTooltips;


	private boolean showEditPanel;


	private boolean showInstanceLink;


	private boolean showAddBasketAction;


	private boolean showPinBasketAction;


	private int pageSize;


	/**
	 * @return the pages
	 */
	public Map<Integer, String> getPages() {

		return this.pages;
	}


	/**
	 * @param pages
	 *            the pages to set
	 */
	public void setPages(final Map<Integer, String> pages) {

		this.pages = pages;
	}


	/**
	 * @return the showTooltips
	 */
	public boolean isShowTooltips() {

		return this.showTooltips;
	}


	/**
	 * @param showTooltips
	 *            the showTooltips to set
	 */
	public void setShowTooltips(final boolean showTooltips) {

		this.showTooltips = showTooltips;
	}


	/**
	 * @return the showEditPanel
	 */
	public boolean isShowEditPanel() {

		return this.showEditPanel;
	}


	/**
	 * @param showEditPanel
	 *            the showEditPanel to set
	 */
	public void setShowEditPanel(final boolean showEditPanel) {

		this.showEditPanel = showEditPanel;
	}


	/**
	 * @return the showInstanceLink
	 */
	public boolean isShowInstanceLink() {

		return this.showInstanceLink;
	}


	/**
	 * @param showInstanceLink
	 *            the showInstanceLink to set
	 */
	public void setShowInstanceLink(final boolean showInstanceLink) {

		this.showInstanceLink = showInstanceLink;
	}


	/**
	 * @return get the showAddBasketAction
	 */
	public boolean isShowAddBasketAction() {

		return this.showAddBasketAction;
	}


	/**
	 * @param showAddBasketAction
	 *            set the showAddBasketAction
	 */
	public void setShowAddBasketAction(final boolean showAddBasketAction) {

		this.showAddBasketAction = showAddBasketAction;
	}


	/**
	 * @return get the showPinBasketAction
	 */
	public boolean isShowPinBasketAction() {

		return this.showPinBasketAction;
	}


	/**
	 * @param showPinBasketAction
	 *            set the showPinBasketAction
	 */
	public void setShowPinBasketAction(final boolean showPinBasketAction) {

		this.showPinBasketAction = showPinBasketAction;
	}


	/**
	 * @return the pageSize
	 */
	public int getPageSize() {

		return this.pageSize;
	}


	/**
	 * @param pageSize
	 *            the pageSize to set
	 */
	public void setPageSize(final int pageSize) {

		this.pageSize = pageSize;
	}
}
