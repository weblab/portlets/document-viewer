/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.service;

import java.util.List;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;


/**
 * @author rgauthier
 */
public class UserService {



	private final List<String> adminRoles;


	/**
	 * @param adminRoles
	 *            List of roles to be considered as admin on this portlet
	 */
	public UserService(final List<String> adminRoles) {

		this.adminRoles = adminRoles;
	}



	/**
	 * @param request
	 *            The portlet request used to retrieve role of the user
	 * @return Whether or not the role of current user could be considered as admin
	 * @throws SystemException
	 *             If something wrong occurs retrieving the roles
	 * @throws PortalException
	 *             If something wrong occurs retrieving the user
	 */
	public boolean hasAdminRole(final PortletRequest request) throws SystemException, PortalException {

		final User user = PortalUtil.getUser(request);

		for (final String role : this.adminRoles) {
			for (final Role userRole : user.getRoles()) {
				if (userRole.getName().equals(role)) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * @return The list of admin roles
	 */
	public List<String> getAdminRoles() {

		return this.adminRoles;
	}

}
