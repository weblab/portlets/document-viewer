/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean.conf;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

/**
 * Bean for service configuration
 *
 * @author emilienbondu
 *
 */
@Component
public class DocumentServiceConfBean {


	private String sparqlEndpointURL;


	private String sparqlEndpointURLUpdate;
	
	
	private boolean remoteEndpoint;


	@Resource(name = "prefixMap")
	private Map<String, String> prefixMap;


	/**
	 * @return the sparqlEndpointURL
	 */
	public String getSparqlEndpointURL() {
		return this.sparqlEndpointURL;
	}


	/**
	 * @param sparqlEndpointURL
	 *            the sparqlEndpointURL to set
	 */
	public void setSparqlEndpointURL(final String sparqlEndpointURL) {
		this.sparqlEndpointURL = sparqlEndpointURL;
	}


	/**
	 * @return the sparqlEndpointURLUpdate
	 */
	public String getSparqlEndpointURLUpdate() {
		return this.sparqlEndpointURLUpdate;
	}


	/**
	 * @param sparqlEndpointURLUpdate
	 *            the sparqlEndpointURLUpdate to set
	 */
	public void setSparqlEndpointURLUpdate(final String sparqlEndpointURLUpdate) {
		this.sparqlEndpointURLUpdate = sparqlEndpointURLUpdate;
	}


	/**
	 * @return the remoteEndpoint
	 */
	public boolean isRemoteEndpoint() {
		return this.remoteEndpoint;
	}


	/**
	 * @param remoteEndpoint
	 *            the remoteEndpoint to set
	 */
	public void setRemoteEndpoint(final boolean remoteEndpoint) {
		this.remoteEndpoint = remoteEndpoint;
	}


	/**
	 * @return The prefix map
	 */
	public Map<String, String> getPrefixMap() {
		return this.prefixMap;
	}


	/**
	 * @param prefixMap
	 *            The prefix map
	 */
	public void setPrefixMap(final Map<String, String> prefixMap) {
		this.prefixMap = prefixMap;
	}

}
