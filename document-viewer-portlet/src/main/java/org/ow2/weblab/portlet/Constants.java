/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet;


/**
 * @author rgauthier
 */
public class Constants {


	public static final String IS_STRUCTURED = "isStructured";


	public static final String IS_TRANSLATION = "isTranslation";


	public static final String IS_SOURCE = "isSource";


	public static final String IS_EMPTY = "isEmptyModel";


	public static final String NEED_RELOAD = "reload";


	public static final String DISPLAY_STRUCTURED = "displayStructured";


	public static final String RESOURCE_URI = "resourceURI";


	public static final String RESOURCE_TITLE = "resourceTitle";


	public static final String RESOURCE = "resource";


	public static final String SEMANTIC_RESOURCE = "semResource";


	public static final String ORIGINAL_RESOURCE = "originalResource";


	public static final String ORIGINAL_SEMANTIC_RESOURCE = "originalSemanticResource";


	public static final String TRANSLATION_RESOURCE = "translationResource";


	public static final String CURRENT_PAGE_NUMBER = "currentPageNumber";


	public static final String TOTAL_PAGE_NUMBER = "totalPageNumber";


	public static final String HIGHLIGHTED_PAGES = "highlightedPages";


	public static final String LEGEND_FIELDS = "legendFields";


	public static final String SPAN_ENTITIES = "spanEntities";


	public static final String SPAN_HREFS = "spanHrefs";


	public static final String LEGEND_URI_CLASS_MAP = "legendUriClassMap";


	public static final String METABEAN = "metaBean";


	public static final String MEDIA_VIEWER_INFO = "mediaViewerInfo";


	public static final String ACTION_MAPPING_SELECT_VALIDATED_ENTITY = "selectValidatedEntity";


	public static final String ACTION_MAPPING_SELECT_CANDIDATE_ENTITY = "selectCandidateEntity";


	public static final String KDB_DOWN = "kdbDown";
	
	public static final String URL_MAKER_ACTIVE= "isExternalUrlMakerActive";
	

}
