/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.bean;

import org.ow2.weblab.core.helper.impl.SemanticResource;

/**
 * @author rgauthier
 */
public class MediaServiceBean {


	protected SemanticResource resource;


	protected boolean audio = false;


	protected boolean video = false;


	protected boolean image = false;


	protected String type;


	protected String nativeContentPath;


	protected String format;


	/**
	 * @return the resource
	 */
	public SemanticResource getResource() {

		return this.resource;
	}


	/**
	 * @param resource
	 *            the resource to set
	 */
	public void setResource(final SemanticResource resource) {

		this.resource = resource;
	}


	/**
	 * @return the audio
	 */
	public boolean isAudio() {

		return this.audio;
	}


	/**
	 * @param audio
	 *            the audio to set
	 */
	public void setAudio(final boolean audio) {

		this.audio = audio;
	}


	/**
	 * @return the video
	 */
	public boolean isVideo() {

		return this.video;
	}


	/**
	 * @param video
	 *            the video to set
	 */
	public void setVideo(final boolean video) {

		this.video = video;
	}


	/**
	 * @return the image
	 */
	public boolean isImage() {

		return this.image;
	}


	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(final boolean image) {

		this.image = image;
	}


	/**
	 * @return the type
	 */
	public String getType() {

		return this.type;
	}


	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(final String type) {

		this.type = type;
	}


	/**
	 * @return the nativeContentPath
	 */
	public String getNativeContentPath() {

		return this.nativeContentPath;
	}


	/**
	 * @param nativeContentPath
	 *            the nativeContentPath to set
	 */
	public void setNativeContentPath(final String nativeContentPath) {

		this.nativeContentPath = nativeContentPath;
	}


	/**
	 * @return the format
	 */
	public String getFormat() {

		return this.format;
	}


	/**
	 * @param format
	 *            the format to set
	 */
	public void setFormat(final String format) {

		this.format = format;
	}

}
