/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.io.Serializable;

/**
 * Bean representing page metadata
 *
 * @author esther
 *
 */
public class DocumentMetadataBean implements Serializable {


	private static final long serialVersionUID = 1L;


	private int totalPages;


	private String title;


	/**
	 * @return the totalPages
	 */
	public int getTotalPages() {
		return this.totalPages;
	}


	/**
	 * @param totalPages
	 *            the totalPages to set
	 */
	public void setTotalPages(final int totalPages) {
		this.totalPages = totalPages;
	}


	/**
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}


	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

}
