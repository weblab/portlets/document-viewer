/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * @author flanglois
 *         Creation : 7 août 2018
 */
public class DefaultUrlMaker implements IUrlMaker {


	protected final Log logger = LogFactory.getLog(this.getClass());


	private String defaultUrl = "";


	private Boolean active = Boolean.FALSE;


	/**
	 * Custom constructor providing an url as parameter and setting the active flag to <code>true</code>
	 *
	 * @param defaultUrl
	 *            The default URL of the maker.
	 */
	public DefaultUrlMaker(final String defaultUrl) {

		this.setDefaultUrl(defaultUrl);
		this.setActive(true);
	}


	/**
	 * Default constructor setting active to false
	 */
	public DefaultUrlMaker() {

		this.setActive(false);
	}


	@Override
	public Boolean isActive() {

		return this.active;
	}


	@Override
	public String make(final boolean candidate, final String classUri, final String uri, final String label) {

		if (this.active.booleanValue()) {
			return this.defaultUrl + label;
		}
		return "";
	}


	/**
	 * @return The default url
	 */
	public String getDefaultUrl() {

		return this.defaultUrl;
	}


	/**
	 * @param defaultUrl
	 *            The defaultUrl to set
	 */
	public void setDefaultUrl(final String defaultUrl) {

		this.defaultUrl = defaultUrl;
	}


	/**
	 * @param active
	 *            The active flag to set
	 */
	public void setActive(final boolean active) {

		this.active = Boolean.valueOf(active);
	}


	@Override
	public String getEntityMapped(final String classUri) {

		return classUri;
	}

}
