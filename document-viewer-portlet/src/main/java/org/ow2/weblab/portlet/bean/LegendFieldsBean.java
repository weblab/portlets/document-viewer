/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Bean containing legends fields and information about the legend
 *
 * @author emilienbondu
 *
 */
public class LegendFieldsBean implements Serializable {


	private static final long serialVersionUID = 1L;


	private List<LegendField> fields;


	private boolean editable;


	/**
	 * @return the fields
	 */
	public List<LegendField> getFields() {
		return this.fields;
	}


	/**
	 * @param fields
	 *            the fields to set
	 */
	public void setFields(final List<LegendField> fields) {
		this.fields = fields;
	}


	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return this.editable;
	}


	/**
	 * @param editable
	 *            the editable to set
	 */
	public void setEditable(final boolean editable) {
		this.editable = editable;
	}


}
