/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.utils;

/**
 * @author flanglois
 *         Creation : 8 août 2018
 */
public interface IUrlMaker {


	/**
	 * @return the active status of this url maker
	 */
	Boolean isActive();


	/**
	 * @param candidate
	 *            Whether the named entity was candidate
	 * @param classUri
	 *            The original uri of the class
	 * @param uri
	 *            The current URI of the entity
	 * @param label
	 *            The label of the entity
	 * @return The URL to be open for these parameters
	 */
	String make(boolean candidate, String classUri, String uri, String label);


	/**
	 * @param classUri
	 *            The original class of the entity
	 * @return The target class of the entity after mapping
	 */
	String getEntityMapped(final String classUri);

}
