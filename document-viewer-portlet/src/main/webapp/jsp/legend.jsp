<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.ow2.weblab.portlet.Constants"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<portlet:defineObjects />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="docview_portlet" />

<portlet:resourceURL id="serveUpdateDisplayStructured" var="serveUpdateDisplayStructuredURL">
	<portlet:param name="displayStructured" value="toBeChanged" />
</portlet:resourceURL>

<div id="legend">

	<!-- highlight -->
	<%@include file="./highlight.jsp" %>

	<!--  structured view control -->
	<c:if test="${isStructured}">
		<div id="div_doc_structured" title='<fmt:message key="portlet.switch_unstructured" />' <c:if test="${not displayStructured}">style="display:none;"</c:if> class="candidate_instances_selected" onclick='jQuery.docviewToggle.toggle_doc_structure(false, "${serveUpdateDisplayStructuredURL}");'>
			<fmt:message key="portlet.structured_view" />
		</div>
		<div id="div_doc_unstructured" title='<fmt:message key="portlet.switch_structured" />' <c:if test="${displayStructured}">style="display:none;"</c:if> class="candidate_instances_unselected" onclick='jQuery.docviewToggle.toggle_doc_structure(true, "${serveUpdateDisplayStructuredURL}");'>
			<fmt:message key="portlet.structured_view" />
		</div>
	</c:if>
	
	<!-- validated instance control -->
	<c:if test="${kdbDown == false}">
		<div id="validated_instances_selected" class="validated_instances_selected" onclick="jQuery.docviewInstanceDisplayManager.change_validated_instance_visibility();">
			<fmt:message key="portlet.validated_instances_showing" />
		</div>
	</c:if>

	<!-- candidate instance control -->
	<div id="candidate_instances_selected" class="candidate_instances_selected" onclick="jQuery.docviewInstanceDisplayManager.change_candidate_instance_visibility();">
		<fmt:message key="portlet.candidate_instances_showing" />
	</div>
	
	<div id="doc_view_div_legend_fieldset" class="doc_view_div_legend_fieldset">
		<form id="doc_view_div_legend_fieldset_form" method='post' action="<portlet:actionURL><portlet:param name='action' value='saveLegend'/></portlet:actionURL>" >
			<c:forEach items="${legendFields.fields}" var="field" >
				<div class="legend_field">
					<div class="color_legend ${field.styleClassName }" style="background-color:rgb(${field.styleColor})" ></div>
					<input type="hidden" id="${field.styleClassName }_input_color" name="${field.styleClassName }_input_color" value="${field.styleColor }">
					
					<div class="legend_field_label"><fmt:message key="${field.displayName}" /></div>
					
					<c:if test="${legendFields.editable}">
						<c:choose>
							<c:when test="${field.show }">
								<input class="legend_checkbox" type="checkbox" id="${field.styleClassName }_input_color_checkbox" name="${field.styleClassName }_input_color_checkbox" value="true" checked="checked">	
							</c:when>
							<c:otherwise>
								<input class="legend_checkbox" type="checkbox" id="${field.styleClassName }_input_color_checkbox" name="${field.styleClassName }_input_color_checkbox" value="false" >
							</c:otherwise>
						</c:choose>
						<input type="submit" value="<fmt:message key="portlet.saveLegend" />" />
					</c:if>
				</div>
			</c:forEach>
		</form>
	</div>
	<div id="hideLegend">
		<a href="#" onclick="jQuery('#colright').hide()"><fmt:message key="portlet.hideLegend" /></a>
	</div>
</div>
