<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="docview_portlet" />

<!-- icons definition -->
<c:url value="/images/" var="baseURL"></c:url>

<div>
	<%-- Your tooltip for ${spanID} here.<br /> --%>

	<c:if test="${not empty tooltipBean.namedEntities}">
		<c:forEach items="${tooltipBean.namedEntities}" var="ent"
			varStatus="status">
 
				<portlet:actionURL var="selectCandidateInstance"
					name="selectCandidateEntity">
					<portlet:param name="candidate_instance_uri"
						value="${ent.entityURI }" />
					<portlet:param name="class_uri" value="${ent.entityClassURI }" />
					<portlet:param name="label" value="${ent.entityLabels[0]}" />
				</portlet:actionURL>

				<portlet:actionURL var="selectValidatedInstance"
					name="selectValidatedEntity">
					<portlet:param name="instance_uri" value="${ent.entityURI }" />
					<portlet:param name="class_uri" value="${ent.entityClassURI }" />
				</portlet:actionURL>
 
		 


			<c:choose>
				<c:when test="${ent.candidate}">
					<c:if test="${not isExternalUrlMakerActive}">
						<c:set var="selectInstanceUrl" value="${selectCandidateInstance}" />
					</c:if>
					<c:if test="${ isExternalUrlMakerActive}">
						<c:set var="selectInstanceUrl" value="${ent.externalUrl}" />
					</c:if>
					<c:set var="instanceIcon" value="iconKnowledgeCreate" />
					<c:set var="instanceTitle">
						<fmt:message key='portlet.tooltip.createCandidate' />
					</c:set>
				</c:when>
				<c:otherwise>
					<c:if test="${not isExternalUrlMakerActive}">
						<c:set var="selectInstanceUrl" value="${selectValidatedInstance}" />
					</c:if>
					<c:if test="${ isExternalUrlMakerActive}">
						<c:set var="selectInstanceUrl" value="${ent.externalUrl}" />
					</c:if>
					<c:set var="instanceIcon" value="iconKnowledgeEdit" />
					<c:set var="instanceTitle">
						<fmt:message key='portlet.tooltip.editValidated' />
					</c:set>
				</c:otherwise>
			</c:choose>

			<div class="ui-tooltip-content">
				<div class="tooltip-field"
					title='<c:out value="${instanceTitle}" />'>

					<c:choose>

						<c:when test="${hasAdminRole || !ent.candidate}">
							<c:set var="targetBlank" value=""></c:set>
							<c:if test="${ isExternalUrlMakerActive}">
								<c:set var="targetBlank" value=" target=\"_blank\" "></c:set>
							</c:if>
							<a href="${selectInstanceUrl}" class="underline" ${targetBlank}>
								<span class='<c:out value="${instanceIcon}" />' />
								${ent.entityLabels[0]}
							</a>
						</c:when>

						<c:otherwise>
							${ent.entityLabels[0]}
					</c:otherwise>
					</c:choose>



				</div>
				<div class="tooltip-field">
					<span class="tooltip-field-title"> <fmt:message
							key="portlet.tooltip.candidate" /></span> <span><fmt:message
							key="portlet.tooltip.candidate.${ent.candidate}" /></span>
				</div>
				<div class="tooltip-field">
					<span class="tooltip-field-title"> <fmt:message
							key="portlet.tooltip.type" /></span> <span
						class="${ent.styleClassName}"><fmt:message
							key="portlet.tooltip.type.${ent.entityClassURI.split('#')[1]}" />
					</span>
				</div>
				<div class="tooltip-field">
					<span class="tooltip-field-title"><fmt:message
							key="portlet.tooltip.label" /></span>
					<c:set var="first" scope="page" value="true" />
					<c:forEach items="${ent.entityLabels}" var="label">
						<span> <c:if test="${first != true}">,</c:if> <c:set
								var="first" scope="page" value="false" /> <c:out
								value="${label}" escapeXml="true" />
						</span>
					</c:forEach>
				</div>

				<c:choose>
					<c:when test="${not empty ent.location}">
						<div class="tooltip-field">
							<span class="tooltip-field-title"><fmt:message
									key="portlet.tooltip.location" /></span><span>
								${ent.location.split('#')[1]} </span>
						</div>
					</c:when>
				</c:choose>
				<c:choose>
					<c:when test="${not empty ent.date}">
						<div class="tooltip-field">
							<span class="tooltip-field-title"><fmt:message
									key="portlet.tooltip.date" /></span><span> ${ent.date} </span>
						</div>
					</c:when>
				</c:choose>
			</div>
			<c:if test="${not status.last }">
				<div class="ui-tooltip-content-separator" />
			</c:if>
		</c:forEach>
	</c:if>
	<c:if test="${not empty href}">
		<div id="ui-tooltip-content" class="tooltip-href">
			<c:choose>
				<c:when test="${tooltipBean.pageNum > 0}">
					<portlet:resourceURL var="pageRequest" id="page">
						<portlet:param name="pageNumber" value="${ tooltipBean.pageNum }"></portlet:param>
					</portlet:resourceURL>
					<fmt:message key='portlet.tooltip.internal' />
					<a class="split_position" href="#"
						onclick="jQuery('#doc_view_text_view_ajax').load('${pageRequest}')">${tooltipBean.pageNum}</a>
				</c:when>
				<c:otherwise>
				 
					<fmt:message key='portlet.tooltip.external' />
					<a style="color: #0000FF" url="${href}" href="${href}"
						class="document-href-renderer"> <c:out value="${hrefDecode}"
							escapeXml="true" /></a>
				</c:otherwise>
			</c:choose>
		</div>
	</c:if>
</div>