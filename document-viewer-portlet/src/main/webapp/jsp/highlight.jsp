<div class="highlight">
	<div class="topHighlight">
		<input id="highlightInput" type="search">
		<button type="button" class="clearButton" data-search="clear" title="<fmt:message key='portlet.highlight.clear' />"></button>
	</div>
	<div class="bottomHighlight">
		<button type="button" class="previousButton" data-search="prev">
			<fmt:message key='portlet.highlight.previous' />
		</button>
		<button type="button" class="nextButton" data-search="next">
			<fmt:message key='portlet.highlight.next' />
		</button>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery.docviewHighlight.init();
	});
</script>