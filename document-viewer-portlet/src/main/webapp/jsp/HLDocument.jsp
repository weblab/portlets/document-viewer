<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://org.ow2.weblab/taglib" prefix="weblab"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- <% pageContext.setAttribute("newLineChar", "\n"); %> --%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<portlet:defineObjects />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="docview_portlet" />

<portlet:actionURL var="add_doc_to_basket_action">
	<portlet:param name="action" value="addToBasket"></portlet:param>
</portlet:actionURL>

<portlet:actionURL var="pin_doc_to_basket_action">
	<portlet:param name="action" value="pinToBasket"></portlet:param>
</portlet:actionURL>

<portlet:resourceURL id="serveTooltip" var="tooltip_url">
	<portlet:param name="spanID" value="toBeChanged" />
</portlet:resourceURL>

<portlet:resourceURL id="servePopupTranslate" var="translationPopup_url">
	<portlet:param name="spanID" value="toBeChanged" />
</portlet:resourceURL>

<portlet:resourceURL id="serveEditPanel" var="editPanel_url">
	<portlet:param name="startOffset" value="startOffsetToBeChanged" />
	<portlet:param name="endOffset" value="endOffsetToBeChanged" />
	<portlet:param name="highlightedText"
		value="highlightedTextToBeChanged" />
	<portlet:param name="spanID" value="spanIDToBeChanged" />
</portlet:resourceURL>

<portlet:resourceURL id="hrefRequest" var="href_url">
	<portlet:param name="spanID" value="hrefspanIDToBeChanged"></portlet:param>
</portlet:resourceURL>

<portlet:resourceURL var="mediaPlayerUrl" escapeXml="false"
	id="mediaPlayer" />

<!-- icons definition -->
<c:url value="/images/" var="baseURL"></c:url>

<portlet:resourceURL var="resourceURL_legend" id="legend" />

<div id="doc_view_text_view">

	<!-- Document title -->
	<div id='doc_view_document_title' class='log_data'>
		<weblab:getPropertyValues uri="http://purl.org/dc/elements/1.1/title"
			var="currentDocTitle" />
		<div id="showTitle">
			<c:choose>
				<c:when test="${!empty currentDocTitle}">
					<c:forEach var="docTitle" items="${currentDocTitle}">
						<c:out value="${docTitle}" escapeXml="true" />
						<c:if test="${status.index<currentDocTitle.size()-1 }">
							</br>
						</c:if>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<weblab:getPropertyValues
						uri="http://weblab.ow2.org/core/1.2/ontology/processing#hasOriginalFileName"
						var="currentDocFileName" />
					<c:choose>
						<c:when test="${!empty currentDocFileName}">
							<c:out value="${currentDocFileName[0]}" escapeXml="true" />
						</c:when>
						<c:otherwise>
							<fmt:message key="portlet.no_doc_title" />
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>

			<div id="showLegend">
				<c:if test="${highlightedPages.showPinBasketAction}">
					<i id="doc_view_pin_document" class="fa fa-thumb-tack"
						title="<fmt:message key="portlet.pin_basket" />"></i>
				</c:if>
				<c:if test="${highlightedPages.showAddBasketAction}">
					<i id="doc_view_add_document" class="fa fa-shopping-basket"
						title="<fmt:message key="portlet.add_basket" />"></i>
				</c:if>
				<i class="fa fa-plus" onclick='jQuery("#colright").toggle();'
					title="<fmt:message key="portlet.show_menu" />"></i>
			</div>
		</div>
	</div>

	<c:set var="content1"
		value="${highlightedPages.pages[currentPageNumber]}" />

	<div id="mediaPlayer"></div>

	<div id="htmlContent" class="structuredDocument"
		<c:if test="${textStartRight == true}">style="direction:rtl; word-break: keep-all;"</c:if>>${highlightedPages.pages[currentPageNumber]}</div>

	<!-- pages navigation -->
	<c:if test="${metaBean.totalPages>0}">
		<div id="doc_view_portlet_top_result_navigation">
			<c:forEach begin="1" end="${metaBean.totalPages}" varStatus="status">
				<portlet:resourceURL var="pageRequest" id="page">
					<portlet:param name="pageNumber" value="${status.index}"></portlet:param>
				</portlet:resourceURL>
				<c:choose>
					<c:when test="${currentPageNumber==status.index}">
						<a class="split_current_position" href="#"
							onclick="docViewChangePage('${pageRequest}')">${status.index}</a>
					</c:when>
					<c:otherwise>
						<a class="split_position" href="#"
							onclick="docViewChangePage('${pageRequest}')">${status.index}</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>
	</c:if>
</div>

<div id="dialog-confirm-pin" style="display: none;">
	<fmt:message key="portlet.confirmPinToBasket" />
</div>
<div id="dialog-confirm-add" style="display: none;">
	<fmt:message key="portlet.confirmAddToBasket" />
</div>

<div id="change_page_loading_modal" style="display: none;">
	<div style="text-align:center;margin:20px;">
		<fmt:message key="portlet.tooltip.loading" />
	</div>
	<div class="loading"></div>
</div>

<script type="text/javascript">

	function docViewChangePage(url) {
		
		window.scrollTo(0, 0);
		
		// popup loading waiting to reload after submit
		var changePageLoadingModal = jQuery("#change_page_loading_modal");
		changePageLoadingModal.dialog({
			autoOpen : false,
			modal : true,
			dialogClass : "dlg-no-title",
			draggable : false,
			resizable : false
		});
		
		changePageLoadingModal.parent().find(".ui-dialog-titlebar").hide();
		changePageLoadingModal.dialog("open");
		
		jQuery('#doc_view_text_view_ajax').load(url, {}, function(){
			changePageLoadingModal.dialog("close");
		});
	};
	
	jQuery(document).ready(function() {
		
		jQuery("#doc_view_legend_view_ajax").load("${resourceURL_legend}", function(){
			
			if (!jQuery.docviewInstanceDisplayManager.candidate_instances_show) {
				jQuery.docviewInstanceDisplayManager.hide_candidate_instance();
			}

			if (!jQuery.docviewInstanceDisplayManager.validated_instances_show) {
				jQuery.docviewInstanceDisplayManager.hide_validated_instance();
			}
		});
		
		jQuery("#mediaPlayer").load("${mediaPlayerUrl}");

		<c:if test="${highlightedPages.showEditPanel}">
		SelectorManager("${editPanel_url}", "${href_url}");
		</c:if>

		<c:if test="${highlightedPages.showTooltips}">
		TooltipManager("${tooltip_url}");
		</c:if>

		TranslationPopupManager("${translationPopup_url}");
		
		if("${highlightedPages.showPinBasketAction}" === "true"){
			jQuery("#doc_view_pin_document").click(function(){
			    // Define the Dialog and its properties.
			    jQuery("#dialog-confirm-pin").dialog({
			        resizable: false,
			        modal: true,
			        autoOpen: true,
			        title: '<fmt:message key="portlet.confirm.add" />',
			        height: 150,
			        width: 300,
			        buttons: {
			            '<fmt:message key="portlet.confirm.add" />': function () {
			                $(this).dialog('close');
			                window.location.href = '${pin_doc_to_basket_action}';
			            },
			            '<fmt:message key="portlet.confirm.cancel" />': function () {
			                $(this).dialog('close');
			                return;
			            }
			        }
			    });
			});
		}
		
		if("${highlightedPages.showAddBasketAction}" === "true"){
			jQuery("#doc_view_add_document").click(function(){
			    // Define the Dialog and its properties.
			    jQuery("#dialog-confirm-add").dialog({
			        resizable: false,
			        modal: true,
			        autoOpen: true,
			        title: '<fmt:message key="portlet.confirm.add" />',
			        height: 150,
			        width: 300,
			        buttons: {
			            '<fmt:message key="portlet.confirm.add" />': function () {
			                $(this).dialog('close');
			                window.location.href = '${add_doc_to_basket_action}';
			            },
			            '<fmt:message key="portlet.confirm.cancel" />': function () {
			                $(this).dialog('close');
			                return;
			            }
			        }
			    });
			});
		}
		
		// switch to unstructured if needed
		if(${not displayStructured}) {
			jQuery.docviewToggle.unstructure_doc();
		}
	});
</script>
