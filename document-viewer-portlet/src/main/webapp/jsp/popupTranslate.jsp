<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="docview_portlet" />

<portlet:resourceURL id="serveTooltip" var="tooltip_url">
	<portlet:param name="spanID" value="toBeChanged" />
</portlet:resourceURL>

<!-- icons definition -->
<c:url value="/images/" var="baseURL"></c:url>

<div id="dialog1" title="Basic dialog">
	<div class="dialogify">
		<c:forEach items="${availableLangs}" var="lang">
			<div style="margin-bottom: 4px; margin-top: 4px;">
				<div style="font-weight: bold;">${lang.value}</div>
				<div>${mapHiddenHighlightedPages[lang.key].pages[currentPageNumber]}</div>
			</div>
		</c:forEach>
	</div>
</div>

<script type="text/javascript">
	TooltipManager("${tooltip_url}");
</script>