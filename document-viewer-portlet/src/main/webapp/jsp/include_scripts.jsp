<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<c:url value="js/mark.min.js" var="markURL"></c:url>

<script type="text/javascript" src="${markURL }"></script>