<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


<fmt:setLocale value="${pageContext.request.locale}" />

<fmt:setBundle basename="docview_portlet" />

<script type="text/javascript">

<c:choose>
	<c:when test="${no_doc}">
		jQuery("#doc_view_legend_view_ajax").parent().replaceWith("<div class='contenu_portlet'><span class='portlet-msg-info'><fmt:message key='portlet.no_doc' /></span></div>");
	</c:when>
	<c:when test="${page_unavailable}">
		jQuery("#doc_view_legend_view_ajax").parent().replaceWith("<div class='contenu_portlet'><span class='portlet-msg-info'><fmt:message key='portlet.page_unavailable' /></span></div>");
	</c:when>
	<c:when test="${resource_processing_error}">
		jQuery("#doc_view_legend_view_ajax").parent().replaceWith("<div class='contenu_portlet'><span class='portlet-msg-error'><fmt:message key='portlet.processing.error' /></span></div>");
	</c:when>
	<c:otherwise>
		jQuery("#doc_view_legend_view_ajax").parent().replaceWith("<div class='contenu_portlet'><span class='portlet-msg-error'>${message_error}</span></div>");
	</c:otherwise>
</c:choose>
</script>