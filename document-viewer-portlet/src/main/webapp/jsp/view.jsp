<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<portlet:defineObjects />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="docview_portlet" />

<%@include file="./include_scripts.jsp" %>

<!-- icons definition -->
<c:url value="/images/" var="baseURL"></c:url>

<div id="docview_content" class='contenu_portlet scroll'>
	<c:choose>
		<c:when test="${isEmptyModel}">
			<div class="portlet-msg-info">
				<fmt:message key="portlet.no_doc" />
			</div>
		</c:when>
		<c:otherwise>

			<!--  <span class="validated_instances_selected" onclick="jQuery.change_validated_instance_visibility();"><fmt:message key="portlet.instances_showing" /></span>
-->
			<!-- AJAX resource collection -->

			<div id="colleft">
				<div id="doc_view_text_view_ajax">
					<div class="doc_view_portlet_loading_image">
						<img alt="loading the document text..." src="${baseURL}loading_animation.gif">
					</div>
				</div>
			</div>

			<div id="colright">
				<!-- AJAX legend -->
				<div id="doc_view_legend_view_ajax">
					<div class="doc_view_portlet_loading_image">
						<img alt="loading legend..." src="${baseURL}loading_animation.gif">
					</div>
				</div>
			</div>

			<div id="editPanel">
				<!-- Edit Panel -->
			</div>

			<div id="translationPopup" style="display: none;"></div>

		</c:otherwise>
	</c:choose>
</div>

<portlet:resourceURL var="resourceURL_page" id="page">
	<portlet:param name="pageNumber" value="${currentPageNumber }" />
</portlet:resourceURL>

<script type="text/javascript">
	jQuery(document).ready(function() {
		// load only if not already loaded
// 		if($('#doc_view_text_view_ajax').find('div.doc_view_portlet_loading_image').length !== 0) {
		jQuery("#doc_view_text_view_ajax").load("${resourceURL_page}");
// 		}
	});
	
	// Refreshment and the loading
	jQuery(window).load(jQuery.weblabResize.adpaterSizeDocViewPortlet());
	jQuery(window).resize(jQuery.weblabResize.adpaterSizeDocViewPortlet());
	
</script>