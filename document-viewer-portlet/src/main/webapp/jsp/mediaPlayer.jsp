<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://org.ow2.weblab/taglib" prefix="weblab"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="docview_portlet" />

<c:if test="${mediaViewerInfo.type == 'media'}">
	<c:choose>
		<c:when test="${mediaViewerInfo.video == true}">
			<a id="showPlayer" href="#"><fmt:message key="portlet.media.hide" /></a>
			<video style="width: 100%;" id="videoContent" controls="controls">
				<source src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${mediaViewerInfo.nativeContentPath}" type="${mediaViewerService.format}">
			</video>
			<script type="text/javascript" >
				jQuery(document).ready(function() {
					if($('#htmlContent').children('div').children('span').text() == '' && !$('#htmlContent').find('img').length){
						$('#htmlContent').hide();
					}
				});
				jQuery("#showPlayer").on('click', function(){
					jQuery('#videoContent').toggle(function(){
						if (jQuery('#videoContent').is(':hidden')){
							jQuery('#showPlayer').text('<fmt:message key="portlet.media.show" />');
						} else {
							jQuery('#showPlayer').text('<fmt:message key="portlet.media.hide" />');
						}
					});
				});
			</script>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${mediaViewerInfo.audio == true}">
			<audio style="width: 100%;" id="audioContent" controls="controls">
				<source src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${mediaViewerInfo.nativeContentPath}" type="${mediaViewerService.format}">
			</audio>
			<script type="text/javascript" >
				jQuery(document).ready(function() {
					if($('#htmlContent').children('div').children('span').text() == '' && !$('#htmlContent').find('img').length){
						$('#htmlContent').hide();
					}
				});
				jQuery("#showPlayer").on('click', function(){
					jQuery('#videoContent').toggle(function(){
						if (jQuery('#videoContent').is(':hidden')){
							jQuery('#showPlayer').text('<fmt:message key="portlet.media.show" />');
						} else {
							jQuery('#showPlayer').text('<fmt:message key="portlet.media.hide" />');
						}
					});
				});
			</script>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${mediaViewerInfo.image == true}">
			<div style="max-width: 100%;">
				<img id="imageContent" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${mediaViewerInfo.nativeContentPath}" />
			</div>
			<script type="text/javascript" >
				jQuery(document).ready(function() {
					$('#htmlContent').hide();
				});
			</script>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
</c:if>
