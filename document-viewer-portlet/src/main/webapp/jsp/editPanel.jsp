<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="docview_portlet" />

<!-- icons definition -->
<c:url value="/images/" var="baseURL"></c:url>

	<div id="doc_view_portlet_div_edit_panel_box" class="doc_view_portlet_div_edit_panel_box">
		<c:if test="${not empty highlightedText}">	
			<form id="doc_view_div_add_panel_fieldset_form" method='post' action="<portlet:actionURL name='saveAddition' />">
				<div  style="display: none;"><h3><fmt:message key='portlet.addTriplet' /></h3></div>				
				<input type="hidden" name="startOffset" value="${namedEntityToAdd.startOffset}">
				<input type="hidden" name="endOffset" value="${namedEntityToAdd.endOffset}">
				<p><b><fmt:message key='portlet.type' /></b>
				<select id="entityClassURI" name="entityClassURI" class='document-renderer-Person'>
					<c:forEach items="${legendFields.fields}" var="field" >
						<option value="${field.entityType }"  class='document-renderer-<fmt:message key='${field.displayName}' />'><fmt:message key='${field.displayName}' /></option>
					</c:forEach>
				</select></p>
				<p><b><fmt:message key='portlet.contents' /></b> ${highlightedText}</p>
				<input type="hidden" name="contents" value="${highlightedText}">		
				<h4><fmt:message key='portlet.labels' /></h4>
				<div id="edit_panel_label_area">
					<p><input name='entityLabels' type='text' value='${highlightedText}'> <a class='add'><img src='${baseURL}add.png' border='0' alt='<fmt:message key='portlet.addLabel' />'></a></p>
				</div>
				<br />
				<p><input type="submit" value="<fmt:message key='portlet.addTriplet' />"> </p>
			</form>
		</c:if>				
		<c:if test="${not empty listNamedEntities}">
			<c:forEach items="${listNamedEntities}" var="namedEntityToEdit">
				<c:set var="classParts" value="${fn:split(namedEntityToEdit.entityClassURI, '#')}" />
				<div class="${namedEntityToEdit.styleClassName}">
				<c:if test="${namedEntityToAdd.startOffset ge namedEntityToEdit.startOffset && namedEntityToAdd.startOffset le namedEntityToEdit.endOffset}">
				    <h3>${classParts[1]}</h3><br />
				    <h4>Class URI : </h4>
				    <c:out value="${namedEntityToEdit.entityClassURI}" escapeXml="true"/><br />
				    <div  style="display: none;">
					    <c:out value="${namedEntityToEdit.startOffset}" escapeXml="true"/><br />
						<c:out value="${namedEntityToEdit.endOffset}" escapeXml="true"/><br />
					</div>
					<h4>Entity URI :</h4>
					<c:out  value="${namedEntityToEdit.entityURI}" escapeXml="true"/><br />
					<h5>Label(s) :</h5>
						<ul id="edit_panel_label_area">
							<c:forEach items="${namedEntityToEdit.entityLabels}" var="label">
								<li><c:out value="${label}" escapeXml="true"/></li>
							</c:forEach>
						</ul>
					<br />
		   			 <a href="
		   			 	<portlet:actionURL name='saveDelete'>
		   			 		<portlet:param name='entityURI' value='${namedEntityToEdit.entityURI}'/>
		   			 		<portlet:param name='entityClassURI' value='${namedEntityToEdit.entityClassURI}'/>
		   			 		<portlet:param name='entityLabels' value='${namedEntityToEdit.entityLabels}'/>
		   			 		<portlet:param name='startOffset' value='${namedEntityToEdit.startOffset}'/>
		   			 		<portlet:param name='endOffset' value='${namedEntityToEdit.endOffset}'/>
		   			 	</portlet:actionURL>"
		   			 	 onclick="return confirm('<fmt:message key='portlet.confirmDelete' />');">
		   			 	<button type="button"><fmt:message key='portlet.deleteTriplet' /></button>
		   			 </a><br /><br />		
				</c:if>
				</div>
			</c:forEach>
		</c:if>	
	</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	$("#edit_panel_label_area").on("click", ".add", function(e) {
	    // e.delegateTarget is <div id="edit_panel_label_area">
	    $(e.delegateTarget).append("<p><input name='entityLabels' type='text' value='${highlightedText}'> <a class='add'><img src='${baseURL}add.png' border='0' alt='<fmt:message key='portlet.addLabel' />'></a> <a class='remove'><img src='${baseURL}remove.png' border='0' alt='<fmt:message key='portlet.removeLabel' />'></a></p>");
	    // Prevent default action (i.e. don't follow links)
	    e.preventDefault();
	 });
	$('#edit_panel_label_area').on("click", ".remove", function(e) {
	    $(this).parent().remove();
	    // Prevent default action (i.e. don't follow links)
	    e.preventDefault();
	});
});
</script>