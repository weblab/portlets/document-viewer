jQuery.docviewToggle = {

	toggle_doc_structure : function(displayStructured, updateURL) {
		
		var updateURL = updateURL.replace("toBeChanged", displayStructured);
		//console.log(updateURL);
	
		if(displayStructured) {
			jQuery.get(updateURL).success(function() {
				jQuery('#doc_view_portlet_top_result_navigation .split_current_position ').click();
			});
		} else {
			jQuery.get(updateURL).success(function() {
				jQuery.docviewToggle.unstructure_doc();
			});
		}
	},

	unstructure_doc : function() {
		var div_doc_structured = jQuery('#div_doc_structured');
	
		// clean all images
		jQuery("#htmlContent img").remove();
	
		// clean style to display content without style
		jQuery("#htmlContent.structuredDocument > div").removeAttr("style");
		jQuery("#htmlContent.structuredDocument > div").attr("class", "non-structured");
	
		// remove empty span
		jQuery("#htmlContent.structuredDocument > div > span").each(
				function(index, value) {
					var content = jQuery(this).html();
					if (jQuery.trim(content) === "") {
						jQuery(this).remove();
					}
				});
	
		// remove empty div
		jQuery("#htmlContent.structuredDocument > div").each(
				function(index, value) {
					var content = jQuery(this).html();
					if (jQuery.trim(content) === "") {
						jQuery(this).remove();
					}
				});
	
		// display unstructured choice
		jQuery("#div_doc_structured").hide();
		jQuery("#div_doc_unstructured").show();
	}
}