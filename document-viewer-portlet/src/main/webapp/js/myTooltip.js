function TooltipManager(url) {
	console.log('ToolTipManager on url' , url);
	$('[class*="document-renderer-"]').each(function(){
		
		
		console.log('current span-id ', $(this).attr('id'));
		 if($(this).is('span')){
		$(this).qtip({
			position: {
//				my: 'bottom left',
//		    	at: 'top right',
		    	target: $(this),
		    	effect: false,
		    	container: $('div#content')
		    },
		    show: {
		        solo: true
		    },
		    hide: {
		          fixed: true,
		          delay: 100
		    },
		    content: function(event, api) {

	            console.log('content');
	            $.ajax({
	                url: url.replace("toBeChanged", $(this).attr('id'))
	            })
	            .then(function(content) {
	                // Set the tooltip content upon successful retrieval
	            	console.log('content ok ',content);
	                api.set('content.text', content);
	            }, function(xhr, status, error) {
	                // Upon failure... set the tooltip content to the status and error value
	            	console.log('content ko ',error, status);
	            	api.set('content.text', status + ': ' + error);
	            });

	            console.log('content ', content);
	            return '<fmt:message key="portlet.tooltip.loading" />'; // Set some initial text
	        },
	        visible: function(){
	            console.log('visible ');
	        	p = $(this).position();
	        	if (p.top < 0)
	        		p.top = 0; 
	        }
		});
		
		 }
	});
	
	
//	$.widget("gamabit.mouseOverTooltip", $.ui.tooltip, {
//		open: function( event ) {
//			  $(".ui-tooltip").each( function() {
//			      var element = $(this);
//			      element.remove();
//			    } );
//			$(".close-pending").each( function() {
//				var element = $(this);
//				element.addClass("close-force");
//				element.mouseOverTooltip("close");
//			});
//			$.ui.tooltip.prototype.open.call(this, event);
//		},
//				
//		close: function( event ) {
//			// This keeps the tooltip "alive" whilst the user has the mouse either over the original text, or over the tooltip itself
//			if( !this.element.hasClass("close-pending") ) {
//				this.element.addClass("close-pending");
//				var that = this;
//				that._closeTimeoutElement = setTimeout( function() {
//					$.ui.tooltip.prototype.close.call(that, event);
//				},
//				that.options.closeTimeout
//				);
//					
//				var target = $( event ? event.currentTarget : that.element );
//				that._find( target ).mouseenter( function() {
//					clearTimeout( that._closeTimeoutElement );
//				}).
//				mouseleave( function() {
//					that.element.addClass("close-force");
//					that.close();
//				});
//			} else if( this.element.hasClass("close-force") ) {
//				clearTimeout( this._closeTimeoutElement );
//				this.element.removeClass("close-pending");
//				this.element.removeClass("close-force");
//				$.ui.tooltip.prototype.close.call(this, event);
//			}
//		}
//	});
//	
//	// Clicking anywhere except on a tooltip will close them
//	$("#htmlContent.structuredDocument:not(.ui-tooltip)").click(function () {
//	  $(".ui-tooltip").each( function() {
//	      var element = $(this);
//	      element.remove();
//	    } );
//	});
//
//	 
//	$('body').on('mouseOverTooltip', '[class*="document-renderer-"]', function(){
//	// Select everything that has document-renderer- in the class name, (includes named entities in hrefs)
//		items: "span",
//		show: { effect: "fade", duration: 100, delay: 750 },
//        position: ({
//        	my: "center bottom",
//        	at: "center top",
//          }),
//		using: function( position, feedback ) {
//			$( this ).css( position );
//		},
//		content: function(response) {
//			var url2 = url.replace("toBeChanged", $(this).attr('id'));
//			$.get(url2,
//				{},
//				response
//			);
//		},
//        closeTimeout: 100
//	 });
//
//	$('.document-href-renderer').not('[class*="document-renderer-"]').mouseOverTooltip({
//		// Select only hrefs that don't have a named entity in them
//		items: "span",
//		show: { effect: "fade", duration: 500, delay: 750 },
//        position: ({
//        	my: "center bottom",
//        	at: "center top",
//          }),
//		using: function( position, feedback ) {
//			$( this ).css( position );
//		},
//		content: function(response) {
//			var url2 = url.replace("toBeChanged", $(this).attr('id'));
//			$.get(url2,
//				{target:$(this).attr('url')},
//				response
//			);
//		},
//        closeTimeout: 100
//	 });
	
};

