jQuery.docviewHighlight = {
		
		init : function(){
			
			var $input = $("input[type='search']"),
			$clearBtn = $("button[data-search='clear']"),
			$prevBtn = $("button[data-search='prev']"),
			$nextBtn = $("button[data-search='next']"),
			$content = $("#doc_view_text_view"),
			$results,
			currentClass = "current",
			offsetTop = 180,
			currentIndex = 0;

			/**
			 * Jumps to the element matching the currentIndex
			 */
			function jumpTo() {
				if ($results.length) {
					var position, $current = $results.eq(currentIndex);
					$results.removeClass(currentClass);
					if ($current.length) {
						$current.addClass(currentClass);
						position = $current.offset().top - $current.offsetParent().offset().top;
						document.getElementById('docview_content').scrollTop = position;
					}
				}
			}

			/**
			 * Searches for the entered keyword in the
			 * specified context on input
			 */
			$input.on("keyup", function(e) {
				if (jQuery("#highlightInput").val().trim().length > 1) {
					var searchVal = this.value;
					$content.unmark({
						done : function() {
							$content.mark(searchVal, {
								separateWordSearch : true,
								done : function() {
									$results = $content.find("mark");
									currentIndex = 0;
									jumpTo();
								}
							});
						}
					});
				} else {
					$content.unmark();
				}
			});

			/**
			 * Clears the search
			 */
			$clearBtn.on("click", function() {
				$content.unmark();
				$input.val("").focus();
			});

			/**
			 * Next and previous search jump to
			 */
			$nextBtn.add($prevBtn).on("click", function() {
				if ($results.length) {
					currentIndex += $(this).is($prevBtn) ? -1 : 1;
					if (currentIndex < 0) {
						currentIndex = $results.length - 1;
					}
					if (currentIndex > $results.length - 1) {
						currentIndex = 0;
					}
					jumpTo();
				}
			});
		}
}