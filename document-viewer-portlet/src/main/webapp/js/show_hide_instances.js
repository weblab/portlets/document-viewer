jQuery.docviewInstanceDisplayManager = {
	
	candidate_instances_show : true,
	validated_instances_show : true,
	
	hide_candidate_instance : function() {
		jQuery('#htmlContent span.candidate_instance').addClass("candidate_instance_hide");
		jQuery('#candidate_instances_selected').addClass('candidate_instances_unselected');
		jQuery('#candidate_instances_selected').removeClass('candidate_instances_selected');
		jQuery.docviewInstanceDisplayManager.candidate_instances_show = false;
	},
	
	show_candidate_instance : function() {
		jQuery('#htmlContent span.candidate_instance').removeClass("candidate_instance_hide");
		jQuery('#candidate_instances_selected').addClass('candidate_instances_selected');
		jQuery('#candidate_instances_selected').removeClass('candidate_instances_unselected');
		jQuery.docviewInstanceDisplayManager.candidate_instances_show = true;
	},
	
	hide_validated_instance : function() {
		jQuery('#htmlContent span.validated_instance').addClass("validated_instance_hide");
		jQuery('#validated_instances_selected').addClass('validated_instances_unselected');
		jQuery('#validated_instances_selected').removeClass('validated_instances_selected');
		jQuery.docviewInstanceDisplayManager.validated_instances_show = false;
	},
	
	show_validated_instance : function() {
		jQuery('#htmlContent span.validated_instance').removeClass("validated_instance_hide");
		jQuery('#validated_instances_selected').addClass('validated_instances_selected');
		jQuery('#validated_instances_selected').removeClass('validated_instances_unselected');
		jQuery.docviewInstanceDisplayManager.validated_instances_show = true;
	},
	
	change_candidate_instance_visibility : function () {
		if (jQuery.docviewInstanceDisplayManager.candidate_instances_show) {
			jQuery.docviewInstanceDisplayManager.hide_candidate_instance();
		} else {
			jQuery.docviewInstanceDisplayManager.show_candidate_instance();
		}
	},
	
	change_validated_instance_visibility : function () {
		if (jQuery.docviewInstanceDisplayManager.validated_instances_show) {
			jQuery.docviewInstanceDisplayManager.hide_validated_instance();
		} else {
			jQuery.docviewInstanceDisplayManager.show_validated_instance();
		}
	}
}