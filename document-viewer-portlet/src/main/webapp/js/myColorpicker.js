jQuery.ColorPickerManager = {
	entityTypeToChange: null,
	map : Array(),
	colorPickerInit : true,		
	addMapEntry : function (key,value){
		jQuery.ColorPickerManager.map[key]=value;
	},
			
	getMapEntry : function (key) {
		return jQuery.ColorPickerManager.map[key];
	},
	
	changeEntityBackgroundColor : function () {		
		var span = jQuery("."+jQuery.ColorPickerManager.entityTypeToChange+"_legend");
		var curColor = new Array();
		curColor[0]=jQuery.ColorPicker.currentColor.r;
		curColor[1]=jQuery.ColorPicker.currentColor.g;
		curColor[2]=jQuery.ColorPicker.currentColor.b;
		var curColorStr = jQuery.ColorPicker.currentColor.r + ',' + jQuery.ColorPicker.currentColor.g + ',' + jQuery.ColorPicker.currentColor.b;
		var curColorValues ="rgb("+jQuery.ColorPicker.currentColor.r + ',' + jQuery.ColorPicker.currentColor.g + ',' + jQuery.ColorPicker.currentColor.b+")";
		jQuery(span[0]).css("background-color", curColorValues);
		var boxValue=jQuery("#"+jQuery.ColorPickerManager.entityTypeToChange+"_showCheckBox").attr("checked");
		var span = jQuery("."+jQuery.ColorPickerManager.entityTypeToChange);
		var mapColor = new Array();
		mapColor["background"]=curColor;
		mapColor["font"]=jQuery.ColorPickerManager.getFontColor(curColor);
		jQuery.ColorPickerManager.addMapEntry(jQuery.ColorPickerManager.entityTypeToChange,mapColor);
		jQuery("#hidden_color_"+jQuery.ColorPickerManager.entityTypeToChange).attr("value",curColorStr);
		if (boxValue==true) {
			for (var i=0; i<span.length;i++) {
				jQuery(span[i]).css("background-color", curColorValues);
				jQuery(span[i]).css("color", jQuery.ColorPickerManager.getFontColor(curColor));
			}
		}
		jQuery.ColorPickerManager.colorsTab[jQuery.ColorPickerManager.entityTypeToChange]=curColor;			
	},
			
	OpenColorPicker2 : function (entityType,event){
		//jQuery.ColorPicker = new Liferay.ColorPicker( { onClose: jQuery.ColorPickerManager.changeEntityBackgroundColor} ); 
		jQuery.ColorPickerManager.entityTypeToChange = entityType;
		if (jQuery.ColorPickerManager.colorPickerInit) {
			jQuery.ColorPicker.init();
			jQuery.ColorPicker.okFunc=jQuery.ColorPickerManager.changeEntityBackgroundColor;
			jQuery.ColorPicker.cPShow(event);
			colorPickerInit=false;	
		} else {
			jQuery.ColorPicker.okFunc=jQuery.ColorPickerManager.changeEntityBackgroundColor;
			jQuery.ColorPicker.cPShow(event);
		}			
	},
			
	ShowEntity : function(entityType) {
		var span = jQuery("."+entityType);
		var boxValue=jQuery("#"+entityType+"_showCheckBox").attr("checked");
		if (!boxValue) {
			if (span.length>0) {
				var mapColor = new Array();
				var backcol = jQuery(span[0]).css("background-color");
				mapColor["background"]=backcol;
				mapColor["font"]=jQuery.ColorPickerManager.getFontColor(backcol);
				jQuery.ColorPickerManager.addMapEntry(entityType,mapColor);			
				for (var i=0; i<span.length;i++) {
					jQuery(span[i]).css("background-color","");
					jQuery(span[i]).css("color", document.fgColor);
				}
			}
			jQuery.ColorPickerManager.colorsCheckTab[entityType]=false;				
		} else {
			var legendColor = jQuery("."+entityType+"_legend");
			var backcolor = jQuery(legendColor[0]).css("background-color");
			var fontcolor = jQuery.ColorPickerManager.getFontColor(backcolor);
			for (var i=0; i<span.length;i++) {
				jQuery(span[i]).css("background-color",backcolor);
				jQuery(span[i]).css("color", fontcolor);
			}
			jQuery.ColorPickerManager.colorsCheckTab[entityType]=true;
		}
	},
			
	getFontColor : function(bgcolor) {
		var color = bgcolor[0] + bgcolor[1] + bgcolor[2];
		if (color > 3 * 128) {
           	return "black";
      	} else {
          	return "white";
       	}
	}
};