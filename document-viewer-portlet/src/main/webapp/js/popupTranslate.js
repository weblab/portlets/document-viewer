function TranslationPopupManager(ajaxUrl) {

	var currentID;

	$('#htmlContent div').hover(function() {
		if (typeof ($(this).attr('rel')) != "undefined") {
			$(this).toggleClass('translation_available');
		}
	});

	$('#htmlContent div').click(
			function() {

				var id = $(this).attr('id');
				var width = $(this).width() + 30;

				if (!isDialogOpen() || currentID != id) {

					// check if document parts have been identified by the
					// structured-document-renderer in the rel attribute
					if (typeof ($(this).attr('rel')) != "undefined") {

						var ajaxUrl2 = ajaxUrl.replace("toBeChanged", $(this)
								.attr('rel'));
						$.get(ajaxUrl2, function(data) {

							currentID = id;

							if ($("#dialog1"))
								$("#dialog1").dialog('destroy').remove()

							$("#translationPopup").html(data);

							$("#dialog1").dialog({
								dialogClass : 'hideDialogTitle',
								show : {
									effect : "blind",
									duration : 150
								},
								autoOpen : false,
								resizable : false,
								width : width,
								minHeight : 1,
								position : {
									my : "left top",
									at : "left bottom",
									of : $('#' + id)
								},
								modal : false
							});
							$("#dialog1").dialog("open");
						});
					}
				}
			});

	$('body').bind(
			'click',
			function(e) {
				if (isDialogOpen() && !jQuery(e.target).is('.ui-dialog, a')
						&& !jQuery(e.target).closest('.ui-dialog').length) {
					currentID = '';
					$('#dialog1').dialog('close');
				}
			});

	function isDialogOpen() {
		var result = ($("#dialog1")
				&& $("#dialog1").hasClass('ui-dialog-content') && $('#dialog1')
				.dialog('isOpen'));
		return result;
	}
};