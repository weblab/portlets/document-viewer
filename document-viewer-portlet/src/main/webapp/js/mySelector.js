function SelectorManager(editPanelUrl, hrefurl) {

	var curStart = $(),
		curEnd = $(),
		curParent = $(),
		htmlEl = function(el){
			return el.nodeType === 3 || el.nodeType === 4 ?  // 3 is a TEXT_NODE, 4 is a CDATA_SECTION_NODE 
				el.parentNode : el;
		};
		
	// If the user clicks anywhere in the document that isn't a named entity or an href,
	// then they want to either add an entity or close the edit panel
	$('#htmlContent.structuredDocument').not('[class*="document-renderer-"]').not('.document-href-renderer').mouseup(function(){
		
		// Initialise the range variables and get rid of the old classes
		var range = $.Range.current();
		var start = range.start(),
			end = range.end();
		curStart.removeClass('start');
		curEnd.removeClass('end');
		curParent.removeClass('parent')
		var starter = htmlEl(start.container);
		var ender = htmlEl(end.container),
			parenter = htmlEl(range.parent());

		// Add classes, to start and end spans and the parent node
		curStart = $(starter).addClass('start');
		curEnd = $(ender).addClass('end');
		curParent = $(parenter).addClass('parent');
		
		// The start of the segment is in the variable spanstart, use it to determine the offset within the text as a whole
		var segStart = parseInt($(starter).data('spanstart'), 10);
		segStart += start.offset;
		var segEnd = parseInt($(ender).data('spanstart'), 10);
		segEnd += end.offset;

		// Don't let the user add just a space
		var highlightedText = $.trim(range.toString());
		if (!isNaN(segEnd) && !isNaN(segStart) && segStart != segEnd && highlightedText != "") {
				// non empty highlighted text - add a triplet
				var url = editPanelUrl.replace("startOffsetToBeChanged", segStart).replace("endOffsetToBeChanged", segEnd).replace("highlightedTextToBeChanged", encodeURIComponent(highlightedText));
				//jQuery("#editPanel div").remove(); // remove previous content from editPanel
				jQuery("#editPanel *").remove(); // remove previous content from editPanel
				$("#showLegend").click(); // Make sure that the edit panel is visible
				jQuery("#editPanel").load(url);
		} else {
			// bizarre or single click, or blank highlighted text
			//jQuery("#editPanel > div").remove(); // remove previous content from editPanel
			jQuery("#editPanel *").remove(); // remove previous content from editPanel
		}
	})
		

	// Find all classes created by the document renderer program (even if it's in an href)
	$('[class*="document-renderer-"]').mouseup(function(evt){
		
		// Initialise the range variables and get rid of the old classes
		var range = $.Range.current();
		var start = range.start(),
			end = range.end();
		curStart.removeClass('start');
		curEnd.removeClass('end');
		curParent.removeClass('parent')
		var starter = htmlEl(start.container);
		var ender = htmlEl(end.container),
			parenter = htmlEl(range.parent());

		// Add classes, to start and end spans and the parent node
		curStart = $(starter).addClass('start');
		curEnd = $(ender).addClass('end');
		curParent = $(parenter).addClass('parent');
		
		// The start of the segment is in the variable spanstart, use it to determine the offset within the text as a whole
		var segStart = parseInt($(starter).data('spanstart'), 10);
		segStart += start.offset;
		var segEnd = parseInt($(ender).data('spanstart'), 10);
		segEnd += end.offset;
		evt.stopPropagation(); // Important to keep the clicks "local"
		
		if (!isNaN(segEnd) && !isNaN(segStart)) {
			// Don't let the user add just a space
			var highlightedText = $.trim(range.toString());
			
			if (segStart != segEnd && highlightedText != "") {
				// non empty highlighted text - add a triplet
				var url = editPanelUrl.replace("startOffsetToBeChanged", segStart).replace("endOffsetToBeChanged", segEnd).replace("highlightedTextToBeChanged", highlightedText);
				jQuery("#editPanel *").remove(); // remove previous content from editPanel
				$("#showLegend").click(); // Make sure that the edit panel is visible
				jQuery("#editPanel").load(url);
			} else {
				// single click on a named entity / entities - edit it / them (or empty highlighted text)				
				var e = window.event || evt;
				var spanID = e.target.id;//to determine on which span user has clicked.
				// Because it's a single click, segStart and segEnd are the same, ie. click position in the text.
				// The click position will be used to filter which entities apply at this point for editing.
				var url = editPanelUrl.replace("spanIDToBeChanged", spanID).replace("startOffsetToBeChanged", segStart).replace("endOffsetToBeChanged", segEnd).replace("highlightedTextToBeChanged", "");
				jQuery("#editPanel *").remove(); // remove previous content from editPanel
				$("#showLegend").click(); // Make sure that the edit panel is visible
				jQuery("#editPanel").load(url);
			}
		} else {
			// bizarre click
			jQuery("#editPanel *").remove(); // remove previous content from editPanel	
		}
	})

	$('.document-href-renderer').not('[class*="document-renderer-"]').mouseup(function(evt){
	// The user has clicked on an href link that doesn't contain a named entity
		var range = $.Range.current();
		var start = range.start(),
			end = range.end();
		var starter = htmlEl(start.container);
		var ender = htmlEl(end.container),
			parenter = htmlEl(range.parent());

		var segStart = parseInt($(starter).data('spanstart'), 10);
		segStart += start.offset;
		var segEnd = parseInt($(ender).data('spanstart'), 10);
		segEnd += end.offset;
		evt.stopPropagation(); // Important to keep the clicks "local"

		if (!isNaN(segEnd) && !isNaN(segStart)) {
			// Don't let the user add just a space
			var highlightedText = $.trim(range.toString());
			
			if (segStart != segEnd && highlightedText != "") {
				// non empty highlighted text - add a triplet
				var url = editPanelUrl.replace("startOffsetToBeChanged", segStart).replace("endOffsetToBeChanged", segEnd).replace("highlightedTextToBeChanged", encodeURIComponent(range.toString()));
				jQuery("#editPanel *").remove(); // remove previous content from editPanel
				$("#showLegend").click(); // Make sure that the edit panel is visible
				jQuery("#editPanel").load(url);
			} else {
				// single click on a link (or blank highlighted text) - go to that page
				jQuery("#editPanel *").remove(); // remove previous content from editPanel
				var e = window.event || evt;
				var spanid = e.target.id;//to know on which span user has clicked.
				var url2 = hrefurl.replace("hrefspanIDToBeChanged", spanid);
				jQuery('#doc_view_text_view_ajax').load(url2);
			}
		} else {
			// bizarre click
			jQuery("#editPanel *").remove(); // remove previous content from editPanel	
		}
	})
};
