/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.service;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.portlet.bean.HighlightedPagesBean;
import org.ow2.weblab.portlet.bean.LegendFieldsBean;
import org.ow2.weblab.portlet.bean.conf.DocumentServiceConfBean;
import org.ow2.weblab.portlet.bean.conf.MediaServiceConfBean;
import org.ow2.weblab.portlet.controller.DocumentViewerController;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * @author ymombrun
 */
public class ConfigurationTest {


	@Test
	public void testLoadConfiguration() throws Exception {

		final FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/DocViewPortlet-portlet.xml");

		Assert.assertNotNull(context);

		final DocumentServiceConfBean documentServiceConfBean = context.getBean("documentServiceConfBean", DocumentServiceConfBean.class);
		Assert.assertNotNull(documentServiceConfBean);
		Assert.assertNotNull(documentServiceConfBean.getSparqlEndpointURL());
		Assert.assertNotNull(documentServiceConfBean.getSparqlEndpointURLUpdate());
		Assert.assertNotNull(documentServiceConfBean.getPrefixMap());
		Assert.assertFalse(documentServiceConfBean.getPrefixMap().isEmpty());

		final MediaServiceConfBean mediaServiceConfBean = context.getBean("mediaServiceConfBean", MediaServiceConfBean.class);
		Assert.assertNotNull(mediaServiceConfBean);
		Assert.assertNotNull(mediaServiceConfBean.getAudioFormats());
		Assert.assertNotNull(mediaServiceConfBean.getImageFormats());
		Assert.assertNotNull(mediaServiceConfBean.getVideoFormats());
		Assert.assertFalse(mediaServiceConfBean.getAudioFormats().isEmpty());
		Assert.assertFalse(mediaServiceConfBean.getImageFormats().isEmpty());
		Assert.assertFalse(mediaServiceConfBean.getVideoFormats().isEmpty());

		final MediaViewerService mediaViewerService = context.getBean("mediaViewerService", MediaViewerService.class);
		Assert.assertNotNull(mediaViewerService);

		final UserService userService = context.getBean("userService", UserService.class);
		Assert.assertNotNull(userService);
		Assert.assertNotNull(userService.getAdminRoles());
		Assert.assertFalse(userService.getAdminRoles().isEmpty());

		final HighlightedPagesBean pagesBean = context.getBean("pagesBean", HighlightedPagesBean.class);
		Assert.assertNotNull(pagesBean);

		final LegendFieldsBean legendBean = context.getBean("legendBean", LegendFieldsBean.class);
		Assert.assertNotNull(legendBean);
		Assert.assertNotNull(legendBean.getFields());
		Assert.assertFalse(legendBean.getFields().isEmpty());

		final DocumentViewerController controller = context.getBean(DocumentViewerController.class);
		Assert.assertNotNull(controller);
	}

}
