/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab;


import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.helper.impl.SemanticResource;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.QuerySolutionMap;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.shared.PrefixMapping;

/**
 * The DocumentRenderer renders a document page in HTML. In the rendered view,
 * each span represents an href, a named entity, a style (bold or italic), or
 * the overlap of one of more of these. In the quick view, only the styles (bold
 * or italic) and their overlap are rendered.
 *
 * @author Esther
 *
 */
public class DocumentRendererProcessor {


	private static final String IS_CANDIDATE = "isCandidate";


	private static final String IS_CANDIDATE_CLASS = "candidate_instance";


	private static final String IS_VALIDATED_CLASS = "validated_instance";


	private static final String DOCUMENT_HREF_RENDERER = "document-href-renderer";


	private static final String UNKNOWN_ENTITY = "document-renderer-Unknown";


	private static final Log logger = LogFactory.getLog(DocumentRendererProcessor.class);


	private static String askIsStructuredRequestName = "askIsStructured";


	private DocumentRendererProcessor() {

		//
	}


	/**
	 * Get an HTML representation of the annotated document (loaded or not). Each span represents an href, a named entity, a style (bold or italic), or the overlap of one of more of these. The span
	 * class corresponds to the entity type.
	 *
	 * @param documentRendererBean
	 *            The configuration holder
	 *
	 * @throws XMLStreamException
	 *             If anything wrong occurs parsing the XHTML
	 * @throws IOException
	 *             If anything occurs accessing reading/writing in the stream
	 */
	public static void getRenderedHtmlRepresentation(final DocumentRendererBean documentRendererBean) throws XMLStreamException, IOException {

		documentRendererBean.setFindPageRequestType(DocumentRendererBean.FindPageRequest.selectAllGivenPageDoc);
		DocumentRendererProcessor.getHtmlRepresentation(documentRendererBean);
	}


	/**
	 * Get an HTML representation of the annotated document (loaded or not). Each span represents an href, a named entity, a style (bold or italic), or the overlap of one of more of these. The span
	 * class corresponds to the entity type.
	 *
	 * @param documentRendererBean
	 *            The configuration holder
	 *
	 * @throws XMLStreamException
	 *             If anything wrong occurs parsing the XHTML
	 *
	 * @throws IOException
	 *             If anything occurs accessing reading/writing in the stream
	 */
	public static void getQuickViewHtmlRepresentation(final DocumentRendererBean documentRendererBean) throws XMLStreamException, IOException {

		documentRendererBean.setFindPageRequestType(DocumentRendererBean.FindPageRequest.QuickViewGivenPageDoc);
		DocumentRendererProcessor.getHtmlRepresentation(documentRendererBean);
	}


	private static void getHtmlRepresentation(final DocumentRendererBean renderBean) throws XMLStreamException, IOException {

		// Test to see whether the document has been normalised or not by ASKing whether it contains page number 1.
		if ((renderBean.getSPARQLEndpoint() != null) && (renderBean.getDocURI() != null)) {
			// Is it better to stop processing here with the expense of another query, or to pass through the rest of the code ?
			final boolean hasPages = DocumentRendererProcessor.askHasPageFromTriplestore(renderBean.getDocURI(), renderBean.getSPARQLEndpoint(), 1,
					DocumentRendererProcessor.askIsStructuredRequestName);
			if (!renderBean.isForceNonNormalised() && hasPages) {
				DocumentRendererProcessor.getHtmlRepresentationNormalised(renderBean);
			} else {
				DocumentRendererProcessor.getHtmlRepresentationNonNormalised(renderBean, hasPages);
			}
		} else {
			if (renderBean.getSemRes() == null) {
				renderBean.setSemRes(new SemanticResource(renderBean.getDoc()));
			}

			final boolean hasPages = DocumentRendererProcessor.askHasPageFromSemRes(renderBean.getDocURI(), renderBean.getSemRes(), 1, DocumentRendererProcessor.askIsStructuredRequestName);
			if (!renderBean.isForceNonNormalised() && hasPages) {
				DocumentRendererProcessor.getHtmlRepresentationNormalised(renderBean);
			} else {
				DocumentRendererProcessor.getHtmlRepresentationNonNormalised(renderBean, hasPages);
			}
		}
	}


	private static void getHtmlRepresentationNonNormalised(final DocumentRendererBean renderBean, final boolean hasPages) throws XMLStreamException, IOException {

		// Create a UUID for the spans for this page
		final UUID spanUUID = UUID.randomUUID();

		DocumentRendererProcessor.logger.trace("Start writing: " + renderBean.getDocURI());

		if (renderBean.isFullHTML()) {
			// Write the heading for the output

			renderBean.getXtw().writeStartDocument("utf-8", "1.0");
			renderBean.getXtw().writeDTD("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" " + "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");

			renderBean.getXtw().writeStartElement("html");
			renderBean.getXtw().writeAttribute("xmlns", "http://www.w3.org/1999/xhtml");
			renderBean.getXtw().writeStartElement("head");
			renderBean.getXtw().writeStartElement("title");
			renderBean.getXtw().writeEndElement(); // end of title
			renderBean.getXtw().writeEndElement(); // end of head
			renderBean.getXtw().flush();

			// Note : css is being written here using the parameters passed in the render request
			// This enables the calling program to decide what colours each class should take.
			DocumentRendererProcessor.writeCss(renderBean.getXtw(), renderBean.getLegendCssMap());

			DocumentRendererProcessor.logger.trace("Writing body");

			renderBean.getXtw().writeStartElement("body");
			renderBean.getXtw().flush();
		} else {
			// Note : css is being written here using the parameters passed in the render request
			// This enables the calling program to decide what colours each class should take.
			DocumentRendererProcessor.writeCss(renderBean.getXtw(), renderBean.getLegendCssMap());
			renderBean.getXtw().flush();
		}

		try {
			// Get the information from the triplestore or from the semantic resource
			ResultSet rs;
			if ((renderBean.getSPARQLEndpoint() != null) && (renderBean.getDocURI() != null)) {
				rs = DocumentRendererProcessor.getDocInfoFromTriplestore(renderBean.getDocURI(), renderBean.getSPARQLEndpoint(), renderBean.getPagenum(), renderBean.getFindPageRequestNonNormalised());
			} else if (hasPages) {
				rs = DocumentRendererProcessor.getDocInfoFromSemRes(renderBean.getDocURI(), renderBean.getSemRes(), renderBean.getPagenum(), "selectGivenPageDocNonNorm");
			} else {
				rs = DocumentRendererProcessor.getDocInfoFromSemRes(renderBean.getDocURI(), renderBean.getSemRes(), renderBean.getPagenum(), renderBean.getFindPageRequestNonNormalised());
			}

			// Every result in the result set corresponds to a linear segment
			// They are in seg start order, but a document can have several overlapping segments
			QuerySolution current;

			// The named entities currently referenced in the spans
			final LinkedList<String> currentEntities = new LinkedList<>();

			// The classes currently applicable to the spans (named entities)
			// The key is the class, the value is the number of times this class is currently applied.
			// When the map is empty, there are no currently applicable classes.
			final HashMap<String, Integer> currentClasses = new HashMap<>();

			// A list of spans, in the same order as the recordset, ie. sorted by segment start
			final LinkedList<Span> startSpans = new LinkedList<>();

			// A set of the spans sorted by the segment end index
			final TreeSet<Span> endSpans = new TreeSet<>();

			final int divStart = 0;
			int divEnd = 0;
			renderBean.setFullText("");

			// All the text on the original page have been condensed into one big block
			if (rs.hasNext()) {
				current = rs.next();

				// The text content of the page without any formatting
				renderBean.setFullText(current.get("fulltext").asLiteral().getString());
			}

			divEnd = renderBean.getFullText().length();

			renderBean.getXtw().writeStartElement("div");
			renderBean.getXtw().writeAttribute("data-divstart", divStart + "");
			renderBean.getXtw().flush();

			// Generate the div id
			final UUID divUUID = UUID.randomUUID();
			renderBean.getXtw().writeAttribute("id", divUUID + "");
			renderBean.getXtw().flush();

			while (rs.hasNext()) {
				// Loop through the recordset to get the entity information, then construct the spans associated.

				current = rs.next();
				Span sp = null;
				// We're still in the previous div, so keep adding the spans to the lists

				if ((current.get("entType") != null) && renderBean.isShowToolTips()) {

					final String entityUri = current.getResource("entType").getURI();

					// Use the map of uris and classes passed as parameter legendUriClassMap to determine what style class name to give the entity
					String styleClassName = renderBean.getLegendUriClassMap().get(entityUri);
					if (styleClassName == null) {
						DocumentRendererProcessor.logger.debug("URI Type is not defined " + entityUri + " as legend field.");
						styleClassName = DocumentRendererProcessor.UNKNOWN_ENTITY;
					}

					// Check to see if isCandidate has been set. If it hasn't, then it's false
					boolean isCandidate = false;
					if (current.get(DocumentRendererProcessor.IS_CANDIDATE) != null) {
						isCandidate = current.getLiteral(DocumentRendererProcessor.IS_CANDIDATE).asLiteral().getBoolean();
					}

					// Write styleClassName and isCandidate to the startSpan and endSpan lists
					sp = new Span(current.getLiteral("segEnd").asLiteral().getInt(), current.getLiteral("segStart").asLiteral().getInt(), "", styleClassName, "",
							current.getResource("segRef").getURI(), isCandidate, renderBean.getDifferentiator());
					renderBean.setDifferentiator(renderBean.getDifferentiator() + 1);
					startSpans.add(sp);
					endSpans.add(sp);
				}
			}

			// End of recordset, so write the text and spans associated with this div
			DocumentRendererProcessor.outputSpansForDivNonNorm(divStart, divEnd, currentClasses, startSpans, endSpans, currentEntities, spanUUID, renderBean);

			renderBean.getXtw().writeEndElement();// end of div
			renderBean.getXtw().flush();

		} catch (final NullPointerException e) {
			// There was a problem with data missing from the triplestore and we are unable to render this page
			DocumentRendererProcessor.logger.warn("Problem writing: " + renderBean.getDocURI() + " - data missing", e);
		}

		if (renderBean.isFullHTML()) {
			renderBean.getXtw().writeEndElement(); // end of body
			renderBean.getXtw().flush();

			// Write the end of the stream

			renderBean.getXtw().writeEndDocument();
			renderBean.getXtw().flush();
		}

		renderBean.getXtw().close();
		DocumentRendererProcessor.logger.trace("Finished writing : " + renderBean.getDocURI());
	}


	private static void outputSpansForDivNonNorm(final int divStart, final int divEnd, final HashMap<String, Integer> currentClasses, final LinkedList<Span> startSpans, final TreeSet<Span> endSpans,
			final LinkedList<String> currentEntities, final UUID spanUUID, final DocumentRendererBean renderBean) throws XMLStreamException {

		/*
		 * Iterate through between div start and div end, and check if there is a start or an end span associated with that index.
		 * For each one encountered, start or end a span as appropriate.
		 * When a span finishes, if the map of current current classes is not empty, then a new span is started with those classes
		 *
		 * A map of span ids and entity references is constructed for use with the tooltips
		 */

		for (int i = divStart; i <= divEnd; i++) {

			// Check to see if a segment starts at this index

			if (!startSpans.isEmpty() && (startSpans.getFirst().begin == i)) {

				// Check if we need to finish a span because there is one currently active
				// Only write end span if part way through div
				if (renderBean.isSpanActive() && (i > divStart)) {

					renderBean.getXtw().writeEndElement(); // end of span
					renderBean.getXtw().flush();
					renderBean.setSpanId(renderBean.getSpanId() + 1);
				}

				while (!startSpans.isEmpty() && (startSpans.getFirst().begin == i)) {
					// Treat all segments that start at this index

					// Treat the named entities applicable to this span
					if (startSpans.getFirst().styleClassName != "") {
						// If the current segment has a named entity

						// Add the entity instance to the list of current entities
						final String entityInst = startSpans.getFirst().namedEntity;
						currentEntities.add(entityInst);

						// Add the entity styleClassName to the list of current classes
						final String styleClassName = startSpans.getFirst().styleClassName;

						if (currentClasses.containsKey(styleClassName)) {
							int value = currentClasses.get(styleClassName).intValue();
							value++;
							currentClasses.put(styleClassName, Integer.valueOf(value));
						} else {
							currentClasses.put(styleClassName, Integer.valueOf(1));
						}

						// Add isCandidate to the list of current classes if applicable
						if (startSpans.getFirst().isCandidate) {
							if (currentClasses.containsKey(DocumentRendererProcessor.IS_CANDIDATE)) {
								int value = currentClasses.get(DocumentRendererProcessor.IS_CANDIDATE).intValue();
								value++;
								currentClasses.put(DocumentRendererProcessor.IS_CANDIDATE, Integer.valueOf(value));
							} else {
								currentClasses.put(DocumentRendererProcessor.IS_CANDIDATE, Integer.valueOf(1));
							}
						}
					}

					// Remove the treated segment from the list
					startSpans.removeFirst();
				}

				// Write the start of a span
				DocumentRendererProcessor.writeStartSpanNonNorm(i, spanUUID, currentClasses, currentEntities, renderBean);

			} else {
				// Even though there isn't a segment starting the div, then a span needs to be started as the div opens
				if (!renderBean.isSpanActive() && (i == divStart)) {
					// if ((!currentStyles.isEmpty() || !currentClasses.isEmpty()) && i == divStart) {

					// Write the start of a span
					DocumentRendererProcessor.writeStartSpanNonNorm(i, spanUUID, currentClasses, currentEntities, renderBean);
				}
			}

			// Check to see if a segment ends at this index

			if (!endSpans.isEmpty() && (endSpans.first().end == i)) {

				// Write the end of a span
				renderBean.getXtw().writeEndElement(); // end of span
				renderBean.getXtw().flush();
				renderBean.setSpanActive(false);
				renderBean.setSpanId(renderBean.getSpanId() + 1);

				// Treat all segments that end at this index
				while (!endSpans.isEmpty() && (endSpans.first().end == i)) {

					if (endSpans.first().styleClassName != "") {
						final String styleClassName = endSpans.first().styleClassName;

						if (currentClasses.containsKey(styleClassName)) {
							int value = currentClasses.get(styleClassName).intValue();
							value--;
							if (value > 0) {
								currentClasses.put(styleClassName, Integer.valueOf(value));
							} else {
								currentClasses.remove(styleClassName);
							}

							// Remove the entity instance from the list of current entities
							final String entityInst = endSpans.first().namedEntity;
							currentEntities.remove(entityInst);
						}

						// Remove isCandidate from the list of current classes if applicable
						if (endSpans.first().isCandidate) {
							if (currentClasses.containsKey(DocumentRendererProcessor.IS_CANDIDATE)) {
								int value = currentClasses.get(DocumentRendererProcessor.IS_CANDIDATE).intValue();
								value--;
								if (value > 0) {
									currentClasses.put(DocumentRendererProcessor.IS_CANDIDATE, Integer.valueOf(value));
								} else {
									currentClasses.remove(DocumentRendererProcessor.IS_CANDIDATE);
								}
							}
						}
					}

					// Remove the treated segment from the list
					endSpans.remove(endSpans.first());
				}

				// Start a new span if we're not at the end of the div
				if (!renderBean.isSpanActive() && (i < divEnd)) {

					// Write the start of a span
					DocumentRendererProcessor.writeStartSpanNonNorm(i, spanUUID, currentClasses, currentEntities, renderBean);
				}
			}

			// Write the text for this position
			if (i < divEnd) {
				renderBean.getXtw().writeCharacters(renderBean.getFullText().substring(i, i + 1));
				renderBean.getXtw().flush();
			}
		}

		// Close the current span
		if (renderBean.isSpanActive()) {
			renderBean.getXtw().writeEndElement(); // span
			renderBean.setSpanActive(false);
			renderBean.getXtw().flush();
			renderBean.setSpanId(renderBean.getSpanId() + 1);
		}

	}


	private static void writeStartSpanNonNorm(final int i, final UUID spanUUID, final HashMap<String, Integer> currentClasses, final LinkedList<String> currentEntities,
			final DocumentRendererBean renderBean) throws XMLStreamException {

		// Write the start of a span
		renderBean.getXtw().writeStartElement("span");
		renderBean.setSpanActive(true);
		renderBean.getXtw().flush();
		renderBean.getXtw().writeAttribute("id", spanUUID + "-" + renderBean.getSpanId());
		renderBean.getXtw().writeAttribute("data-spanstart", i + "");
		renderBean.getXtw().flush();

		// Write all the applicable classes for this span
		if (!currentClasses.isEmpty()) {
			final String appliClasses = DocumentRendererProcessor.getClasses(currentClasses);

			if (appliClasses != "") {
				renderBean.getXtw().writeAttribute("class", appliClasses);
				renderBean.getXtw().flush();
			}
		}

		if (!currentEntities.isEmpty()) {
			// Add the spanId entityRef combination to spanEntities
			final LinkedList<String> currentEntitiesCopy = new LinkedList<>(currentEntities);
			renderBean.getSpanEntities().put(spanUUID + "-" + renderBean.getSpanId(), currentEntitiesCopy);
		}
	}


	private static void getHtmlRepresentationNormalised(final DocumentRendererBean renderBean) throws XMLStreamException, IOException {

		// Create a UUID for the spans for this page
		final UUID spanUUID = UUID.randomUUID();

		DocumentRendererProcessor.logger.trace("Start writing : " + renderBean.getDocURI());

		if (renderBean.isFullHTML()) {
			// Write the heading for the output

			renderBean.getXtw().writeStartDocument("utf-8", "1.0");
			renderBean.getXtw().writeDTD("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" " + "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");

			renderBean.getXtw().writeStartElement("html");
			renderBean.getXtw().writeAttribute("xmlns", "http://www.w3.org/1999/xhtml");
			renderBean.getXtw().writeStartElement("head");
			renderBean.getXtw().writeStartElement("title");
			renderBean.getXtw().writeEndElement(); // end of title
			renderBean.getXtw().writeEndElement(); // end of head
			renderBean.getXtw().flush();

			// Note : css is being written here using the parameters passed in the render request
			// This enables the calling program to decide what colours each class should take.
			DocumentRendererProcessor.writeCss(renderBean.getXtw(), renderBean.getLegendCssMap());

			DocumentRendererProcessor.logger.trace("Writing body");

			renderBean.getXtw().writeStartElement("body");
			renderBean.getXtw().flush();
		} else {

			// Note : css is being written here using the parameters passed in the render request
			// This enables the calling program to decide what colours each class should take.
			DocumentRendererProcessor.writeCss(renderBean.getXtw(), renderBean.getLegendCssMap());
			renderBean.getXtw().flush();
		}

		try {
			// Get the information from the triplestore or from the semantic resource
			ResultSet rs;
			if ((renderBean.getSPARQLEndpoint() != null) && (renderBean.getDocURI() != null)) {
				rs = DocumentRendererProcessor.getDocInfoFromTriplestore(renderBean.getDocURI(), renderBean.getSPARQLEndpoint(), renderBean.getPagenum(), renderBean.getFindPageRequestNormalised());
			} else {
				rs = DocumentRendererProcessor.getDocInfoFromSemRes(renderBean.getDocURI(), renderBean.getSemRes(), renderBean.getPagenum(), renderBean.getFindPageRequestNormalised());
			}

			// Every result in the result set corresponds to a linear segment
			// They are in div number order, but a div can have several overlapping segments, or even segments that start in one div and end in another
			QuerySolution current;

			String style = "";

			// The named entities currently referenced in the spans
			final LinkedList<String> currentEntities = new LinkedList<>();

			// The classes currently applicable to the spans (named entities and hrefs)
			// The key is the class, the value is the number of times this class is currently applied.
			// When the map is empty, there are no currently applicable classes.
			final HashMap<String, Integer> currentClasses = new HashMap<>();

			// The styles currently applicable to the spans
			// The key is the style, the value is the number of times this style is currently applied.
			// When the map is empty, there are no currently applicable styles.
			final HashMap<String, Integer> currentStyles = new HashMap<>();

			// A list of spans, in the same order as the recordset, ie. sorted by segment start
			final LinkedList<Span> startSpans = new LinkedList<>();

			// A set of the spans sorted by the segment end index
			final TreeSet<Span> endSpans = new TreeSet<>();

			// A list of linebreaks, in the same order as the recordset, ie. sorted by segment start
			final LinkedList<Integer> lineBreaks = new LinkedList<>();

			// Indicates that the first div has been written
			boolean first = true;

			int divStart = 0;
			int divEnd = 0;

			// All the images on the original page have been condensed into one
			if (rs.hasNext()) {
				current = rs.next();
				renderBean.getXtw().writeStartElement("image");
				final RDFNode imageNode = current.get("image");
				if (imageNode == null) {
					throw new XMLStreamException("Resource " + renderBean.getDocURI()
							+ " contains images but Renderer could not find their content. Make sure ContentManager is configurated properly during the pdf normalization.");
				}
				String imgURL = imageNode.toString();
				// String imgURL = imageNode.asResource().getURI();
				if (renderBean.isRelative()) {
					imgURL = new URL(imgURL).getPath();
				}
				renderBean.getXtw().writeAttribute("src", imgURL);
				renderBean.getXtw().writeEndElement(); // end image
				renderBean.getXtw().flush();

				// The text content of the page without any formatting
				renderBean.setFullText(current.get("fulltext").asLiteral().getString());
			}

			while (first && rs.hasNext()) {
				current = rs.next();

				// The first div on the page
				if (current.get("isDiv").asLiteral().getBoolean()) {

					divStart = current.getLiteral("divStart").asLiteral().getInt();
					divEnd = current.getLiteral("divEnd").asLiteral().getInt();

					renderBean.getXtw().writeStartElement("div");
					renderBean.getXtw().flush();
					style = current.get("style").asLiteral().getString();
					renderBean.getXtw().writeAttribute("style", style);
					renderBean.getXtw().writeAttribute("data-divstart", divStart + "");
					renderBean.getXtw().flush();

					// Generate the div id
					String divId = current.get("seg").asResource().getURI();
					renderBean.getXtw().writeAttribute("rel", divId);
					divId = DocumentRendererProcessor.hash(divId);
					divId = "ID_" + divId;
					renderBean.getXtw().writeAttribute("id", divId);
					renderBean.getXtw().flush();

					first = false;
				}
			}

			while (rs.hasNext()) {

				// Loop through the recordset. For each div, construct the spans associated.

				current = rs.next();

				if (current.get("isDiv").asLiteral().getBoolean()) {
					// End of previous div, so write the text and spans associated
					DocumentRendererProcessor.outputSpansForDivNormalised(divStart, divEnd, currentStyles, currentClasses, startSpans, endSpans, lineBreaks, currentEntities, spanUUID, renderBean);

					renderBean.getXtw().writeEndElement(); // end of div

					renderBean.getXtw().flush();

					// Now write the new div

					divStart = current.getLiteral("divStart").asLiteral().getInt();
					divEnd = current.getLiteral("divEnd").asLiteral().getInt();

					renderBean.getXtw().writeStartElement("div");
					renderBean.getXtw().flush();
					style = current.get("style").asLiteral().getString();
					renderBean.getXtw().writeAttribute("style", style);
					renderBean.getXtw().writeAttribute("data-divstart", divStart + "");
					renderBean.getXtw().flush();

					// Generate the div id
					String divId = current.get("seg").asResource().getURI();
					renderBean.getXtw().writeAttribute("rel", divId);
					divId = DocumentRendererProcessor.hash(divId);
					divId = "ID_" + divId;
					renderBean.getXtw().writeAttribute("id", divId);
					renderBean.getXtw().flush();
				} else {
					Span sp = null;
					// We're still in the previous div, so keep adding the spans to the lists

					if (current.get("isLine").asLiteral().getBoolean()) {
						// Write <br> to the linebreaks list
						lineBreaks.add(Integer.valueOf(current.get("segStart").asLiteral().getInt()));
					}

					if (current.contains("isBold")) {

						final String bold = "font-weight:bold";

						// Write bold to the startSpan and endSpan lists
						sp = new Span(current.getLiteral("segEnd").asLiteral().getInt(), current.getLiteral("segStart").asLiteral().getInt(), bold, "", "", "", false, renderBean.getDifferentiator());
						renderBean.setDifferentiator(renderBean.getDifferentiator() + 1);
					}

					if (current.contains("isItalic")) {

						final String italic = "font-style:italic";

						// Write italic to the startSpan and endSpan lists
						if (sp == null) {
							sp = new Span(current.getLiteral("segEnd").asLiteral().getInt(), current.getLiteral("segStart").asLiteral().getInt(), italic, "", "", "", false,
									renderBean.getDifferentiator());
							renderBean.setDifferentiator(renderBean.getDifferentiator() + 1);
						} else {
							sp.setStyle(italic);
						}
					}

					if ((current.get("href") != null) && renderBean.isShowToolTips() && renderBean.isShowInstanceLink()) {

						final String href = current.getResource("href").toString();

						// Write href to the startSpan and endSpan lists
						if (sp == null) {
							sp = new Span(current.getLiteral("segEnd").asLiteral().getInt(), current.getLiteral("segStart").asLiteral().getInt(), "", "", href, "", false,
									renderBean.getDifferentiator());
							renderBean.setDifferentiator(renderBean.getDifferentiator() + 1);
						} else {
							sp.setHref(href);
						}
					}

					if ((current.get("entType") != null) && renderBean.isShowToolTips()) {

						final String entityUri = current.getResource("entType").getURI();

						// Use the map of uris and classes passed as parameter legendUriClassMap to determine what style class name to give the entity
						String styleClassName = renderBean.getLegendUriClassMap().get(entityUri);

						if (styleClassName == null) {
							DocumentRendererProcessor.logger.debug("URI Type is not defined " + entityUri + " as legend field.");
							styleClassName = DocumentRendererProcessor.UNKNOWN_ENTITY;
						}

						// Check to see if isCandidate has been set. If it hasn't, then it's false
						boolean isCandidate = false;
						if (current.get(DocumentRendererProcessor.IS_CANDIDATE) != null) {
							isCandidate = current.getLiteral(DocumentRendererProcessor.IS_CANDIDATE).asLiteral().getBoolean();
						}

						// Write styleClassName and isCandidate to the startSpan and endSpan lists
						if (sp == null) {
							sp = new Span(current.getLiteral("segEnd").asLiteral().getInt(), current.getLiteral("segStart").asLiteral().getInt(), "", styleClassName, "",
									current.getResource("segRef").getURI(), isCandidate, renderBean.getDifferentiator());
							renderBean.setDifferentiator(renderBean.getDifferentiator() + 1);
						} else {
							sp.setStyleClassName(styleClassName);
							sp.setCandidate(isCandidate);
							sp.setNamedEntity(current.getResource("segRef").getURI());
						}
					}
					if (sp != null) {
						startSpans.add(sp);
						endSpans.add(sp);
					}
				}
			}

			// End of final div, so write the text and spans associated
			DocumentRendererProcessor.outputSpansForDivNormalised(divStart, divEnd, currentStyles, currentClasses, startSpans, endSpans, lineBreaks, currentEntities, spanUUID, renderBean);

			if (!first) {
				// Only write the end div if we've written the start (for a doc with
				// only images, there will be no text div)
				renderBean.getXtw().writeEndElement();// end of final div
			}
			renderBean.getXtw().flush();
		} catch (final NullPointerException e) {
			// There was a problem with data missing from the triplestore and we are unable to render this page
			DocumentRendererProcessor.logger.warn("Problem writing: " + renderBean.getDocURI() + " - data missing.", e);
		}

		if (renderBean.isFullHTML()) {
			renderBean.getXtw().writeEndElement(); // end of body
			renderBean.getXtw().flush();
			// Write the end of the stream
			renderBean.getXtw().writeEndDocument();
			renderBean.getXtw().flush();
		}

		renderBean.getXtw().close();
		DocumentRendererProcessor.logger.trace("Finished writing : " + renderBean.getDocURI());
	}


	private static void outputSpansForDivNormalised(final int divStart, final int divEnd, final HashMap<String, Integer> currentStyles, final HashMap<String, Integer> currentClasses,
			final LinkedList<Span> startSpans, final TreeSet<Span> endSpans, final LinkedList<Integer> lineBreaks, final LinkedList<String> currentEntities, final UUID spanUUID,
			final DocumentRendererBean renderBean) throws XMLStreamException {

		/*
		 * Iterate through between div start and div end, and check if there is a start or an end span associated with that index. For each one encountered, start or end a span as appropriate. When a
		 * span finishes, if the maps of current styles and current classes are not empty, then a new span is started with those styles and classes
		 *
		 * If a Line is encountered, then a <br> is written.
		 *
		 * The classes for each span are managed similarly to the styles, with the addition that a map of span ids and entity references is constructed for use with the tooltips
		 */

		for (int i = divStart; i <= divEnd; i++) {

			// Check to see if a segment starts at this index

			if ((!startSpans.isEmpty() && (startSpans.getFirst().begin == i)) || (!lineBreaks.isEmpty() && (lineBreaks.getFirst().intValue() == i))) {

				// Check if we need to finish a span because there is one currently active
				// Only write end span if part way through div, or if there is a line break
				if (renderBean.isSpanActive() && (i > divStart)) {
					renderBean.getXtw().writeEndElement(); // end of span
					renderBean.getXtw().flush();
					renderBean.setSpanId(renderBean.getSpanId() + 1);
				}

				if (!lineBreaks.isEmpty() && (lineBreaks.getFirst().intValue() == i)) {
					// Write a line break
					renderBean.getXtw().writeEmptyElement("br");
					renderBean.getXtw().flush();
					lineBreaks.removeFirst();
				}

				while (!startSpans.isEmpty() && (startSpans.getFirst().begin == i)) {
					// Treat all segments that start at this index

					// First treat the styles applicable to this span
					if (startSpans.getFirst().style != "") {
						// If the style of the current segment is not empty
						final String style = startSpans.getFirst().style;
						// Add the style to the list of current styles
						if (currentStyles.containsKey(style)) {
							int value = currentStyles.get(style).intValue();
							value++;
							currentStyles.put(style, Integer.valueOf(value));
						} else {
							currentStyles.put(style, Integer.valueOf(1));
						}
					}

					// Treat the href applicable to this span

					if (startSpans.getFirst().href != "") {
						// If the href of the current segment is not empty

						final String hrefURl = startSpans.getFirst().href;
						renderBean.setCurrentHref(hrefURl);

						// Add href to the list of current classes
						// (note, the inversion of renderer and href is done on purpose in the name to make the jquery select easier)
						if (currentClasses.containsKey(DocumentRendererProcessor.DOCUMENT_HREF_RENDERER)) {
							int value = currentClasses.get(DocumentRendererProcessor.DOCUMENT_HREF_RENDERER).intValue();
							value++;
							currentClasses.put(DocumentRendererProcessor.DOCUMENT_HREF_RENDERER, Integer.valueOf(value));
						} else {
							currentClasses.put(DocumentRendererProcessor.DOCUMENT_HREF_RENDERER, Integer.valueOf(1));
						}
					}

					// Now treat the named entities applicable to this span
					if (startSpans.getFirst().styleClassName != "") {
						// If the current segment has a named entity

						// Add the entity instance to the list of current
						// entities
						final String entityInst = startSpans.getFirst().namedEntity;
						currentEntities.add(entityInst);

						// Add the entity styleClassName to the list of current classes
						final String styleClassName = startSpans.getFirst().styleClassName;

						if (currentClasses.containsKey(styleClassName)) {
							int value = currentClasses.get(styleClassName).intValue();
							value++;
							currentClasses.put(styleClassName, Integer.valueOf(value));
						} else {
							currentClasses.put(styleClassName, Integer.valueOf(1));
						}

						// Add isCandidate to the list of current classes if applicable
						if (startSpans.getFirst().isCandidate) {
							if (currentClasses.containsKey(DocumentRendererProcessor.IS_CANDIDATE)) {
								int value = currentClasses.get(DocumentRendererProcessor.IS_CANDIDATE).intValue();
								value++;
								currentClasses.put(DocumentRendererProcessor.IS_CANDIDATE, Integer.valueOf(value));
							} else {
								currentClasses.put(DocumentRendererProcessor.IS_CANDIDATE, Integer.valueOf(1));
							}
						}
					}

					// Remove the treated segment from the list
					startSpans.removeFirst();
				}
				// Write the start of the span to the writer
				DocumentRendererProcessor.writeStartSpanNormalised(i, spanUUID, currentClasses, currentEntities, currentStyles, renderBean);

			} else {
				// Even though there isn't a segment starting the div, then a span needs to be started as the div
				// opens
				if (!renderBean.isSpanActive() && (i == divStart)) {
					// Write the start of the span to the writer
					DocumentRendererProcessor.writeStartSpanNormalised(i, spanUUID, currentClasses, currentEntities, currentStyles, renderBean);
				}
			}

			// Check to see if a segment ends at this index

			if (!endSpans.isEmpty() && (endSpans.first().end == i)) {

				// Write the end of a span
				renderBean.getXtw().writeEndElement(); // end of span
				renderBean.getXtw().flush();
				renderBean.setSpanActive(false);
				renderBean.setSpanId(renderBean.getSpanId() + 1);

				// Treat all segments that end at this index
				while (!endSpans.isEmpty() && (endSpans.first().end == i)) {

					if (endSpans.first().style != "") {
						final String style = endSpans.first().style;
						if (currentStyles.containsKey(style)) {
							int value = currentStyles.get(style).intValue();
							value--;
							if (value > 0) {
								currentStyles.put(style, Integer.valueOf(value));
							} else {
								currentStyles.remove(style);
							}
						}
					}

					if (endSpans.first().styleClassName != "") {
						final String styleClassName = endSpans.first().styleClassName;

						// Remove entity from the list of current classes if applicable
						if (currentClasses.containsKey(styleClassName)) {
							int value = currentClasses.get(styleClassName).intValue();
							value--;
							if (value > 0) {
								currentClasses.put(styleClassName, Integer.valueOf(value));
							} else {
								currentClasses.remove(styleClassName);
							}

							// Remove the entity instance from the list of current entities
							final String entityInst = endSpans.first().namedEntity;
							currentEntities.remove(entityInst);
						}

						// Remove isCandidate from the list of current classes if applicable
						if (endSpans.first().isCandidate) {
							if (currentClasses.containsKey(DocumentRendererProcessor.IS_CANDIDATE)) {
								int value = currentClasses.get(DocumentRendererProcessor.IS_CANDIDATE).intValue();
								value--;
								if (value > 0) {
									currentClasses.put(DocumentRendererProcessor.IS_CANDIDATE, Integer.valueOf(value));
								} else {
									currentClasses.remove(DocumentRendererProcessor.IS_CANDIDATE);
								}
							}
						}
					}

					// Remove href from the list of current classes if applicable
					if (endSpans.first().href != "") {
						if (currentClasses.containsKey(DocumentRendererProcessor.DOCUMENT_HREF_RENDERER)) {
							int value = currentClasses.get(DocumentRendererProcessor.DOCUMENT_HREF_RENDERER).intValue();
							value--;
							if (value > 0) {
								currentClasses.put(DocumentRendererProcessor.DOCUMENT_HREF_RENDERER, Integer.valueOf(value));
							} else {
								currentClasses.remove(DocumentRendererProcessor.DOCUMENT_HREF_RENDERER);
								renderBean.setCurrentHref("");
							}
						}
					}

					// Remove the treated segment from the list
					final Span span = endSpans.first();
					endSpans.remove(span);
				}

				// Start a new span if we're not at the end of the div
				if (!renderBean.isSpanActive() && (i < divEnd)) {
					// Write the start of the span to the writer
					DocumentRendererProcessor.writeStartSpanNormalised(i, spanUUID, currentClasses, currentEntities, currentStyles, renderBean);
				}
			}

			// Write the text for this position
			if ((i < divEnd) && (i < renderBean.getFullText().length())) {
				renderBean.getXtw().writeCharacters(renderBean.getFullText().substring(i, i + 1));
				renderBean.getXtw().flush();
			}
		}

		// Close the current span
		if (renderBean.isSpanActive()) {
			renderBean.getXtw().writeEndElement(); // span
			renderBean.setSpanActive(false);
			renderBean.getXtw().flush();
			renderBean.setSpanId(renderBean.getSpanId() + 1);
		}
	}


	private static void writeStartSpanNormalised(final int i, final UUID spanUUID, final HashMap<String, Integer> currentClasses, final LinkedList<String> currentEntities,
			final HashMap<String, Integer> currentStyles, final DocumentRendererBean renderBean) throws XMLStreamException {

		// Write the start of a span
		renderBean.getXtw().writeStartElement("span");
		renderBean.setSpanActive(true);
		renderBean.getXtw().flush();
		renderBean.getXtw().writeAttribute("id", spanUUID + "-" + renderBean.getSpanId());
		renderBean.getXtw().writeAttribute("data-spanstart", i + "");
		renderBean.getXtw().flush();


		// Check if there are still current styles or classes
		if (!currentStyles.isEmpty() || !currentClasses.isEmpty()) {
			final String appliStyles = DocumentRendererProcessor.getStyles(currentStyles);
			if (appliStyles != "") {
				renderBean.getXtw().writeAttribute("style", appliStyles);
				renderBean.getXtw().flush();
			}

			// Write all the applicable classes for this span
			final String appliClasses = DocumentRendererProcessor.getClasses(currentClasses);

			if (appliClasses != "") {
				renderBean.getXtw().writeAttribute("class", appliClasses);
				renderBean.getXtw().flush();
				if (appliClasses.contains(DocumentRendererProcessor.DOCUMENT_HREF_RENDERER)) {
					// Write the url as an attribute
					renderBean.getXtw().writeAttribute("url", renderBean.getCurrentHref());
					renderBean.getXtw().flush();
					// Add the span id <=> href url to the map spanHrefs
					final String hrefURL = renderBean.getCurrentHref();
					renderBean.getSpanHrefs().put(spanUUID + "-" + renderBean.getSpanId(), hrefURL);
				}
			}
		}

		if (!currentEntities.isEmpty()) {
			// Add the spanId entityRef combination to spanEntities
			final LinkedList<String> currentEntitiesCopy = new LinkedList<>(currentEntities);
			renderBean.getSpanEntities().put(spanUUID + "-" + renderBean.getSpanId(), currentEntitiesCopy);
		}
	}


	private static String getClasses(final HashMap<String, Integer> currentClasses) {

		final Set<String> classes = currentClasses.keySet();
		String allClasses = "";
		for (final String spanClass : classes) {
			// For the candidate instances, need to add "candidate_instance" as a class
			if (spanClass != DocumentRendererProcessor.IS_CANDIDATE) {
				allClasses += spanClass + " ";
			} else {
				allClasses += " " + DocumentRendererProcessor.IS_CANDIDATE_CLASS + " ";
			}
		}

		if (!allClasses.contains(DocumentRendererProcessor.IS_CANDIDATE_CLASS)) {
			return allClasses + " " + DocumentRendererProcessor.IS_VALIDATED_CLASS;
		}

		return allClasses.trim();
	}


	private static String getStyles(final HashMap<String, Integer> currentStyles) {

		final Set<String> styles = currentStyles.keySet();
		String allStyles = "";
		for (final String style : styles) {
			allStyles += style + ";";
		}
		return allStyles.trim();
	}


	private static ResultSet getDocInfoFromSemRes(final String docURI, final SemanticResource semRes, final int pagenum, final String requestName) throws IOException {

		String sparqlQuery = null;
		final String resourceName = "requests/" + requestName + ".rq";
		DocumentRendererProcessor.logger.trace("Searching for: " + resourceName);

		sparqlQuery = DocumentRendererProcessor.loadRequest(resourceName);

		final PrefixMapping pmap = DocumentRendererProcessor.createPrefixMapping();

		final QuerySolutionMap qsmap = new QuerySolutionMap();
		qsmap.add("RESOURCE_1", ResourceFactory.createResource(docURI));
		qsmap.add("LITERAL_1", ResourceFactory.createTypedLiteral(Integer.valueOf(pagenum)));

		final ParameterizedSparqlString queryStr = new ParameterizedSparqlString(sparqlQuery, qsmap, pmap);
		sparqlQuery = queryStr.toString();

		if (DocumentRendererProcessor.logger.isTraceEnabled()) {
			DocumentRendererProcessor.logger.trace(sparqlQuery);
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			semRes.constructAsRDF("CONSTRUCT {?s ?p ?o} WHERE {?s ?p ?o}", baos);
			DocumentRendererProcessor.logger.trace(baos.toString("UTF-8"));
		}

		return semRes.selectAsJenaResultSet(sparqlQuery);
	}


	/**
	 * @param docURI
	 *            The document URI
	 * @param semRes
	 *            The semantic resource opened on the document
	 * @param pagenum
	 *            The current page number
	 * @param requestName
	 *            The name of the request file to be loaded
	 * @return The answer of the ask Query issued to the semantic resource : whether the document contains the page requested
	 * @throws FileNotFoundException
	 *             If the file is not found
	 * @throws IOException
	 *             If an error occurred when loading the request file
	 */
	public static boolean askHasPageFromSemRes(final String docURI, final SemanticResource semRes, final int pagenum, final String requestName) throws IOException {

		String sparqlQuery = null;
		final String resourceName = "requests/" + requestName + ".rq";
		DocumentRendererProcessor.logger.trace("Searching for : " + resourceName);

		sparqlQuery = DocumentRendererProcessor.loadRequest(resourceName);

		final PrefixMapping pmap = DocumentRendererProcessor.createPrefixMapping();

		final QuerySolutionMap qsmap = new QuerySolutionMap();
		qsmap.add("RESOURCE_1", ResourceFactory.createResource(docURI));
		qsmap.add("LITERAL_1", ResourceFactory.createTypedLiteral(Integer.valueOf(pagenum)));

		final ParameterizedSparqlString queryStr = new ParameterizedSparqlString(sparqlQuery, qsmap, pmap);
		sparqlQuery = queryStr.toString();

		if (DocumentRendererProcessor.logger.isTraceEnabled()) {
			DocumentRendererProcessor.logger.trace(sparqlQuery);
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			semRes.constructAsRDF("CONSTRUCT {?s ?p ?o} WHERE {?s ?p ?o}", baos);
			DocumentRendererProcessor.logger.trace(baos.toString("UTF-8"));
		}

		return semRes.ask(sparqlQuery);

	}


	/**
	 * @return The prefix mapping to be used everywhere in this code
	 */
	protected static PrefixMapping createPrefixMapping() {

		final PrefixMapping pmap = PrefixMapping.Factory.create();
		pmap.setNsPrefix("model", "http://weblab.ow2.org/core/1.2/ontology/model#");
		pmap.setNsPrefix("structure", "http://weblab.ow2.org/ontology/structure#");
		pmap.setNsPrefix("processing", "http://weblab.ow2.org/core/1.2/ontology/processing#");
		pmap.setNsPrefix("wookie", "http://weblab.ow2.org/wookie.owl#");
		pmap.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		pmap.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		pmap.setNsPrefix("owl", "http://www.w3.org/2002/07/owl#");
		return pmap;
	}


	/**
	 * @param docURI
	 *            The document URI
	 * @param SPARQLEndpoint
	 *            The address of the SPARQL endpoint
	 * @param pagenum
	 *            The current page number
	 * @param requestName
	 *            The name of the request file to be loaded
	 * @return The resultset from the query
	 */
	public static ResultSet getDocInfoFromTriplestore(final String docURI, final String SPARQLEndpoint, final int pagenum, final String requestName) {

		/*
		 * Get the result set from the triplestore for the given document uri
		 * and page
		 */
		final String requestFilesPath = "requests";

		final PrefixMapping pmap = DocumentRendererProcessor.createPrefixMapping();
		final QuerySolutionMap qsmap = new QuerySolutionMap();

		final QueryTriplestoreParameterised qtsp = new QueryTriplestoreParameterised(SPARQLEndpoint, requestFilesPath);
		qsmap.add("RESOURCE_1", ResourceFactory.createResource(docURI));
		qsmap.add("LITERAL_1", ResourceFactory.createTypedLiteral(Integer.valueOf(pagenum)));

		return qtsp.getSelectQuery(requestName, qsmap, pmap);

	}


	/**
	 * @param docURI
	 *            The document URI
	 * @param SPARQLEndpoint
	 *            The address of the SPARQL endpoint
	 * @param pagenum
	 *            The current page number
	 * @param requestName
	 *            The name of the request file to be loaded
	 * @return The answer of the ask Query issued to the triplestore : whether the document contains the page requested
	 */
	public static boolean askHasPageFromTriplestore(final String docURI, final String SPARQLEndpoint, final int pagenum, final String requestName) {

		/*
		 * Ask if the given document uri has page
		 */
		final String requestFilesPath = "requests";

		final PrefixMapping pmap = DocumentRendererProcessor.createPrefixMapping();
		final QuerySolutionMap qsmap = new QuerySolutionMap();

		final QueryTriplestoreParameterised qtsp = new QueryTriplestoreParameterised(SPARQLEndpoint, requestFilesPath);
		qsmap.add("RESOURCE_1", ResourceFactory.createResource(docURI));
		qsmap.add("LITERAL_1", ResourceFactory.createTypedLiteral(Integer.valueOf(pagenum)));

		return qtsp.getAskQuery(requestName, qsmap, pmap);
	}


	/**
	 * @param text
	 * @return a hash value of the input text (used to generate URIs)
	 */
	private static String hash(final String text) {

		final byte[] bytesOfMessage = text.getBytes(StandardCharsets.UTF_8);
		final MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA1");
		} catch (final NoSuchAlgorithmException nsae) {
			throw new WebLabUncheckedException("Unable to load SHA1 algorithm. This should never happen.", nsae);
		}
		return new String(Hex.encodeHex(md.digest(bytesOfMessage)));
	}


	private static void writeCss(final XMLStreamWriter xtw, final HashMap<String, String> legendCssMap) throws XMLStreamException {
		// Yes, it's horrible, but we're writing the css here using the parameters passed by the calling program

		if (legendCssMap != null) {
			xtw.writeStartElement("style");
			xtw.writeAttribute("type", "text/css");

			final CreateEntityCSS createEntityCss = new CreateEntityCSS(legendCssMap);

			// Write the single attributes
			for (final String entityClassName : legendCssMap.keySet()) {
				createEntityCss.writeSingleCSS(entityClassName, legendCssMap.get(entityClassName), xtw);
			}

			// Write the multiple attributes
			createEntityCss.construct(0, legendCssMap.size(), "", "", 0, xtw);

			xtw.writeEndElement(); // end of style
			xtw.flush();
		}

	}


	/**
	 * Loading the request
	 *
	 * @param resourceName
	 *            The name of the file to load.
	 * @return The String contained in the loaded file
	 * @throws IOException
	 *             If anything wrong happens.
	 */
	public static String loadRequest(final String resourceName) throws IOException {

		try (final InputStream resourceAsStream = DocumentRendererProcessor.class.getClassLoader().getResourceAsStream(resourceName);) {
			if (resourceAsStream == null) {
				final String message = "Unable to load request " + resourceName + "...";
				DocumentRendererProcessor.logger.error(message);
				throw new IOException(message);
			}
			return IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
		}
	}

}
