/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab;


import java.io.File;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolutionMap;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.sparql.resultset.ResultSetMem;

/**
 * The QueryTriplestoreParameterised generates SPARQL requests for a given endpoint from files in main/requests
 * and returns the results of the requests.
 * Each request has two maps associated with it : the prefixes and the variables to be substituted.
 *
 * @author Esther
 *
 */
public class QueryTriplestoreParameterised {


	// ATTRIBUTES

	private final String requestFilesPath;


	private final String SPARQLEndpoint;


	private static Log logger = LogFactory.getLog(QueryTriplestoreParameterised.class);


	private String query;


	protected QueryTriplestoreParameterised(final String SPARQLEndpoint, final String requestFilesPath) {
		// path to sparql request templates
		this.requestFilesPath = requestFilesPath;

		// repository url
		this.SPARQLEndpoint = SPARQLEndpoint;
	}


	protected final ResultSet getSelectQuery(final String requestFileName, final QuerySolutionMap qsmap, final PrefixMapping pmap) {
		try (final QueryExecution qe = this.prepareParameterizedRequest(requestFileName, qsmap, pmap)) {
			return new ResultSetMem(qe.execSelect());
		}
	}


	protected final Model getDescribeQuery(final String requestFileName, final QuerySolutionMap qsmap, final PrefixMapping pmap) {
		try (final QueryExecution qe = this.prepareParameterizedRequest(requestFileName, qsmap, pmap)) {
			return qe.execDescribe();
		}
	}


	protected final boolean getAskQuery(final String requestFileName, final QuerySolutionMap qsmap, final PrefixMapping pmap) {
		try (final QueryExecution qe = this.prepareParameterizedRequest(requestFileName, qsmap, pmap)) {
			return qe.execAsk();
		}
	}


	protected QueryExecution prepareParameterizedRequest(final String requestFileName, final QuerySolutionMap qsmap, final PrefixMapping pmap) {
		// Populate query string with the contents of the file requestFileName
		this.readQueryString(requestFileName);

		// Use the query string found to create a parameterized sparql string
		final ParameterizedSparqlString queryStr = new ParameterizedSparqlString(this.getQuery(), qsmap, pmap);

		if (QueryTriplestoreParameterised.logger.isTraceEnabled()) {
			QueryTriplestoreParameterised.logger.trace(queryStr);
		}

		// Create the parameterized query on the user-given endpoint
		return QueryExecutionFactory.createServiceRequest(this.SPARQLEndpoint, queryStr.asQuery());
	}


	protected void readQueryString(final String requestFileName) {
		// For a given requestFileName, the contents of the file gives the query string
		try {
			final String resourceName = this.requestFilesPath + File.separator + requestFileName + ".rq";
			QueryTriplestoreParameterised.logger.trace("Searching for: " + resourceName);
			this.setQuery(DocumentRendererProcessor.loadRequest(resourceName));
			if (QueryTriplestoreParameterised.logger.isTraceEnabled()) {
				QueryTriplestoreParameterised.logger.trace("prepared request: " + this.getQuery());
			}
		} catch (final IOException e) {
			QueryTriplestoreParameterised.logger.error("Unable to find request file for this method " + e.getMessage(), e);
		}
	}


	/**
	 * @return the query
	 */
	public String getQuery() {
		return this.query;
	}


	/**
	 * @param query
	 *            the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

}
