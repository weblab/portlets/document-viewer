/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab;

/**
 * Bean containing the span information
 *
 * @author esther
 *
 */
public class Span implements Comparable<Span> {


	int end;


	int begin;


	String style;


	String styleClassName;


	String href;


	String namedEntity;


	boolean isCandidate;


	// This field serves to differentiate two segments which start and end in the same place, and point to exactly the same named entity
	int differentiator;


	Span(final int end, final int begin, final String style, final String styleClassName, final String href, final String namedEntity, final boolean isCandidate, final int differentiator) {
		this.end = end;
		this.begin = begin;
		this.style = style;
		this.styleClassName = styleClassName;
		this.href = href;
		this.namedEntity = namedEntity;
		this.isCandidate = isCandidate;
		this.differentiator = differentiator;
	}


	/**
	 * @param end
	 *            the end to set
	 */
	public void setEnd(final int end) {
		this.end = end;
	}


	/**
	 * @param begin
	 *            the begin to set
	 */
	public void setBegin(final int begin) {
		this.begin = begin;
	}


	/**
	 * @param style
	 *            the style to set
	 */
	public void setStyle(final String style) {
		this.style = style;
	}


	/**
	 * @param styleClassName
	 *            the styleClassName to set
	 */
	public void setStyleClassName(final String styleClassName) {
		this.styleClassName = styleClassName;
	}


	/**
	 * @param href
	 *            the href to set
	 */
	public void setHref(final String href) {
		this.href = href;
	}


	/**
	 * @param namedEntity
	 *            the namedEntity to set
	 */
	public void setNamedEntity(final String namedEntity) {
		this.namedEntity = namedEntity;
	}


	/**
	 * @param isCandidate
	 *            the isCandidate to set
	 */
	public void setCandidate(final boolean isCandidate) {
		this.isCandidate = isCandidate;
	}


	/**
	 * @param differentiator
	 *            the differentiator to set
	 */
	public void setDifferentiator(final int differentiator) {
		this.differentiator = differentiator;
	}


	@Override
	public int compareTo(final Span o) {
		if ((this.end - o.end) != 0) {
			// The spans don't finish at the same point
			return (this.end - o.end);
		}
		// The spans finish at the same point, so check the start point
		if ((this.begin - o.begin) != 0) {
			// The spans don't start at the same point
			return (this.begin - o.begin);
		}
		if (!this.style.equals(o.style)) {
			// The spans have different styles
			return (this.style.compareToIgnoreCase(o.style));
		}
		if (!this.styleClassName.equals(o.styleClassName)) {
			// The spans have different styleClassNames
			return (this.styleClassName.compareToIgnoreCase(o.styleClassName));
		}
		if (!this.href.equals(o.href)) {
			// The spans have different hrefs
			return (this.href.compareToIgnoreCase(o.href));
		}
		if (!this.namedEntity.equals(o.namedEntity)) {
			// The spans have different named entities
			return (this.namedEntity.compareToIgnoreCase(o.namedEntity));
		}
		if (this.isCandidate != o.isCandidate) {
			// The spans have different values for isCandidate
			return this.isCandidate ? 1 : -1; // But consistant this.compareTo(o) = - o.compareTo(this)
		}
		// Everything else is equal, it depends on the differentiator
		return (this.differentiator - o.differentiator);
	}
}
