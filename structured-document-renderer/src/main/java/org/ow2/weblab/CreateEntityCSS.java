/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab;


import java.util.HashMap;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * Writes the css for the entities and their colours as detailed in the legend map parameter.
 * The resulting css file contains the css for each entity, and also the css for each overlap The overlaps are written as linear-gradients
 *
 * @author esther
 */
public class CreateEntityCSS {


	private static HashMap<String, String> legendMap;


	/**
	 * Default Constructor
	 *
	 * @param legendMap
	 *            The map of entity class names and their corresponding colours
	 */
	CreateEntityCSS(final HashMap<String, String> legendMap) {
		CreateEntityCSS.legendMap = legendMap;
	}


	private static HashMap<String, String> legendMap() {
		return CreateEntityCSS.legendMap;
	}


	/**
	 * Called recursively on the entities, it constructs the css file. It is based on a binary tree using two calls, one with the current entity included, the other without it.
	 *
	 * @param currentPos
	 *            The current position in the recursive call
	 * @param maxPos
	 *            The length of the entity enumeration (and therefore the maximum depth in the recursive call)
	 * @param entityClassNames
	 *            The string of entity class names with fullstops before each one (to indicate class)
	 * @param colours
	 *            The string of their corresponding colours separated by commas
	 * @param isMulti
	 *            Indicates how many entries are in this class, and therefore which css instruction to write
	 * @param xtw
	 *            The XMLStreamWriter created by the calling program to write the html page
	 * @throws XMLStreamException
	 *             If an error occurs writing CSS to html stream
	 */
	public void construct(final int currentPos, final int maxPos, final String entityClassNames, final String colours, final int isMulti, final XMLStreamWriter xtw) throws XMLStreamException {

		if (currentPos > (maxPos - 1)) {
			// We're at the bottom of the recursive call
			if (isMulti > 1) {
				// There are two or more class entries, so write the overlap css
				CreateEntityCSS.writeMultiCSS(entityClassNames, colours, xtw);
			}
		} else {
			// Call the method again for the next level without this entity
			this.construct(currentPos + 1, maxPos, entityClassNames, colours, isMulti, xtw);

			String colourString;
			if (!"".equals(colours)) {
				// If colours have already been written, then add the next separated by a comma
				colourString = colours + ", " + CreateEntityCSS.rgb(CreateEntityCSS.legendMap().values().toArray()[currentPos]);
			} else {
				// No colours have yet been written, so don't add a comma
				colourString = CreateEntityCSS.rgb(CreateEntityCSS.legendMap().values().toArray()[currentPos]);
			}
			// Call the method again for the next level with this entity and its colour
			this.construct(currentPos + 1, maxPos, entityClassNames + "." + CreateEntityCSS.legendMap().keySet().toArray()[currentPos], colourString, isMulti + 1, xtw);
		}
	}


	/**
	 * Generate good rgb tag depending on number of colors
	 *
	 * @param colors
	 * @return if nb of colors > 3 return rgba else it returns rgb css function
	 */
	private static String rgb(final Object colors) {
		final int nbColors = colors.toString().split(",").length;

		final String rgb;
		if (nbColors > 3) {
			rgb = "rgba(";
		} else {
			rgb = "rgb(";
		}
		return rgb + colors + ")";
	}


	/**
	 * Write a css block for a class containing multiple entries. It uses
	 * linear-gradient to show the overlap.
	 *
	 * @param entityClassNames
	 *            The class entries separated by spaces
	 * @param colours
	 *            The corresponding colours separated by commas
	 * @param xtw
	 *            The XMLStreamWriter created by the calling program to write the html page
	 * @throws XMLStreamException
	 *             If an error occurs writing CSS to html stream
	 */
	private static void writeMultiCSS(final String entityClassNames, final String colours, final XMLStreamWriter xtw) throws XMLStreamException {
		// When there are two or more entities, use a linear-gradient to show the overlap
		final StringBuilder cssString = new StringBuilder();
		cssString.append(entityClassNames);
		cssString.append(" {\n");
		cssString.append("\t background-color: rgb(168,168,168) ; /* fallback color (gray) if gradients are not supported */\n");
		cssString.append("\t background-image: -webkit-linear-gradient(top, ");
		cssString.append(colours);
		cssString.append("); /* For Chrome and Safari */\n");
		cssString.append("\t background-image: -moz-linear-gradient(top, ");
		cssString.append(colours);
		cssString.append("); /* For old Fx (3.6 to 15) */\n");
		cssString.append("\t background-image: -ms-linear-gradient(top, ");
		cssString.append(colours);
		cssString.append("); /* For pre-releases of IE 10*/\n");
		cssString.append("\t background-image: -o-linear-gradient(top, ");
		cssString.append(colours);
		cssString.append("); /* For old Opera (11.1 to 12.0) */\n");
		cssString.append("\t background-image:linear-gradient(to bottom, ");
		cssString.append(colours);
		cssString.append("); /* Standard syntax; must be last */\n");
		cssString.append("}\n");
		xtw.writeCharacters(cssString.toString());
	}


	/**
	 * Write a css block for a class containing one entry. It simply uses background-color as there is no overlap.
	 *
	 * @param entity
	 *            The class entry
	 * @param colour
	 *            The corresponding colour
	 * @param xtw
	 *            The XMLStreamWriter created by the calling program to write the html page
	 * @throws XMLStreamException
	 *             If an error occured writing to xml
	 */
	public void writeSingleCSS(final String entity, final String colour, final XMLStreamWriter xtw) throws XMLStreamException {
		// When there is just one entity, use a background color
		xtw.writeCharacters("." + entity + " {\n\t background-color:" + CreateEntityCSS.rgb(colour) + "}\n");
	}
}
