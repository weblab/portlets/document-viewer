/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab;

import java.util.HashMap;
import java.util.LinkedList;

import javax.xml.stream.XMLStreamWriter;

import org.ow2.weblab.core.helper.impl.SemanticResource;
import org.ow2.weblab.core.model.Document;

/**
 * @author rgauthier
 */
public class DocumentRendererBean {


	public enum FindPageRequest {
		QuickViewGivenPageDoc, selectAllGivenPageDoc
	}


	// The URI of the document in the SPARQL endpoint
	private String docURI;


	// The document resource
	private Document doc;


	// The URL of the SPARQL endpoint
	private String SPARQLEndpoint;


	// The semantic resource build from doc with docUri
	private SemanticResource semRes;


	// The page number of the document to render
	private int pagenum;


	// Whether the hrefs are shown
	private boolean showInstanceLink = false;


	// Whether the tooltips are shown
	private boolean showToolTips = false;


	// The XMLStreamWriter into which the result is written
	private XMLStreamWriter xtw = null;


	// A map between a span id and the list of named entities referred to in that span
	private HashMap<String, LinkedList<String>> spanEntities = null;


	// A map between a span id and the href referred to in that span
	private HashMap<String, String> spanHrefs = null;


	// Whether the output should be strict XHTML (includes the head and body tags) or just the fragment necessary for display in a portlet
	private boolean fullHTML = false;


	// A map of the legend field class names used for the display and their associated colours
	private HashMap<String, String> legendCssMap = null;


	// A list of equivalent entity types to the parent entity class
	private HashMap<String, String> legendUriClassMap = null;


	// Whether the path to the image is relative to the portlet
	private boolean relative = false;


	// Boolean to force non normalised document to be returned
	private boolean forceNonNormalised = false;


	// enum to indicate which page request will be used to check pages structure in triple store
	private FindPageRequest findPageRequestType;


	// This field serves to differentiate between two segments which start and end in the same place on the same page, and refer to exactly the same named entity
	private int differentiator = 0;


	private String fullText;


	private int spanId = 1;


	private String currentHref = "";


	private boolean spanActive = false;


	/**
	 * @return The document URI
	 */
	public String getDocURI() {

		if (this.docURI != null) {
			return this.docURI;
		} else if (this.doc != null) {
			return this.doc.getUri();
		}
		return null;
	}


	/**
	 * @param docURI
	 *            The document URI
	 */
	public void setDocURI(final String docURI) {

		this.docURI = docURI;
	}


	/**
	 * @return The request for normalised document
	 */
	public String getFindPageRequestNormalised() {

		return this.findPageRequestType.toString();
	}


	/**
	 * @return The request for non normalised
	 */
	public String getFindPageRequestNonNormalised() {

		return this.findPageRequestType.toString() + "NonNorm";
	}


	/**
	 * @return the doc
	 */
	public Document getDoc() {

		return this.doc;
	}


	/**
	 * @param doc
	 *            the doc to set
	 */
	public void setDoc(final Document doc) {

		this.doc = doc;
	}


	/**
	 * @return the SPARQLEndpoint
	 */
	public String getSPARQLEndpoint() {

		return this.SPARQLEndpoint;
	}


	/**
	 * @param sparqlEndpoint
	 *            the sparqlEndpoint to set
	 */
	public void setSPARQLEndpoint(final String sparqlEndpoint) {

		this.SPARQLEndpoint = sparqlEndpoint;
	}


	/**
	 * @return the semRes
	 */
	public SemanticResource getSemRes() {

		return this.semRes;
	}


	/**
	 * @param semRes
	 *            the semRes to set
	 */
	public void setSemRes(final SemanticResource semRes) {

		this.semRes = semRes;
	}


	/**
	 * @return the pagenum
	 */
	public int getPagenum() {

		return this.pagenum;
	}


	/**
	 * @param pagenum
	 *            the pagenum to set
	 */
	public void setPagenum(final int pagenum) {

		this.pagenum = pagenum;
	}


	/**
	 * @return the showInstanceLink
	 */
	public boolean isShowInstanceLink() {

		return this.showInstanceLink;
	}


	/**
	 * @param showInstanceLink
	 *            the showInstanceLink to set
	 */
	public void setShowInstanceLink(final boolean showInstanceLink) {

		this.showInstanceLink = showInstanceLink;
	}


	/**
	 * @return the showToolTips
	 */
	public boolean isShowToolTips() {

		return this.showToolTips;
	}


	/**
	 * @param showToolTips
	 *            the showToolTips to set
	 */
	public void setShowToolTips(final boolean showToolTips) {

		this.showToolTips = showToolTips;
	}


	/**
	 * @return the xtw
	 */
	public XMLStreamWriter getXtw() {

		return this.xtw;
	}


	/**
	 * @param xtw
	 *            the xtw to set
	 */
	public void setXtw(final XMLStreamWriter xtw) {

		this.xtw = xtw;
	}


	/**
	 * @return the spanEntities
	 */
	public HashMap<String, LinkedList<String>> getSpanEntities() {

		return this.spanEntities;
	}


	/**
	 * @param spanEntities
	 *            the spanEntities to set
	 */
	public void setSpanEntities(final HashMap<String, LinkedList<String>> spanEntities) {

		this.spanEntities = spanEntities;
	}


	/**
	 * @return the spanHrefs
	 */
	public HashMap<String, String> getSpanHrefs() {

		return this.spanHrefs;
	}


	/**
	 * @param spanHrefs
	 *            the spanHrefs to set
	 */
	public void setSpanHrefs(final HashMap<String, String> spanHrefs) {

		this.spanHrefs = spanHrefs;
	}


	/**
	 * @return the fullHTML
	 */
	public boolean isFullHTML() {

		return this.fullHTML;
	}


	/**
	 * @param fullHTML
	 *            the fullHTML to set
	 */
	public void setFullHTML(final boolean fullHTML) {

		this.fullHTML = fullHTML;
	}


	/**
	 * @return the legendCssMap
	 */
	public HashMap<String, String> getLegendCssMap() {

		return this.legendCssMap;
	}


	/**
	 * @param legendCssMap
	 *            the legendCssMap to set
	 */
	public void setLegendCssMap(final HashMap<String, String> legendCssMap) {

		this.legendCssMap = legendCssMap;
	}


	/**
	 * @return the legendUriClassMap
	 */
	public HashMap<String, String> getLegendUriClassMap() {

		return this.legendUriClassMap;
	}


	/**
	 * @param legendUriClassMap
	 *            the legendUriClassMap to set
	 */
	public void setLegendUriClassMap(final HashMap<String, String> legendUriClassMap) {

		this.legendUriClassMap = legendUriClassMap;
	}


	/**
	 * @return the relative
	 */
	public boolean isRelative() {

		return this.relative;
	}


	/**
	 * @param relative
	 *            the relative to set
	 */
	public void setRelative(final boolean relative) {

		this.relative = relative;
	}


	/**
	 * @return the forceNonNormalised
	 */
	public boolean isForceNonNormalised() {

		return this.forceNonNormalised;
	}


	/**
	 * @param forceNonNormalised
	 *            the forceNonNormalised to set
	 */
	public void setForceNonNormalised(final boolean forceNonNormalised) {

		this.forceNonNormalised = forceNonNormalised;
	}


	/**
	 * @return the findPageRequestType
	 */
	public FindPageRequest getFindPageRequestType() {

		return this.findPageRequestType;
	}


	/**
	 * @param findPageRequestType
	 *            the findPageRequestType to set
	 */
	public void setFindPageRequestType(final FindPageRequest findPageRequestType) {

		this.findPageRequestType = findPageRequestType;
	}


	/**
	 * @return the differentiator
	 */
	public int getDifferentiator() {

		return this.differentiator;
	}


	/**
	 * @param differentiator
	 *            The differentiator to set
	 */
	public void setDifferentiator(final int differentiator) {

		this.differentiator = differentiator;
	}


	/**
	 * @return the fullText
	 */
	public String getFullText() {

		return this.fullText;
	}


	/**
	 * @param fullText
	 *            The fullText to set
	 */
	public void setFullText(final String fullText) {

		this.fullText = fullText;
	}


	/**
	 * @return the spanId
	 */
	public int getSpanId() {

		return this.spanId;
	}


	/**
	 * @param spanId
	 *            The spanId to set
	 */
	public void setSpanId(final int spanId) {

		this.spanId = spanId;
	}


	/**
	 * @return The currentHref
	 */
	public String getCurrentHref() {

		return this.currentHref;
	}


	/**
	 * @param currentHref
	 *            The currentHref to set
	 */
	public void setCurrentHref(final String currentHref) {

		this.currentHref = currentHref;
	}


	/**
	 * @return whether spanActive or not
	 */
	public boolean isSpanActive() {

		return this.spanActive;
	}


	/**
	 * @param spanActive
	 *            The spanActive to set
	 */
	public void setSpanActive(final boolean spanActive) {

		this.spanActive = spanActive;
	}

}