/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 */

package org.ow2.weblab;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;

import com.hp.hpl.jena.query.QuerySolutionMap;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.shared.impl.PrefixMappingImpl;

public class TestDocumentRenderer {


	@Test
	public void testEmptyDoc() throws Exception {

		final int pagenum = 1;
		final String filename = "target/outputEmptyDoc.html";

		final HashMap<String, String> legendCssMap = new HashMap<>();
		legendCssMap.put("document-renderer-Person", "51, 204, 255, 0.5");
		legendCssMap.put("document-renderer-Place", "251, 4, 255");

		final HashMap<String, String> legendUriClassMap = new HashMap<>();
		legendUriClassMap.put("http://weblab.ow2.org/wookie#Person", "document-renderer-Person");
		legendUriClassMap.put("http://weblab.ow2.org/wookie#Place", "document-renderer-Place");

		final XMLOutputFactory xof = XMLOutputFactory.newInstance();
		try (FileOutputStream fos = new FileOutputStream(filename)) {
			final XMLStreamWriter xtw = xof.createXMLStreamWriter(fos, "utf-8");
			final WebLabMarshaller marsh = new WebLabMarshaller();
			final Document doc = marsh.unmarshal(new File("src/test/resources/emptyDoc.xml"), Document.class);

			final DocumentRendererBean renderBean = new DocumentRendererBean();
			renderBean.setDoc(doc);
			renderBean.setPagenum(pagenum);
			renderBean.setXtw(xtw);
			renderBean.setFullHTML(false);
			renderBean.setLegendCssMap(legendCssMap);
			renderBean.setLegendUriClassMap(legendUriClassMap);
			renderBean.setRelative(false);

			// DocumentRendererProcessor.getQuickViewHtmlRepresentation(renderBean);
			DocumentRendererProcessor.getRenderedHtmlRepresentation(renderBean);
		}
	}


	@Test
	public void testArabicDoc() throws Exception {

		final int pagenum = 1;
		final String filename = "target/arabic.html";

		final HashMap<String, String> legendCssMap = new HashMap<>();
		legendCssMap.put("document-renderer-Person", "51, 204, 255, 0.5");
		legendCssMap.put("document-renderer-Place", "251, 4, 255");

		final HashMap<String, String> legendUriClassMap = new HashMap<>();
		legendUriClassMap.put("http://weblab.ow2.org/wookie#Person", "document-renderer-Person");
		legendUriClassMap.put("http://weblab.ow2.org/wookie#Place", "document-renderer-Place");

		try (FileOutputStream fos = new FileOutputStream(filename)) {
			final XMLOutputFactory xof = XMLOutputFactory.newInstance();
			final XMLStreamWriter xtw = xof.createXMLStreamWriter(fos, "utf-8");
			final WebLabMarshaller marsh = new WebLabMarshaller();
			final Document doc = marsh.unmarshal(new File("src/test/resources/arabic.xml"), Document.class);

			final DocumentRendererBean renderBean = new DocumentRendererBean();
			renderBean.setDoc(doc);
			renderBean.setPagenum(pagenum);
			renderBean.setXtw(xtw);
			renderBean.setFullHTML(true);
			renderBean.setLegendCssMap(legendCssMap);
			renderBean.setLegendUriClassMap(legendUriClassMap);
			renderBean.setRelative(false);

			// DocumentRendererProcessor.getQuickViewHtmlRepresentation(renderBean);
			DocumentRendererProcessor.getRenderedHtmlRepresentation(renderBean);
		}
	}


	@Test
	public void testProblematicPdf() throws Exception {

		final int pagenum = 1;
		final String filename = "target/serval.xml";

		final HashMap<String, String> legendCssMap = new HashMap<>();
		legendCssMap.put("document-renderer-Person", "51, 204, 255, 0.5");
		legendCssMap.put("document-renderer-Place", "251, 4, 255");

		final HashMap<String, String> legendUriClassMap = new HashMap<>();
		legendUriClassMap.put("http://weblab.ow2.org/wookie#Person", "document-renderer-Person");
		legendUriClassMap.put("http://weblab.ow2.org/wookie#Place", "document-renderer-Place");

		try (FileOutputStream fos = new FileOutputStream(filename)) {
			final XMLOutputFactory xof = XMLOutputFactory.newInstance();
			final XMLStreamWriter xtw = xof.createXMLStreamWriter(fos, "utf-8");
			final WebLabMarshaller marsh = new WebLabMarshaller();
			final Document doc = marsh.unmarshal(new File("src/test/resources/arabic.xml"), Document.class);

			final DocumentRendererBean renderBean = new DocumentRendererBean();
			renderBean.setDoc(doc);
			renderBean.setPagenum(pagenum);
			renderBean.setXtw(xtw);
			renderBean.setFullHTML(true);
			renderBean.setLegendCssMap(legendCssMap);
			renderBean.setLegendUriClassMap(legendUriClassMap);
			renderBean.setRelative(false);

			// DocumentRendererProcessor.getQuickViewHtmlRepresentation(renderBean);
			DocumentRendererProcessor.getRenderedHtmlRepresentation(renderBean);
		}
	}


	@Test
	public void testRenderMultiThread() throws Exception {

		final int nbDocs = 20;
		final int nbParallel = 10;

		final HashMap<String, String> legendCssMap = new HashMap<>();
		legendCssMap.put("document-renderer-Person", "51, 204, 255, 0.5");
		legendCssMap.put("document-renderer-Place", "251, 4, 255");

		final HashMap<String, String> legendUriClassMap = new HashMap<>();
		legendUriClassMap.put("http://weblab.ow2.org/wookie#Person", "document-renderer-Person");
		legendUriClassMap.put("http://weblab.ow2.org/wookie#Place", "document-renderer-Place");
		final WebLabMarshaller marsh = new WebLabMarshaller();

		final List<Callable<String>> callables = new ArrayList<>();

		for (int i = 1; i <= nbDocs; i++) {
			final DocumentRendererBean renderBean = new DocumentRendererBean();

			renderBean.setPagenum(1);
			renderBean.setFullHTML(false);
			renderBean.setLegendCssMap(legendCssMap);
			renderBean.setLegendUriClassMap(legendUriClassMap);
			renderBean.setRelative(false);
			if ((i % 2) == 0) {
				renderBean.setDoc(marsh.unmarshal(new File("src/test/resources/arabic.xml"), Document.class));
			} else {
				renderBean.setDoc(marsh.unmarshal(new File("src/test/resources/serval.xml"), Document.class));
			}

			callables.add(new RendererCallable(renderBean));
		}


		final ExecutorService executor = Executors.newFixedThreadPool(nbParallel);
		final List<Future<String>> results = executor.invokeAll(callables, 1, TimeUnit.MINUTES);

		for (final Future<String> future : results) {
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			Assert.assertNotNull(future.get());
		}

		executor.shutdownNow();
	}


	class RendererCallable implements Callable<String> {


		DocumentRendererBean rendererBean;


		public RendererCallable(final DocumentRendererBean rendererBean) {

			this.rendererBean = rendererBean;
		}


		@Override
		public String call() throws Exception {

			final StringWriter sw = new StringWriter();
			final XMLOutputFactory xof = XMLOutputFactory.newInstance();
			final XMLStreamWriter xtw = xof.createXMLStreamWriter(sw);
			this.rendererBean.setXtw(xtw);

			// DocumentRendererProcessor.getQuickViewHtmlRepresentation(this.rendererBean);
			DocumentRendererProcessor.getRenderedHtmlRepresentation(this.rendererBean);

			return sw.toString();
		}
	}


	@Test
	public void test4CoverageOnly() throws Exception {

		Href h1 = new Href(0, 0, "");
		Assert.assertEquals(0, h1.compareTo(h1));
		Assert.assertEquals(-1, h1.compareTo(new Href(1, 0, "")));
		Assert.assertEquals(-1, h1.compareTo(new Href(0, 1, "")));

		Span s1 = new Span(0, 0, "", "", "", "", false, 0);
		Span s2 = new Span(0, 0, "", "", "", "", false, 0);
		Assert.assertEquals(0, s1.compareTo(s2));
		s2.setDifferentiator(5);
		Assert.assertEquals(-5, s1.compareTo(s2));
		s2.setCandidate(true);
		Assert.assertEquals(-1, s1.compareTo(s2));
		Assert.assertEquals(1, s2.compareTo(s1));
		s2.setNamedEntity("ne2");
		Assert.assertEquals("".compareToIgnoreCase("ne2"), s1.compareTo(s2));
		s2.setHref("h2");
		Assert.assertEquals("".compareToIgnoreCase("h2"), s1.compareTo(s2));
		s2.setStyleClassName("scn2");
		Assert.assertEquals("".compareToIgnoreCase("scn2"), s1.compareTo(s2));
		s2.setStyle("s2");
		Assert.assertEquals("".compareToIgnoreCase("s2"), s1.compareTo(s2));
		s2.setBegin(1);
		Assert.assertEquals(-1, s1.compareTo(s2));
		s2.setEnd(5);
		Assert.assertEquals(-5, s1.compareTo(s2));

		DocumentRendererBean drb = new DocumentRendererBean();
		Assert.assertNull(drb.getDocURI());
		drb.setDocURI("docUri");
		Assert.assertEquals("docUri", drb.getDocURI());
		drb.setSPARQLEndpoint("sparqlEndpoint");
		drb.setShowInstanceLink(true);
		Assert.assertTrue(drb.isShowInstanceLink());
		drb.setShowToolTips(true);
		Assert.assertTrue(drb.isShowToolTips());
		drb.setSpanEntities(new HashMap<>());
		Assert.assertTrue(drb.getSpanEntities().isEmpty());
		drb.setSpanHrefs(new HashMap<>());
		Assert.assertTrue(drb.getSpanHrefs().isEmpty());
		Assert.assertNull(drb.getLegendUriClassMap());
		drb.setForceNonNormalised(true);
		Assert.assertTrue(drb.isForceNonNormalised());
		Assert.assertNull(drb.getFindPageRequestType());
		drb.setCurrentHref("currentHref");
		Assert.assertEquals("currentHref", drb.getCurrentHref());

		QueryTriplestoreParameterised qtp = new QueryTriplestoreParameterised("", "requests");
		Assert.assertNull(qtp.getQuery());
		qtp.readQueryString("notfound!");
		Assert.assertNull(qtp.getQuery());
		qtp.prepareParameterizedRequest("askIsStructured", new QuerySolutionMap(), DocumentRendererProcessor.createPrefixMapping());
		Assert.assertNotNull(qtp.getQuery());

	}

}
